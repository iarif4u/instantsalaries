import React from 'react';
import {AppProvider} from './src/context/AppContext';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';

import Router from './src/Router';
import Activity from './src/resource/Activity';
import {APPS_COLOR} from './src/lib/const';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: APPS_COLOR,
    accent: '#f1c40f',
  },
};

class Application extends React.Component<{}> {
  render() {
    return (
      <AppProvider>
        <PaperProvider theme={theme}>
          <Activity />
          <Router />
        </PaperProvider>
      </AppProvider>
    );
  }
}

export default Application;
