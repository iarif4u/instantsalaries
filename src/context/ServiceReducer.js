import {
  SET_OFFICE_FRONT,
  SET_SERVICE,
  SET_OFFICE_BACK,
  SET_OFFICE_NAME,
  SET_OFFICE_ADDRESS,
  SET_DESIGNATION,
  SET_JOINING_DATE,
  SET_SALARY,
  SET_EXPERIENCE,
} from './ReducerConst';

const ServiceReducer = (state, action) => {
  switch (action.type) {
    case SET_OFFICE_NAME:
      return {...state, company: action.payload};
    case SET_OFFICE_ADDRESS:
      return {...state, address: action.payload};
    case SET_DESIGNATION:
      return {...state, designation: action.payload};
    case SET_JOINING_DATE:
      return {...state, joining_date: action.payload};
    case SET_SALARY:
      return {...state, salary: action.payload};
    case SET_EXPERIENCE:
      return {...state, experience: action.payload};
    case SET_SERVICE:
      return action.payload;
    case SET_OFFICE_FRONT:
      return {...state, oid_front: action.payload};
    case SET_OFFICE_BACK:
      return {...state, oid_back: action.payload};
    default:
      return state;
  }
};
export default ServiceReducer;
