import {
  SET_PHOTO,
  SET_PROFILE,
  SET_SIGNATURE,
  SET_NID_FRONT,
  SET_NID_BACK,
  SET_TIN,
} from './ReducerConst';

const ProfileReducer = (state, action) => {
  switch (action.type) {
    case SET_PROFILE:
      if (
        action.payload?.religion === null ||
        action.payload?.religion === '' ||
        typeof action.payload?.religion === 'undefined'
      ) {
        return {
          ...action.payload,
          religion: 'ISLAM',
          profession: 'SERVICE',
          nationality: 'Bangladeshi',
        };
      }
      return {
        ...action.payload,
        profession: 'SERVICE',
        nationality: 'Bangladeshi',
      };
    case SET_PHOTO:
      return {...state, photo: action.payload};
    case SET_SIGNATURE:
      return {...state, signature: action.payload};
    case SET_NID_FRONT:
      return {...state, nid_front: action.payload};
    case SET_NID_BACK:
      return {...state, nid_back: action.payload};
    case SET_TIN:
      return {...state, tin_doc: action.payload};
    default:
      return state;
  }
};
export default ProfileReducer;
