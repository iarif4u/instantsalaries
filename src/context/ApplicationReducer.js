import {
  SET_TIN_ID,
  SET_SALARY_STATEMENT,
  SET_APPLICATION,
} from './ReducerConst';

const ApplicationReducer = (state, action) => {
  switch (action.type) {
    case SET_APPLICATION:
      return action.payload;
    case SET_TIN_ID:
      return {...state, tin_doc: action.payload};
    case SET_SALARY_STATEMENT:
      return {...state, salary_statement: action.payload};
    default:
      return state;
  }
};
export default ApplicationReducer;
