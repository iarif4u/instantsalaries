import React, {createContext, useReducer, useEffect} from 'react';
import {showMessage, hideMessage} from 'react-native-flash-message';

import LoadingReducer from './LadingReducer';
import ImagePicker from 'react-native-image-crop-picker';

import {
  IS_STARTUP,
  LOADING,
  USER_SIGN_IN,
  SIGN_OUT,
  SET_PROFILE,
  SET_PHOTO,
  SET_SERVICE,
  SET_OFFICE_FRONT,
  SET_OFFICE_BACK,
  SET_OFFICE_NAME,
  SET_OFFICE_ADDRESS,
  SET_DESIGNATION,
  SET_JOINING_DATE,
  SET_SALARY,
  SET_EXPERIENCE,
  SET_SIGNATURE,
  SET_APPLICATION,
  SET_APPLICATIONS,
  CHANGE_NAME,
  SET_NID_FRONT,
  SET_NID_BACK,
  SET_SALARY_STATEMENT,
  SET_TRANSACTIONS,
  SET_OFFLINE,
} from './ReducerConst';
import UserReducer from './UserReducer';

import http from '../lib/http';
import {
  PROFILE_DOC_URL,
  LOGIN_URL,
  PROFILE_PHOTO_URL,
  PROFILE_UPDATE_URL,
  REGISTER_URL,
  SERVICE_INFO_URL,
  OFFICE_ID_URL,
  SERVICE_UPDATE_URL,
  PROFILE_URL,
  SIGNATURE_URL,
  FORGET_PASSWORD_URL,
  RESET_PASSWORD_URL,
  APPLICATION_URL,
  APPLICATIONS_URL,
  CONTACT_US_URL,
  TRANSACTIONS_URL,
  CANCEL_URL,
} from '../lib/const';
import {getJson, setJson, clearItems} from '../lib/keystore';
import {
  checkApplication,
  checkLogin,
  checkProfile,
  checkRegistration,
  checkResetPassword,
  validateEmail,
  checkServiceInfo,
  checkContactUs,
} from '../lib/funcs';
import ProfileReducer from './ProfileReducer';
import ServiceReducer from './ServiceReducer';
import ApplicationReducer from './ApplicationReducer';
import ApplicationsReducer from './ApplicationsReducer';
import TransactionsReducer from './TransactionsReducer';

const initialState = {
  user: {
    name: '',
    email: '',
    contact_no: '',
    password: '',
    password_confirmation: '',
    remember: '',
    terms_and_condition: true,
    is_signed_in: false,
    token: '',
    religion: 'ISLAM',
    profession: 'SERVICE',
  },
  service: {},
  application: {
    serviceId: 1,
    salary_expected_date: '1',
    details: '',
    referredBy: 'Info',
    referrerName: ['', ''],
    referrerContactNo: ['', ''],
    referrerNID: ['', ''],
    referrerProfessionalDetails: ['', ''],
    salary_statement: {},
    tin_doc: {},
  },
  profile: {
    profession: 'SERVICE',
    religion: 'ISLAM',
  },
  loading: {
    is_loading: false,
    apps_loaded: false,
    is_offline: false,
  },
  applications: [],
  transactions: [],
};

export const AppContext = createContext(initialState);
export const AppProvider = ({children}) => {
  const [loadingState, dispatchLoading] = useReducer(
    LoadingReducer,
    initialState.loading,
  );
  const [userState, dispatchUser] = useReducer(UserReducer, initialState.user);
  const [applicationState, dispatchApplication] = useReducer(
    ApplicationReducer,
    initialState.application,
  );
  const [serviceState, dispatchService] = useReducer(
    ServiceReducer,
    initialState.service,
  );
  const [profileState, dispatchProfile] = useReducer(
    ProfileReducer,
    initialState.user,
  );
  const [applicationsState, dispatchApplications] = useReducer(
    ApplicationsReducer,
    initialState.applications,
  );
  const [transactionsState, dispatchTransactions] = useReducer(
    TransactionsReducer,
    initialState.transactions,
  );
  const getServiceInfo = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      done(false);
      return;
    }
    makeLoading(true);
    http
      .getData(SERVICE_INFO_URL, userState.token)
      .then((response) => {
        dispatchService({
          type: SET_SERVICE,
          payload: response.data,
        });
      })
      .catch((e) => {})
      .then(() => {
        done(true);
        makeLoading(false);
      });
  };
  const getServiceData = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    makeLoading(true);
    http
      .getData(SERVICE_INFO_URL, userState.token)
      .then((response) => {
        dispatchService({
          type: SET_SERVICE,
          payload: response.data,
        });
        done(response.data);
      })
      .catch((e) => {
        done(false);
      })
      .then(() => {
        makeLoading(false);
      });
  };
  const getUserApplications = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    makeLoading(true);
    http
      .getData(APPLICATIONS_URL, userState.token)
      .then((response) => {
        dispatchApplications({
          type: SET_APPLICATIONS,
          payload: response.data?.instant_salaries,
        });
      })
      .catch((e) => {
        console.log(e.response);
      })
      .then(() => {
        makeLoading(false);
      });
  };
  const getUserTransactions = () => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    makeLoading(true);
    http
      .getData(TRANSACTIONS_URL, userState.token)
      .then((response) => {
        dispatchTransactions({
          type: SET_TRANSACTIONS,
          payload: response.data,
        });
      })
      .catch((e) => {
        console.log(e.response);
      })
      .then(() => {
        makeLoading(false);
      });
  };
  const makeLoading = (status) => {
    dispatchLoading({
      type: LOADING,
      payload: status,
    });
  };
  const makeOffline = (status) => {
    dispatchLoading({
      type: SET_OFFLINE,
      payload: status,
    });
  };
  const changeUserData = (type, data) => {
    dispatchUser({
      type: type,
      payload: data,
    });
  };
  const getUserProfile = () => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    makeLoading(true);
    http
      .getData(PROFILE_URL, userState.token)
      .then((response) => {
        changeUserData(CHANGE_NAME, response.data.name);
        updateProfile(response.data.profile);
      })
      .catch((e) => {})
      .then(() => {
        makeLoading(false);
      });
  };
  const updateProfile = (data) => {
    dispatchProfile({
      type: SET_PROFILE,
      payload: data,
    });
  };
  const updateApplication = (data) => {
    dispatchApplication({
      type: SET_APPLICATION,
      payload: data,
    });
  };
  const resetUserData = () => {
    clearItems().then((r) => {
      dispatchProfile({
        type: SET_PROFILE,
        payload: initialState.profile,
      });
      dispatchService({
        type: SET_SERVICE,
        payload: initialState.service,
      });
      dispatchUser({
        type: SIGN_OUT,
        payload: initialState.user,
      });
    });
  };
  const resetUserEmailNPass = () => {
    dispatchUser({
      type: SIGN_OUT,
      payload: initialState.user,
    });
  };
  const submitLogin = () => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
    } else {
      if (checkLogin({email: userState.email, password: userState.password})) {
        makeLoading(true);
        http
          .postData(LOGIN_URL, {
            email: userState.email,
            password: userState.password,
          })
          .then((response) => {
            setJson('token', response.data.access_token).then(() => {
              dispatchUser({
                type: USER_SIGN_IN,
                payload: response.data.access_token,
              });
            });
          })
          .catch((e) => {
            showMessage({
              message: 'Invalid username or password',
              type: 'danger',
            });
          })
          .then(() => {
            makeLoading(false);
          });
      }
    }
  };
  const cancelApplication = (application_id) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    makeLoading(true);
    http
      .postDataToken(
        `${CANCEL_URL}/${application_id}`,
        {application_id},
        userState.token,
      )
      .then((response) => {
        getUserApplications();
        getUserProfile();
        showMessage({
          message: response.data.join(''),
          type: 'success',
        });
      })
      .catch((e) => {
        showMessage({
          message: 'Application cancel failed',
          type: 'danger',
        });
      })
      .then(() => {
        makeLoading(false);
      });
  };
  const sendResetMail = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      done(false);
      return;
    }
    if (validateEmail(userState.email)) {
      hideMessage();
      makeLoading(true);
      http
        .postData(FORGET_PASSWORD_URL, {
          email: userState.email,
        })
        .then((response) => {
          showMessage({
            message: response.data.join(''),
            type: 'success',
          });
          done(true);
        })
        .catch((e) => {
          showMessage({
            message: 'Email address not found',
            type: 'danger',
          });
          done(false);
        })
        .then(() => {
          makeLoading(false);
        });
    } else {
      if (userState.email.length === 0 || userState.email === '') {
        showMessage({
          message: 'Email address is required',
          type: 'danger',
        });
      } else {
        showMessage({
          message: 'Email address is invalid',
          type: 'danger',
        });
      }
      done(false);
    }
  };
  const resetPassword = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      done(false);
      return;
    }
    if (
      checkResetPassword({
        token: userState.token,
        password: userState.password,
        confirm_password: userState.password_confirmation,
      })
    ) {
      hideMessage();
      makeLoading(true);
      http
        .postData(RESET_PASSWORD_URL, {
          email: userState.email,
          token: userState.token,
          password: userState.password,
          password_confirmation: userState.password_confirmation,
        })
        .then((response) => {
          showMessage({
            message: 'Your password has been reset',
            type: 'success',
          });
          done(true);
        })
        .catch((e) => {
          if (e.response.status === 422) {
            showMessage({
              message: e.response.data?.errors[
                Object.keys(e.response.data?.errors)[0]
              ].join(''),
              type: 'danger',
            });
          } else {
            showMessage({
              message: "Verification code doesn't match",
              type: 'danger',
            });
          }
          done(false);
        })
        .then(() => {
          makeLoading(false);
        });
    } else {
      done(false);
    }
  };
  const submitServices = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (
      checkServiceInfo({
        oid_front: serviceState.oid_front,
        oid_back: serviceState.oid_back,
        company: serviceState.company,
        address: serviceState.address,
        designation: serviceState.designation,
        joining_date: serviceState.joining_date,
        salary: serviceState.salary,
        experience: serviceState.experience,
      })
    ) {
      makeLoading(true);
      var data = new FormData();
      data.append('company', serviceState.company);
      data.append('address', serviceState.address);
      data.append('designation', serviceState.designation);
      data.append('joining_date', serviceState.joining_date);
      data.append('salary', serviceState.salary);
      data.append('experience', serviceState.experience);
      http
        .postDataToken(SERVICE_UPDATE_URL, data, userState.token)
        .then((response) => {
          showMessage({
            message: 'Service Information Successfully Done',
            type: 'success',
          });
          done(true);
        })
        .catch((e) => {
          if (e.response.status === 422) {
            showMessage({
              message: e.response.data?.errors[
                Object.keys(e.response.data?.errors)[0]
              ].join(''),
              type: 'danger',
            });
          }
          done(false);
        })
        .catch((e) => {
          showMessage({
            message: 'Service Information Update Failed',
            type: 'danger',
          });
          done(false);
        })
        .then(() => makeLoading(false));
      done(false);
    }
  };
  const submitRegistration = () => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (
      checkRegistration({
        name: userState.name,
        email: userState.email,
        password: userState.password,
        mobile: userState.contact_no,
        confirm_password: userState.password_confirmation,
        terms_and_condition: userState.terms_and_condition,
      })
    ) {
      hideMessage();
      makeLoading(true);
      http
        .postData(REGISTER_URL, userState)
        .then((response) => {
          setJson('token', response.data.access_token).then(() => {
            dispatchUser({
              type: USER_SIGN_IN,
              payload: response.data.access_token,
            });
          });
        })
        .catch((e) => {
          if (e.response.status === 422) {
            showMessage({
              message: e.response.data?.errors[
                Object.keys(e.response.data?.errors)[0]
              ].join(''),
              type: 'danger',
            });
          }
        })
        .then(() => makeLoading(false))
        .catch((e) => console.log(e));
    }
  };
  const submitContactUs = ({name, phone, subject, message}, done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (
      checkContactUs({
        subject: subject,
        message: message,
      })
    ) {
      hideMessage();
      makeLoading(true);
      http
        .postData(CONTACT_US_URL, {
          name: name,
          email: userState.email ? userState.email : profileState.contact_email,
          phone: phone,
          subject: subject,
          message: message,
        })
        .then((response) => {
          showMessage({
            message: response.data.join(''),
            type: 'success',
          });
          done(true);
        })
        .catch((e) => {
          if (e.response.status === 422) {
            showMessage({
              message: e.response.data?.errors[
                Object.keys(e.response.data?.errors)[0]
              ].join(''),
              type: 'danger',
            });
          }
          done(false);
        })
        .then(() => makeLoading(false))
        .catch((e) => console.log(e));
    }
  };
  const TakeImage = (isCamera = false) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          uploadProfilePhoto(imgSrc);
          dispatchProfile({
            type: SET_PHOTO,
            payload: imgSrc.path,
          });
        } else {
          console.log('User Cancel');
        }
      });
    } else {
      GetImageFromGallery((imgSrc) => {
        if (imgSrc) {
          uploadProfilePhoto(imgSrc);
          dispatchProfile({
            type: SET_PHOTO,
            payload: imgSrc.path,
          });
        } else {
          console.log('User Cancle');
        }
      });
    }
  };
  const GetImageFromGallery = (imgSrc) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      imgSrc(false);
      return;
    }
    ImagePicker.openPicker({
      mediaType: 'photo',
      width: 600,
      height: 600,
      cropping: true,
    })
      .then((image) => {
        imgSrc(image);
      })
      .catch((e) => {
        imgSrc(false);
      });
  };
  const GetImageFromCamera = (imgSrc) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      imgSrc(false);
      return;
    }
    ImagePicker.openCamera({
      mediaType: 'photo',
      width: 600,
      height: 600,
      cropping: true,
    })
      .then((image) => {
        imgSrc(image);
      })
      .catch((e) => {
        imgSrc(false);
      });
  };
  const TakeSignature = (isCamera) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          updateImage(imgSrc, SIGNATURE_URL, 'signature', (is_done) => {
            if (is_done) {
              dispatchProfile({
                type: SET_SIGNATURE,
                payload: imgSrc.path,
              });
              showMessage({
                message: 'Signature update successfully done',
                type: 'success',
              });
            }
          });
        } else {
          console.log('User Cancel');
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          updateImage(image, SIGNATURE_URL, 'signature', (is_done) => {
            if (is_done) {
              dispatchProfile({
                type: SET_SIGNATURE,
                payload: image.path,
              });
              showMessage({
                message: 'Signature update successfully done',
                type: 'success',
              });
            }
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const TakeOfficeFrontId = (isCamera = false) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          updateImage(imgSrc, OFFICE_ID_URL, 'oid_front', (is_done) => {
            if (is_done) {
              dispatchService({
                type: SET_OFFICE_FRONT,
                payload: imgSrc.path,
              });
            }
          });
        } else {
          console.log('User Cancel');
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          updateImage(image, OFFICE_ID_URL, 'oid_front', (is_done) => {
            if (is_done) {
              dispatchService({
                type: SET_OFFICE_FRONT,
                payload: image.path,
              });
            }
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const TakeNIDFront = (isCamera) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          updateImage(imgSrc, PROFILE_DOC_URL, 'nid_front', (is_done) => {
            if (is_done) {
              dispatchProfile({
                type: SET_NID_FRONT,
                payload: imgSrc.path,
              });
              showMessage({
                message: 'NID front image update success',
                type: 'success',
              });
            }
          });
        } else {
          console.log('User Cancel');
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          updateImage(image, PROFILE_DOC_URL, 'nid_front', (is_done) => {
            if (is_done) {
              dispatchProfile({
                type: SET_NID_FRONT,
                payload: image.path,
              });
              showMessage({
                message: 'NID front image update success',
                type: 'success',
              });
            }
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const TakeNIDBack = (isCamera) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          updateImage(imgSrc, PROFILE_DOC_URL, 'nid_back', (is_done) => {
            if (is_done) {
              dispatchProfile({
                type: SET_NID_BACK,
                payload: imgSrc.path,
              });
              showMessage({
                message: 'NID back image update success',
                type: 'success',
              });
            }
          });
        } else {
          console.log('User Cancel');
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          updateImage(image, PROFILE_DOC_URL, 'nid_back', (is_done) => {
            if (is_done) {
              dispatchProfile({
                type: SET_NID_BACK,
                payload: image.path,
              });
              showMessage({
                message: 'NID back image update success',
                type: 'success',
              });
            }
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const TakeTINImage = (isCamera) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          updateImage(imgSrc, PROFILE_DOC_URL, 'tin_doc', (is_done) => {
            if (is_done) {
              getUserProfile();
            }
          });
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          updateImage(image, PROFILE_DOC_URL, 'tin_doc', (is_done) => {
            if (is_done) {
              getUserProfile();
            }
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const TakeSalaryStatementImage = (isCamera) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          dispatchApplication({
            type: SET_SALARY_STATEMENT,
            payload: imgSrc,
          });
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          dispatchApplication({
            type: SET_SALARY_STATEMENT,
            payload: image,
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const TakeImages = (name, type, txt = false) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    ImagePicker.openPicker({
      mediaType: 'photo',
      width: 600,
      height: 600,
      cropping: true,
    })
      .then((image) => {
        updateImage(image, PROFILE_DOC_URL, name, (is_done) => {
          if (is_done) {
            if (txt) {
              showMessage({
                message: txt,
                type: 'success',
              });
            }
            dispatchProfile({
              type: type,
              payload: image.path,
            });
          }
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const TakeApplicationImg = (name, type) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    ImagePicker.openPicker({
      mediaType: 'photo',
      width: 600,
      height: 600,
      cropping: true,
    })
      .then((image) => {
        dispatchApplication({
          type: type,
          payload: image,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const TakeOfficeBackId = (isCamera = false) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (isCamera) {
      GetImageFromCamera((imgSrc) => {
        if (imgSrc) {
          updateImage(imgSrc, OFFICE_ID_URL, 'oid_back', (is_done) => {
            if (is_done) {
              dispatchService({
                type: SET_OFFICE_BACK,
                payload: imgSrc.path,
              });
            }
          });
        } else {
          console.log('User Cancel');
        }
      });
    } else {
      ImagePicker.openPicker({
        mediaType: 'photo',
        width: 600,
        height: 600,
        cropping: true,
      })
        .then((image) => {
          updateImage(image, OFFICE_ID_URL, 'oid_back', (is_done) => {
            if (is_done) {
              dispatchService({
                type: SET_OFFICE_BACK,
                payload: image.path,
              });
            }
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const updateImage = (image, url, name, done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    makeLoading(true);
    var data = new FormData();
    data.append(name, {
      uri: image.path,
      type: 'image/jpeg',
      name: 'avatar_',
      user_id: profileState.user_id,
    });
    http
      .postDataToken(url, data, userState.token)
      .then((response) => {
        done(true);
      })
      .catch((e) => {
        if (e.response.status === 422) {
          showMessage({
            message: e.response.data?.errors[
              Object.keys(e.response.data?.errors)[0]
            ].join(''),
            type: 'danger',
          });
        }
        done(false);
      })
      .then(() => {
        makeLoading(false);
      });
  };
  const changeCompanyName = (value) => {
    dispatchService({
      type: SET_OFFICE_NAME,
      payload: value,
    });
  };
  const changeCompanyAddress = (value) => {
    dispatchService({
      type: SET_OFFICE_ADDRESS,
      payload: value,
    });
  };
  const changeDesignation = (value) => {
    dispatchService({
      type: SET_DESIGNATION,
      payload: value,
    });
  };
  const changeJoiningDate = (value) => {
    dispatchService({
      type: SET_JOINING_DATE,
      payload: value,
    });
  };
  const changeSalary = (value) => {
    dispatchService({
      type: SET_SALARY,
      payload: value,
    });
  };
  const changeExperience = (value) => {
    dispatchService({
      type: SET_EXPERIENCE,
      payload: value,
    });
  };
  const profileUpdate = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (
      checkProfile({
        photo: profileState.photo,
        signature: profileState.signature,
        nid_front: profileState.nid_front,
        nid_back: profileState.nid_back,
        full_name: profileState.full_name,
        father_name: profileState.father_name,
        mother_name: profileState.mother_name,
        gender: profileState.gender,
        dob: profileState.dob,
        religion: profileState.religion,
        present_address: profileState.present_address,
        permanent_address: profileState.permanent_address,
        email: profileState.contact_email,
        mobile: profileState.contact_no,
        nid: profileState.nid,
        tin: profileState.tin,
      })
    ) {
      makeLoading(true);
      var data = new FormData();
      data.append('full_name', profileState.full_name);
      data.append('father_name', profileState.father_name);
      data.append('mother_name', profileState.mother_name);
      data.append('gender', profileState.gender);
      data.append('date_of_birth', profileState.dob);
      data.append('present_address', profileState.present_address);
      data.append('permanent_address', profileState.permanent_address);
      data.append('religion', profileState.religion);
      data.append('nationality', profileState.nationality);
      data.append('contact_email', profileState.contact_email);
      data.append('contact_no', profileState.contact_no);
      data.append('profession', profileState.profession);
      data.append('tin', profileState.tin);
      data.append('nid', profileState.nid);
      http
        .postDataToken(PROFILE_UPDATE_URL, data, userState.token)
        .then((response) => {
          showMessage({
            message: 'Profile has been updated successfully',
            type: 'success',
          });
          done(true);
        })
        .catch((e) => {
          if (e.response.status === 422) {
            showMessage({
              message: e.response.data?.errors[
                Object.keys(e.response.data?.errors)[0]
              ].join(''),
              type: 'danger',
            });
          }
          done(false);
        })
        .then(() => {
          makeLoading(false);
        });
      done(false);
    }
  };
  const applySalary = (done) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    if (
      checkApplication({
        nid: profileState.nid,
        tin: profileState.tin,
        salary: serviceState.salary,
        company: serviceState.company,
        address: serviceState.address,
        designation: serviceState.designation,
        joining_date: serviceState.joining_date,
        experience: serviceState?.experience,
        salary_expected_date: applicationState.salary_expected_date,
        details: applicationState.details,
        referrerName: applicationState.referrerName,
        referrerContactNo: applicationState.referrerContactNo,
        referrerNID: applicationState.referrerNID,
        referrerProfessionalDetails:
          applicationState.referrerProfessionalDetails,
        salary_statement: applicationState.salary_statement,
        tin_doc: profileState.tin_doc,
      })
    ) {
      var data = new FormData();
      data.append('serviceId', '1');
      data.append('nid', profileState.nid);
      data.append('tin', profileState.tin);
      data.append('salary', serviceState.salary);
      data.append('company', serviceState.company);
      data.append('address', serviceState.address);
      data.append('designation', serviceState.designation);
      data.append('experience', serviceState?.experience);
      data.append('joining_date', serviceState.joining_date);
      data.append(
        'salary_expected_date',
        applicationState.salary_expected_date,
      );
      data.append('details', applicationState.details);
      data.append('referredBy', 'Info');
      applicationState.referrerName.forEach((refName) => {
        data.append('referrerName[]', refName);
      });
      applicationState.referrerContactNo.forEach((refContactNo) => {
        data.append('referrerContactNo[]', refContactNo);
      });
      applicationState.referrerNID.forEach((refNid) => {
        data.append('referrerNID[]', refNid);
      });
      applicationState.referrerProfessionalDetails.forEach(
        (refProfessionalDetails) => {
          data.append('referrerProfessionalDetails[]', refProfessionalDetails);
        },
      );
      data.append('salary_statement', {
        uri: applicationState.salary_statement.path,
        type: 'image/jpeg',
        name: 'avatar_',
        user_id: profileState.user_id,
      });
      makeLoading(true);
      http
        .postDataToken(APPLICATION_URL, data, userState.token)
        .then((response) => {
          getUserProfile();
          updateApplication(initialState.application);
          showMessage({
            message: response.data?.message,
            type: 'success',
          });
          done(true);
        })
        .catch((e) => {
          if (e.response?.status === 422) {
            showMessage({
              message: e.response.data?.errors[
                Object.keys(e.response.data?.errors)[0]
              ].join(''),
              type: 'danger',
            });
          } else {
            showMessage({
              message: 'Something went wrong',
              type: 'danger',
            });
          }
          done(false);
        })
        .then(() => {
          makeLoading(false);
        });
    } else {
      done(false);
    }
  };
  const uploadProfilePhoto = (image) => {
    if (loadingState.is_offline) {
      showMessage({
        message: 'Check your internet connection',
        type: 'danger',
      });
      return;
    }
    const data = new FormData();
    data.append('photo', {
      uri: image.path,
      type: 'image/jpeg', // or photo.type
      name: 'avatar_',
      user_id: profileState.user_id,
    });
    http
      .postDataToken(PROFILE_PHOTO_URL, data, userState.token)
      .then((response) => {
        showMessage({
          message: 'Profile photo update successfully done',
          type: 'success',
        });
      })
      .catch((e) => {
        console.log(e.response.data);
      })
      .then(() => {
        getUserProfile();
      });
  };
  useEffect(() => {
    getJson('token')
      .then((token) => {
        if (token) {
          dispatchUser({
            type: USER_SIGN_IN,
            payload: token,
          });
        }
      })
      .then(() => {
        dispatchLoading({
          type: IS_STARTUP,
          payload: true,
        });
      });
  }, []);
  return (
    <AppContext.Provider
      value={{
        loading: loadingState,
        user: userState,
        profile: profileState,
        service: serviceState,
        application: applicationState,
        applications: applicationsState,
        transactions: transactionsState,
        getUserTransactions,
        cancelApplication,
        getUserApplications,
        getServiceData,
        applySalary,
        TakeApplicationImg,
        TakeImages,
        getUserProfile,
        resetUserEmailNPass,
        changeCompanyAddress,
        changeDesignation,
        changeJoiningDate,
        changeExperience,
        changeSalary,
        makeLoading,
        makeOffline,
        changeUserData,
        submitLogin,
        submitRegistration,
        resetUserData,
        updateProfile,
        updateApplication,
        TakeImage,
        TakeSignature,
        profileUpdate,
        getServiceInfo,
        TakeOfficeFrontId,
        TakeOfficeBackId,
        changeCompanyName,
        submitServices,
        sendResetMail,
        resetPassword,
        submitContactUs,
        TakeNIDFront,
        TakeNIDBack,
        TakeTINImage,
        TakeSalaryStatementImage,
      }}>
      {children}
    </AppContext.Provider>
  );
};
