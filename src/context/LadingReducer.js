import {IS_STARTUP, LOADING, SET_OFFLINE} from './ReducerConst';

const LoadingReducer = (state, action) => {
  switch (action.type) {
    case LOADING:
      return {...state, is_loading: action.payload};
    case IS_STARTUP:
      return {...state, apps_loaded: action.payload};
    case SET_OFFLINE:
      return {...state, is_offline: action.payload};
    default:
      return state;
  }
};
export default LoadingReducer;
