import {SET_APPLICATIONS} from './ReducerConst';

const ApplicationsReducer = (state, action) => {
  switch (action.type) {
    case SET_APPLICATIONS:
      return action.payload;
    default:
      return state;
  }
};
export default ApplicationsReducer;
