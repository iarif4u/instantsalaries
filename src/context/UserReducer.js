import {
  CHANGE_EMAIL,
  CHANGE_MOBILE,
  CHANGE_NAME,
  CHANGE_PASSWORD,
  CHANGE_REMEMBER,
  CHANGE_TERMS,
  PASSWORD_CONFIRMATION,
  USER_SIGN_IN,
  SIGN_OUT,
  SET_TOKEN,
  SET_USER,
} from './ReducerConst';

const UserReducer = (state, action) => {
  switch (action.type) {
    case CHANGE_NAME:
      return {...state, name: action.payload};
    case CHANGE_EMAIL:
      return {...state, email: action.payload};
    case CHANGE_PASSWORD:
      return {...state, password: action.payload};
    case CHANGE_MOBILE:
      return {...state, contact_no: action.payload};
    case PASSWORD_CONFIRMATION:
      return {...state, password_confirmation: action.payload};
    case CHANGE_REMEMBER:
      return {...state, remember: action.payload};
    case CHANGE_TERMS:
      return {...state, terms_and_condition: action.payload};
    case USER_SIGN_IN:
      return {...state, is_signed_in: true, token: action.payload};
    case SET_TOKEN:
      return {...state, token: action.payload};
    case SET_USER:
      return {...state, ...action.payload};
    case SIGN_OUT:
      return action.payload;
    default:
      return state;
  }
};
export default UserReducer;
