import React, {useState, useContext, createRef} from 'react';
import {
  ScrollView,
  Image,
  StyleSheet,
  Text,
  Linking,
  TouchableOpacity,
  View,
  ToastAndroid,
} from 'react-native';
import {
  CONTACT_US_IMG,
  DISPLAY_WIDTH,
  SUPPORT_EMAIL,
  SUPPORT_NUMBER,
  TAWK_TO_LINK,
} from '../lib/const';
import Input from './Form/Input';
import {AppContext} from '../context/AppContext';
const ContactUs = ({navigation}) => {
  const {user, profile, submitContactUs} = useContext(AppContext);
  const nameInputRef = createRef(null);
  const phoneInputRef = createRef(null);
  const subjectInputRef = createRef(null);
  const messageInputRef = createRef(null);
  const [name, setName] = useState(user.name);
  const [phone, setPhone] = useState(profile.contact_no);
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  return (
    <ScrollView keyboardShouldPersistTaps={'handled'}>
      <Image
        resizeMode="contain"
        style={{
          width: DISPLAY_WIDTH,
          top: 0,
          height: 250,
          backgroundColor: '#fff',
        }}
        source={CONTACT_US_IMG}
      />
      <View style={styles.maincontent}>
        <Text style={styles.sectionhead}>Location</Text>
        <Text style={styles.sectiontext}>
          5th Floor, Assurance Nazir Tower, 65/B Kemal Atartuk Avenue, Banani
          1213 Dhaka, Dhaka Division, Bangladesh.
        </Text>
        <View style={styles.getintouch}>
          {user.name.length === 0 && (
            <Input
              inputRef={nameInputRef}
              onSubmitEditing={phoneInputRef}
              value={name}
              onChangeText={(value) => setName(value)}
              label={'Name'}
            />
          )}
          {profile?.contact_no?.length !== 11 && (
            <Input
              inputRef={phoneInputRef}
              onSubmitEditing={subjectInputRef}
              value={phone}
              label={'Phone'}
              onChangeText={(value) => setPhone(value)}
              keyboardType="phone-pad"
            />
          )}
          <Input
            inputRef={subjectInputRef}
            onSubmitEditing={messageInputRef}
            value={subject}
            onChangeText={(value) => setSubject(value)}
            label={'Subject'}
          />
          <Input
            value={message}
            inputRef={messageInputRef}
            onChangeText={(value) => setMessage(value)}
            label={'Message'}
            multiline={true}
            numberOfLines={4}
          />
          <TouchableOpacity
            onPress={() => {
              submitContactUs(
                {
                  name: name,
                  phone: phone,
                  subject: subject,
                  message: message,
                },
                (done) => {
                  if (done) {
                    setName(user.name);
                    setPhone(profile.contact_no);
                    setSubject('');
                    setMessage('');
                    navigation.navigate('Dashboard');
                  }
                },
              );
            }}
            style={styles.btn}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>
              Send Message
            </Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.support}>
          Need help? Get in touch with our support team.
        </Text>
        <View style={styles.buttoncom}>
          <View style={{...styles.buttonarea}}>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(`tel:${SUPPORT_NUMBER}`);
              }}>
              <View style={styles.buttonabs}>
                <Text style={styles.buttonicontext}>Customer Care Number</Text>
                <Text style={styles.buttonicontext}>01921222111</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{...styles.buttonarea}}>
            <View style={styles.buttonabs}>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL(`mailto:${SUPPORT_EMAIL}`);
                }}>
                <Text style={styles.buttonicontext}>Customer Care Queries</Text>
                <Text style={styles.buttonicontext}>
                  care@instantsalaries.com
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{...styles.buttonarea}}>
            <View style={styles.buttonabs}>
              <TouchableOpacity
                onPress={() => {
                  Linking.canOpenURL(TAWK_TO_LINK).then((supported) => {
                    if (supported) {
                      Linking.openURL(TAWK_TO_LINK);
                    } else {
                      ToastAndroid.show(
                        'Can not open browser',
                        ToastAndroid.SHORT,
                      );
                    }
                  });
                }}>
                <Text style={styles.buttonicontext}>Live Chat</Text>
                <Text style={styles.buttonicontext}>Start Now</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default ContactUs;

const styles = StyleSheet.create({
  header: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 50,
    marginBottom: 90,
    borderBottomWidth: 4,
    borderBottomColor: 'white',
  },
  bgimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  bgyellow: {
    backgroundColor: '#f47f20cb',
  },
  sectionhead: {
    marginTop: 5,
    marginBottom: 5,
    fontWeight: 'bold',
    fontSize: 25,
  },
  sectiontext: {
    marginBottom: 3,
    fontSize: 16,
    textAlign: 'justify',
  },
  maincontent: {
    marginLeft: 10,
    marginRight: 10,
  },
  getintouch: {
    margin: 10,
    borderRadius: 2,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
    backgroundColor: '#f5f5f5',
  },
  textinput: {
    borderWidth: 0.5,
    margin: 5,
  },
  btn: {
    backgroundColor: '#f48120',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 5,
    alignItems: 'center',
  },
  support: {
    marginTop: 20,
    marginBottom: 15,
    alignSelf: 'center',
    borderBottomWidth: 2,
    borderBottomColor: '#f48120',
  },
  buttonicontext: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#f48120',
  },
  buttonabs: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonarea: {
    flex: 1,
    //   width:width*0.3,
    margin: 10,
    paddingTop: 12,
    paddingBottom: 17,
    borderRadius: 50,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
    backgroundColor: '#f5f5f5',
  },
  buttoncom: {
    margin: 20,
  },
});
