import {APPS_COLOR, TEXT_BLACK} from '../../lib/const';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';

const BackHeader = ({navigation, headText}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginBottom: 15,
        paddingTop: 10,
        paddingBottom: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomColor: APPS_COLOR,
      }}>
      <TouchableOpacity onPress={() => navigation.navigate('HomeDashboard')}>
        <Icon name="arrow-back" size={22} color="#000" />
      </TouchableOpacity>
      <Text
        style={{
          color: TEXT_BLACK,
          fontSize: 20,
          marginBottom: 4,
        }}>
        {headText}
      </Text>
    </View>
  );
};
export default BackHeader;
