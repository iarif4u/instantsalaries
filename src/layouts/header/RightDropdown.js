import React, {useContext, useState} from 'react';
import {AppContext} from '../../context/AppContext';
import {View} from 'react-native';
import Menu, {MenuDivider, MenuItem} from 'react-native-material-menu';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TEXT_BLACK} from '../../lib/const';

const RightDropdown = () => {
  const {resetUserData} = useContext(AppContext);
  const [menuRef, setMenuRef] = useState(null);

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Menu
        ref={(ref) => {
          setMenuRef(ref);
        }}
        button={
          <TouchableOpacity
            containerStyle={{padding: 10}}
            onPress={() => {
              menuRef.show();
            }}>
            <Icon name="more-vert" size={24} color={TEXT_BLACK} />
          </TouchableOpacity>
        }>
        <MenuItem onPress={() => menuRef.hide()}>Menu item 1</MenuItem>
        <MenuItem onPress={() => menuRef.hide()}>Menu item 2</MenuItem>
        <MenuItem onPress={() => menuRef.hide()} disabled>
          Menu item 3
        </MenuItem>
        <MenuDivider />
        <MenuItem
          onPress={() => {
            menuRef.hide();
            resetUserData();
          }}>
          Log Out
        </MenuItem>
      </Menu>
    </View>
  );
};

export default RightDropdown;
