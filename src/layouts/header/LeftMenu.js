import React from 'react';
import {TouchableOpacity} from 'react-native';
import {DrawerActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TEXT_BLACK} from '../../lib/const';

const LeftMenu = ({navigation, color = TEXT_BLACK}) => {
  return (
    <TouchableOpacity
      style={{marginRight: 10}}
      onPress={() => {
        navigation.dispatch(DrawerActions.toggleDrawer());
      }}>
      <Icon name="menu" size={35} color={color} />
    </TouchableOpacity>
  );
};

export default LeftMenu;
