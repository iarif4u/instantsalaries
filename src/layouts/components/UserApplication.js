import {APPS_COLOR} from '../../lib/const';
import {Text, View} from 'react-native';
import React, {useContext} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {AppContext} from '../../context/AppContext';
const UserApplication = ({navigation, application}) => {
  const getStatusBackground = (status) => {
    switch (status) {
      case 'REJECTED':
        return 'red';
      case 'CANCELED':
        return 'red';
      case 'PENDING':
        return 'yellow';
      case 'APPROVED':
        return 'blue';
      default:
        return 'blue';
    }
  };
  const getStatus = (str) => {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  };
  const {cancelApplication} = useContext(AppContext);
  return (
    <View
      style={{
        borderWidth: 0.5,
        borderColor: 'gray',
        marginTop: 10,
        borderRadius: 5,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
        }}>
        <View>
          <View>
            <Text
              style={{
                color: 'gray',
                fontWeight: 'bold',
                fontSize: 17,
                paddingBottom: 5,
              }}>
              Applied On:
            </Text>
            <Text
              style={{
                color: '#2f2222',
                fontWeight: 'bold',
                fontSize: 16,
                paddingBottom: 5,
              }}>
              {application.created_at}
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text
              style={{
                color: 'gray',
                fontWeight: 'bold',
                fontSize: 17,
                paddingBottom: 5,
              }}>
              Referral Code
            </Text>
            <Text
              style={{
                color: '#2f2222',
                fontWeight: 'bold',
                fontSize: 16,
                paddingBottom: 5,
              }}>
              {application.referral_code}
            </Text>
          </View>
        </View>
        <View style={{justifyContent: 'space-between'}}>
          <View>
            <Text
              style={{
                color: 'gray',
                fontWeight: 'bold',
                fontSize: 17,
                paddingBottom: 5,
              }}>
              Expected Salary On:
            </Text>
            <Text
              style={{fontSize: 18, fontWeight: 'bold', textAlign: 'right'}}>
              {application.salary_expected_date} of month
            </Text>
          </View>
          <View style={{alignSelf: 'flex-end'}}>
            <Text
              style={{
                fontSize: 16,
                padding: 6,
                borderRadius: 5,
                color: '#fff',
                backgroundColor: getStatusBackground(application.status),
              }}>
              {getStatus(application.status)}
            </Text>
          </View>
        </View>
      </View>
      {(application.application_fee === null ||
        application.application_fee === 0) &&
        application.status.toUpperCase() === 'APPROVED' && (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ApplicationFee', {
                referralCode: application.referral_code,
              });
            }}
            style={{
              alignItems: 'center',
              backgroundColor: APPS_COLOR,
              padding: 10,
              borderBottomLeftRadius: 5,
              borderBottomRightRadius: 5,
            }}>
            <Text style={{color: '#ffffff'}}>Pay Fees</Text>
          </TouchableOpacity>
        )}
      {application.transaction_query === 'ASKED' &&
        application.status.toUpperCase() === 'APPROVED' && (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('TransactionMethod', {
                serviceId: application.id,
              });
            }}
            style={{
              alignItems: 'center',
              backgroundColor: 'green',
              padding: 10,
              borderBottomLeftRadius: 5,
              borderBottomRightRadius: 5,
            }}>
            <Text style={{color: '#ffffff'}}>Provide Transaction Method</Text>
          </TouchableOpacity>
        )}
      {['APPLIED', 'VERIFIED', 'ON_VERIFICATION'].includes(
        application.status.toUpperCase(),
      ) && (
        <TouchableOpacity
          onPress={() => {
            cancelApplication(application.id);
          }}
          style={{
            alignItems: 'center',
            backgroundColor: 'red',
            padding: 10,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
          }}>
          <Text style={{color: '#ffffff'}}>Cancel</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};
export default UserApplication;
