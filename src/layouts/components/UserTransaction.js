import {TEXT_BLACK} from '../../lib/const';
import {Text, View} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';

const UserTransaction = ({transaction}) => {
  return (
    <View
      style={{
        borderWidth: 0.5,
        borderColor: 'gray',
        marginTop: 10,
        borderRadius: 5,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
        }}>
        <View>
          <Text
            style={{
              color: TEXT_BLACK,
              paddingBottom: 5,
            }}>
            Amount : {transaction.amount}
          </Text>
          <Text
            style={{
              color: TEXT_BLACK,
            }}>
            Application Date
          </Text>
          <Text>{transaction.updated_at}</Text>
          <Text style={{fontWeight: 'bold'}}>
            {transaction.transaction_type}
          </Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              color: 'black',
            }}>
            Referral Code
          </Text>
          <Text
            style={{
              color: 'black',
            }}>
            {transaction.application_code}
          </Text>
          <Text
            style={{
              color: 'red',
              marginRight: 5,
            }}>
            {transaction.status}
          </Text>
        </View>
      </View>
    </View>
  );
};
export default UserTransaction;
