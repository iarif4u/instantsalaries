import React, {useContext} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ToastAndroid,
  Linking,
} from 'react-native';
import apply from '../../assets/apply.png';
import service from '../../assets/service.png';
import customer from '../../assets/customer.png';
import check from '../../assets/check.png';
import transaction from '../../assets/transaction.png';
import {AppContext} from '../../context/AppContext';
import {isApplicable} from '../../lib/funcs';
import {showMessage} from 'react-native-flash-message';
import Icon from 'react-native-vector-icons/FontAwesome';
import {SUPPORT_NUMBER} from '../../lib/const';

const Menu = ({navigation}) => {
  const {getServiceInfo, getServiceData, profile, getUserProfile} = useContext(
    AppContext,
  );
  return (
    <>
      <View style={styles.buttoncom}>
        <View style={{...styles.buttonarea, backgroundColor: '#8C96FD'}}>
          <TouchableOpacity
            style={styles.buttonabs}
            onPress={() => {
              navigation.navigate('ProfileUpdate');
            }}>
            <Icon
              style={{alignSelf: 'center'}}
              name="user-o"
              color="#fff"
              size={34}
            />
            <Text style={styles.buttonicontext}>Update</Text>
            <Text style={styles.buttonicontext}>Your Profile</Text>
          </TouchableOpacity>
        </View>
        <View style={{...styles.buttonarea, backgroundColor: '#4EC38F'}}>
          <TouchableOpacity
            onPress={() => {
              getServiceInfo((is_done) => {
                if (is_done) {
                  navigation.navigate('ServiceUpdate');
                }
              });
            }}
            style={styles.buttonabs}>
            <Image source={service} style={styles.buttoniconsmall} />
            <Text style={styles.buttonicontext}>Employee</Text>
            <Text style={styles.buttonicontext}>Information</Text>
          </TouchableOpacity>
        </View>
        <View style={{...styles.buttonarea, backgroundColor: '#FD7648'}}>
          <TouchableOpacity
            onPress={() => {
              if (profile.can_apply) {
                getUserProfile();
                getServiceData((user_service) => {
                  if (user_service) {
                    if (
                      isApplicable({
                        photo: profile.photo,
                        signature: profile.signature,
                        nid_front: profile.nid_front,
                        nid_back: profile.nid_back,
                        nid: profile.nid,
                        tin: profile.tin,
                        salary: user_service.salary,
                        company: user_service.company,
                        address: user_service.address,
                        designation: user_service.designation,
                        joining_date: user_service.joining_date,
                        experience: user_service.experience,
                      })
                    ) {
                      navigation.navigate('SalaryApplication');
                    }
                  } else {
                    showMessage({
                      message: 'Please update your information',
                      type: 'danger',
                      position: 'center',
                    });
                  }
                });
              } else {
                showMessage({
                  message: 'You already have a processing application',
                  type: 'danger',
                });
              }
            }}
            style={styles.buttonabs}>
            <Image source={apply} style={styles.buttoniconsmall} />
            <Text style={styles.buttonicontext}>Apply</Text>
            <Text style={styles.buttonicontext}>For Salary</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{...styles.buttoncom, marginTop: -10}}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Applications');
          }}
          style={{...styles.buttonarea, backgroundColor: '#1589CD'}}>
          <View style={styles.buttonabs}>
            <Image source={check} style={styles.buttoniconsmall} />
            <Text style={styles.buttonicontext}>View</Text>
            <Text style={styles.buttonicontext}>Applications</Text>
          </View>
        </TouchableOpacity>
        <View style={{...styles.buttonarea, backgroundColor: '#B036F5'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Transactions');
            }}
            style={styles.buttonabs}>
            <Image source={transaction} style={styles.buttoniconsmall} />
            <Text style={styles.buttonicontext}>Check</Text>
            <Text style={styles.buttonicontext}>Transactions</Text>
          </TouchableOpacity>
        </View>
        <View style={{...styles.buttonarea, backgroundColor: '#E5AE6D'}}>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${SUPPORT_NUMBER}`);
            }}
            style={styles.buttonabs}>
            <Image source={customer} style={styles.buttoniconsmall} />
            <Text style={styles.buttonicontext}>Customer</Text>
            <Text style={styles.buttonicontext}>Service</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default Menu;

const styles = StyleSheet.create({
  buttoncom: {
    flex: 1,
    flexDirection: 'row',
    margin: 15,
  },
  buttonarea: {
    flex: 1,
    margin: 5,
    paddingTop: 12,
    paddingBottom: 17,
    borderRadius: 2,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 15,
  },
  buttonicon: {
    resizeMode: 'cover',
    alignSelf: 'center',
  },
  buttonabs: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttoniconsmall: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
  },
  buttonicontext: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 14,
  },
});
