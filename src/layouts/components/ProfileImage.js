import {AVATAR, DISPLAY_HEIGHT, LOGO, ROW_WIDTH} from '../../lib/const';
import {Image, View} from 'react-native';
import React from 'react';

const ProfileImage = () => {
  return (
    <View
      style={{
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        width: 150,
        height: 10,
        borderWidth: 3,
      }}>
      <Image
        style={{width: '100%', height: 100}}
        resizeMode={'center'}
        source={AVATAR}
      />
    </View>
  );
};
export default ProfileImage;
