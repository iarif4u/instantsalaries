import React from 'react';
import {Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import RBSheet from 'react-native-raw-bottom-sheet';

const CameraAndGallery = ({refRBSheet, TakeImage}) => {
  return (
    <RBSheet
      ref={refRBSheet}
      closeOnDragDown={true}
      closeOnPressMask={true}
      height={200}
      customStyles={{
        wrapper: {
          backgroundColor: 'transparent',
        },
        draggableIcon: {
          backgroundColor: '#000',
        }
      }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',

          borderColor: '#acabab',
          borderTopWidth: 0.5,
        }}>
        <View style={{borderColor: 'gray', borderWidth: 1, padding: 15}}>
          <TouchableHighlight
            activeOpacity={1}
            onPress={() => {
              TakeImage(true);
              refRBSheet.current.close();
            }}>
            <>
              <Icon
                style={{alignSelf: 'center'}}
                color={'#000'}
                name="camera"
                size={34}
              />
              <Text style={{color: '#000'}}>Camera</Text>
            </>
          </TouchableHighlight>
        </View>
        <View style={{borderColor: 'gray', borderWidth: 1, padding: 15}}>
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => {
              TakeImage(false);
              refRBSheet.current.close();
            }}>
            <>
              <Icon style={{alignSelf: 'center'}} name="image" size={34} />
              <Text>Open Gallery</Text>
            </>
          </TouchableOpacity>
        </View>
      </View>
    </RBSheet>
  );
};
export default CameraAndGallery;
