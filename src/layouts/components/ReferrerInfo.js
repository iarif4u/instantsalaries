import React, {createRef, useContext, useState} from 'react';
import Input from '../Form/Input';
import {AppContext} from '../../context/AppContext';
import {
  Collapse,
  CollapseBody,
  CollapseHeader,
} from 'accordion-collapse-react-native';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
const ReferrerInfo = ({index, removeRef}) => {
  const [expanded, setExpanded] = useState(false);
  const {application, updateApplication} = useContext(AppContext);
  const referrerInputRef = createRef(null);
  const referrerContactInputRef = createRef(null);
  const referrerNidInputRef = createRef(null);
  const referrerDetailsInputRef = createRef(null);
  const changeReferrerName = (value) => {
    application.referrerName[index] = value;
    updateApplication({...application, ...application.referrerName});
  };
  const changeReferrerContactNo = (value) => {
    application.referrerContactNo[index] = value;
    updateApplication({...application, ...application.referrerContactNo});
  };
  const changeReferrerNID = (value) => {
    application.referrerNID[index] = value;
    updateApplication({...application, ...application.referrerNID});
  };
  const changeProfessionalDetails = (value) => {
    application.referrerProfessionalDetails[index] = value;
    updateApplication({
      ...application,
      ...application.referrerProfessionalDetails,
    });
  };
  const removeReferrer = () => {
    application.referrerName = application.referrerName.filter(
      (ref, i) => i !== index,
    );
    application.referrerContactNo = application.referrerContactNo.filter(
      (ref, i) => i !== index,
    );
    application.referrerNID = application.referrerNID.filter(
      (ref, i) => i !== index,
    );
    application.referrerProfessionalDetails = application.referrerProfessionalDetails.filter(
      (ref, i) => i !== index,
    );
    updateApplication({
      ...application,
      ...application.referrerProfessionalDetails,
    });
    updateApplication({...application, ...application.referrerName});
    updateApplication({...application, ...application.referrerContactNo});
    updateApplication({...application, ...application.referrerNID});
    removeRef(index);
  };
  return (
    <Collapse
      onToggle={(isExpanded) => setExpanded(isExpanded)}
      isExpanded={expanded}
      style={{
        padding: 10,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 10,
      }}>
      <CollapseHeader>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text>Referrer {index + 1}</Text>
          <Icon
            name={expanded ? 'ios-arrow-down-sharp' : 'ios-arrow-forward'}
            size={20}
            color="#000"
          />
        </View>
      </CollapseHeader>
      <CollapseBody style={{marginTop: 10}}>
        <Input
          inputRef={referrerInputRef}
          onSubmitEditing={referrerContactInputRef}
          onChangeText={changeReferrerName}
          value={application.referrerName[index]}
          placeholder="Referrer Name"
        />
        <Input
          inputRef={referrerContactInputRef}
          onSubmitEditing={referrerNidInputRef}
          onChangeText={changeReferrerContactNo}
          value={application.referrerContactNo[index]}
          keyboardType="phone-pad"
          placeholder="Referrer Contact No"
        />
        <Input
          inputRef={referrerNidInputRef}
          onSubmitEditing={referrerDetailsInputRef}
          onChangeText={changeReferrerNID}
          value={application.referrerNID[index]}
          placeholder="Referrer NID"
          keyboardType="numeric"
        />
        <Input
          onChangeText={changeProfessionalDetails}
          value={application.referrerProfessionalDetails[index]}
          multiline={true}
          numberOfLines={3}
          inputRef={referrerDetailsInputRef}
          placeholder="Referrer Professional Details"
        />
        {index > 1 && (
          <TouchableOpacity
            style={{backgroundColor: 'red', padding: 5, borderRadius: 5}}
            onPress={removeReferrer}>
            <Text style={{color: '#fff', textAlign: 'center'}}>
              Remove Referrer
            </Text>
          </TouchableOpacity>
        )}
      </CollapseBody>
    </Collapse>
  );
};
export default ReferrerInfo;
