import React from 'react';
import {Image, View} from 'react-native';
import {LOGO} from '../../lib/const';
import LeftMenu from '../header/LeftMenu';

const DrawerHeader = ({navigation}) => {
  return (
    <View
      style={{
        paddingTop: 8,
        paddingLeft: 10,
        paddingBottom: 8,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
      }}>
      <Image
        style={{width: '35%', height: 45}}
        resizeMode={'stretch'}
        source={LOGO}
      />
      <LeftMenu navigation={navigation} />
    </View>
  );
};

export default DrawerHeader;
