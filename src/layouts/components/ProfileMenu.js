import React, {useContext} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../../context/AppContext';
import {isApplicable} from '../../lib/funcs';
import {showMessage} from 'react-native-flash-message';

const ProfileMenu = ({navigation}) => {
  const {getServiceInfo, getServiceData, profile, getUserProfile} = useContext(
    AppContext,
  );
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{...styles.btn, backgroundColor: '#4EC38F'}}
        onPress={() => {
          navigation.navigate('ProfileUpdate');
        }}>
        <Text style={styles.text}>Profile Update</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{...styles.btn, backgroundColor: '#8C96FD'}}
        onPress={() => {
          getServiceInfo((is_done) => {
            if (is_done) {
              navigation.navigate('ServiceUpdate');
            }
          });
        }}>
        <Text style={styles.text}>Service Information</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{...styles.btn, backgroundColor: '#FD7648'}}
        onPress={() => {
          getUserProfile();
          getServiceData((user_service) => {
            if (user_service) {
              if (
                isApplicable({
                  photo: profile.photo,
                  signature: profile.signature,
                  nid_front: profile.nid_front,
                  nid_back: profile.nid_back,
                  nid: profile.nid,
                  tin: profile.tin,
                  salary: user_service.salary,
                  company: user_service.company,
                  address: user_service.address,
                  designation: user_service.designation,
                  joining_date: user_service.joining_date,
                  experience: user_service.experience,
                })
              ) {
                navigation.navigate('SalaryApplication');
              }
            } else {
              showMessage({
                message: 'Please update your information',
                type: 'danger',
                position: 'center',
              });
            }
          });
        }}>
        <Text style={styles.text}>Apply</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
  },
  btn: {
    flex: 1,
    padding: 20,
    margin: 10,
    alignItems: 'center',
    borderRadius: 50,
  },
  text: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 20,
  },
  bgApply: {},
});
export default ProfileMenu;
