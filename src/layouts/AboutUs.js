import React, {useCallback, useRef, useState} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  CHAIRMAN_IMG,
  DIRECTOR_IMG,
  E_DIRECTOR_IMG,
  DISPLAY_WIDTH,
  Agro,
  LEATHER,
  POLYMER,
  STEEL,
  TECH,
  HAND_TREE,
} from '../lib/const';

const team = [
  {
    name: 'MD Monzur Alam Sikder',
    designation: 'Managing Director',
    image: CHAIRMAN_IMG,
    words:
      'Being a Founder and sustainable business visionary with 25 years in business,governance, over the years, MD. Monzur Alam Sikder has accumulated experience in substances of business expansion, operations, investment and strategic management and many more. Having boundless skills in communication enlargement and maintaining corporate relationships with various classified successful industrialist/ market leaders, he gained immense expertise. Demonstrated with unparalleled devotion in enlargement of corporation through idealistic product development, ethical management practice and excellent leadership skills in motivating talented employees, he founded Alesha Holdings Limited- a new venture with people oriented vision.',
  },
  {
    name: 'Sadia Chowdhury',
    designation: 'Director',
    image: DIRECTOR_IMG,
    words:
      'Surprising control over business and administration standard as they pertain in advancement, resource allocation, quality control of product and leading others. Having excellent knowledge in information management with experience using project management. Strong Communicator and visionary thinking with experience in both public speaking and interpersonal affiliation. Capable of genuinely connect, understand and empathize with patron to assist them in overcoming their challenges.',
  },
  {
    name: 'MD Ashraful Hoque',
    designation: 'Executive Director',
    image: E_DIRECTOR_IMG,
    words:
      'MD Ashraful Hoque started his corporate journey in the early stage of his educational life in 2012 and earned an experience of more than 8years in various posts. Functioning as the Executive Director of the company with a vast experience in Agro technology assuring finest sources from Australia, Africa, Europe and Asia. Introducing the state of art technology for modern farming, he is deliberately reaching the mission and vision of the company under direct instructions from the Managing Director. With an experience of administrating over 400 people under this organization, he is also efficient with asset management, legal affairs, compliance, banking and market analysis, policy making, utilizing.',
  },
];
const AboutUs = () => {
  return (
    <View>
      <Image style={styles.cplogo} source={Agro} />
    </View>
  );
};
export default AboutUs;
const styles = StyleSheet.create({
  header: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 35,
    borderBottomWidth: 4,
    borderBottomColor: 'white',
    marginBottom: 10,
  },
  bgimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  bgyellow: {
    backgroundColor: '#f47f20cb',
    padding: 15,
    paddingTop: 10,
    paddingBottom: 15,
  },
  content: {
    backgroundColor: 'white',
    marginTop: -50,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
  },
  maincontent: {
    padding: 15,
  },
  sectionhead: {
    marginTop: 15,
    marginBottom: 5,
    fontWeight: 'bold',
    fontSize: 17,
    alignSelf: 'center',
  },
  sectiontext: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
  },
  sectionlogo: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 25,
    marginRight: 25,
  },
  cplogo: {
    flex: 1,
    resizeMode: 'contain',
    margin: 3,
    height: 70,
  },
  team: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: 'white',
    padding: 10,
    marginTop: 10,
    marginBottom: 15,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 10,
  },
  teamprofile: {
    flex: 1,
    marginTop: 20,
  },
  teamdetails: {
    fontWeight: 'bold',
    padding: 1,
    fontSize: 14,
    alignSelf: 'center',
  },
  teamimage: {
    resizeMode: 'cover',
    margin: 3,
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: 'center',
  },
});
