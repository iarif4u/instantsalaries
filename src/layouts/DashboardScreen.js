import * as React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import {useContext} from 'react';
import {AppContext} from '../context/AppContext';
import HomeScreen from './screens/HomeScreen';
import PrivacyPolicy from './PrivacyPolicy';
import ContactUs from './ContactUs';
import Faq from './Faq';
import TermsAndConditions from './TermsAndConditions';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {Linking, Text, View} from 'react-native';
import {
  APPS_COLOR,
  SUPPORT_NUMBER,
  DRAWER_INACTIVE_COLOR,
  AVATAR,
} from '../lib/const';
import {Avatar, Title} from 'react-native-paper';
import DrawerHeader from './components/DrawerHeader';

const Drawer = createDrawerNavigator();

const CustomDrawerContent = (props) => {
  const {resetUserData, profile, user} = useContext(AppContext);
  return (
    <DrawerContentScrollView {...props}>
      <View
        style={{
          alignItems: 'center',
          padding: 20,
          flexDirection: 'row',
          backgroundColor: APPS_COLOR,
          marginTop: -10,
          opacity: 0.9,
        }}>
        <>
          {profile.photo ? (
            <Avatar.Image size={70} source={{uri: profile.photo}} />
          ) : (
            <Avatar.Image size={80} source={AVATAR} />
          )}
        </>
        <View
          style={{
            margin: 10,
            paddingLeft: 10,
          }}>
          <Title style={{color: '#ffffff'}}>
            {profile.full_name
              ? profile.full_name
              : user.name
              ? user?.name
              : profile.contact_email}
          </Title>
          <Text style={{color: '#ffffff'}}>{profile.contact_no}</Text>
        </View>
      </View>
      <DrawerItemList {...props} />
      <DrawerItem
        activeTintColor={APPS_COLOR}
        icon={({focused, size}) => (
          <Ionicons name="call-sharp" size={size} color={APPS_COLOR} />
        )}
        label="Call"
        onPress={() => Linking.openURL(`tel:${SUPPORT_NUMBER}`)}
      />
      <DrawerItem
        activeTintColor={APPS_COLOR}
        icon={({focused, size}) => (
          <Ionicons name="log-out-outline" size={size} color={APPS_COLOR} />
        )}
        label="Logout"
        onPress={() => resetUserData()}
      />
    </DrawerContentScrollView>
  );
};

const DashboardScreen = () => {
  return (
    <Drawer.Navigator
      itemStyle={{
        backgroundColor: APPS_COLOR,
        width: 240,
      }}
      labelStyle={{
        backgroundColor: APPS_COLOR,
        width: 240,
      }}
      drawerPosition="right"
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      initialRouteName="HomeScreen">
      <Drawer.Screen
        options={({navigation}) => ({
          drawerLabel: ({focused, color}) => {
            return (
              <Text style={{color: DRAWER_INACTIVE_COLOR}}>Dashboard</Text>
            );
          },
          drawerIcon: ({focused, size}) => (
            <AntDesign name="appstore-o" size={size} color={APPS_COLOR} />
          ),
          headerShown: false,
        })}
        name="Dashboard"
        component={HomeScreen}
      />
      <Drawer.Screen
        options={({navigation}) => ({
          headerShown: true,
          header: () => <DrawerHeader navigation={navigation} />,
          drawerLabel: ({focused, color}) => {
            return (
              <Text style={{color: DRAWER_INACTIVE_COLOR}}>Contact Us</Text>
            );
          },
          drawerIcon: ({focused, size}) => (
            <MaterialIcons
              name="support-agent"
              size={size}
              color={APPS_COLOR}
            />
          ),
        })}
        name="ContactUs"
        component={ContactUs}
      />
      <Drawer.Screen
        options={({navigation}) => ({
          headerShown: true,
          header: () => <DrawerHeader navigation={navigation} />,
          drawerLabel: ({focused, color}) => {
            return <Text style={{color: DRAWER_INACTIVE_COLOR}}>FAQ</Text>;
          },
          drawerIcon: ({focused, size}) => (
            <AntDesign name="questioncircle" size={size} color={APPS_COLOR} />
          ),
        })}
        name="FAQ"
        component={Faq}
      />
      <Drawer.Screen
        options={({navigation}) => ({
          headerShown: true,
          header: () => <DrawerHeader navigation={navigation} />,
          drawerLabel: ({focused, color}) => {
            return (
              <Text style={{color: DRAWER_INACTIVE_COLOR}}>
                Terms & Conditions
              </Text>
            );
          },
          drawerIcon: ({focused, size}) => (
            <Entypo name="newsletter" size={size} color={APPS_COLOR} />
          ),
        })}
        name="Terms & Conditions"
        component={TermsAndConditions}
      />
      <Drawer.Screen
        options={({navigation}) => ({
          headerShown: true,
          header: () => <DrawerHeader navigation={navigation} />,
          drawerLabel: ({focused, color}) => {
            return (
              <Text style={{color: DRAWER_INACTIVE_COLOR}}>Privacy Policy</Text>
            );
          },
          drawerIcon: ({focused, size}) => (
            <MaterialIcons name="privacy-tip" size={size} color={APPS_COLOR} />
          ),
        })}
        name="Privacy Policy"
        component={PrivacyPolicy}
      />
    </Drawer.Navigator>
  );
};

export default DashboardScreen;
