import React from 'react';
import {Image, StyleSheet, Text, View, ScrollView} from 'react-native';
import {TERMS_CONDITION_IMG, DISPLAY_WIDTH} from '../lib/const';
const TermsAndConditions = () => {
  return (
    <ScrollView>
      <Image
        style={{width: DISPLAY_WIDTH, height: 210, top: 0, marginTop: 0}}
        resizeMode="cover"
        source={TERMS_CONDITION_IMG}
      />
      <View style={styles.content}>
        <Text style={[styles.maincontent, styles.maincontentextra]}>
          Effective Date: 26 March 2021{' '}
        </Text>
        <Text style={styles.maincontent}>
          Please read the Terms & Conditions carefully. If you do not accept
          this Policy, please do not register as a Instantsalaries member or
          submit any personal information to us.
        </Text>
        <View style={styles.hr} />
        <Text style={styles.sectionhead}>Instant Salary</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. You need to be employed at any organization of Bangladesh.
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Submit proper and authentic documentation.
        </Text>
        <Text style={styles.sectionpointtext}>
          4. Fulfill the applications completely.
        </Text>
        <Text style={styles.sectionpointtext}>
          5. HR approval (Regarding salary and credit limit).
        </Text>
        <Text style={styles.sectionpointtext}>
          6. Reference code (investor code for higher credit limit).
        </Text>
        <Text style={styles.sectionpointtext}>
          7. References (5 references, if none of above is available, Maximum
          Limit 10,000).
        </Text>
        <Text style={styles.sectionpointtext}>
          8. Read and accept the terms & conditions attached.
        </Text>
        <Text style={styles.sectionhead}>Stakeholder</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Citizen or permission of residence in Bangladesh.
        </Text>
        <Text style={styles.sectionpointtext}>3. Legal Sources of Fund.</Text>
        <Text style={styles.sectionpointtext}>
          4. Accepted terms and conditions with total understandings.
        </Text>
        <Text style={styles.sectionhead}>Investor</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Citizen or permission of residence in Bangladesh.
        </Text>
        <Text style={styles.sectionpointtext}>3. Legal Sources of Fund.</Text>
        <Text style={styles.sectionpointtext}>
          4. Accepted terms and conditions with total understandings.
        </Text>
        <Text style={styles.sectionhead}>Medical Emergency</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Patient or direct family member of the patient.
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Prescription from any licensed doctor of Bangladesh.
        </Text>
        <Text style={styles.sectionpointtext}>
          4. Medicine collection from selected medicine shops (popular as
          pharmacies).
        </Text>
        <Text style={styles.sectionpointtext}>
          5. Repayment commitment declaration.
        </Text>
        <Text style={styles.sectionpointtext}>
          6. Proper application and reference.
        </Text>
        <Text style={styles.sectionpointtext}>
          7. Accepted terms and conditions.
        </Text>
        <Text style={styles.sectionhead}>Medical Loan</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Patient or direct family member of the patient.
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Prescription from any licensed doctor of Bangladesh.
        </Text>
        <Text style={styles.sectionpointtext}>
          4. Medicine collection from selected medicine shops (popular as
          pharmacies).
        </Text>
        <Text style={styles.sectionpointtext}>
          5. Repayment commitment declaration.
        </Text>
        <Text style={styles.sectionpointtext}>
          6. Proper application and reference.
        </Text>
        <Text style={styles.sectionpointtext}>
          7. Accepted terms and conditions.
        </Text>
        <Text style={styles.sectionhead}>Grocery Loan</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Can be purchased only from aleshamart.com
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Repayment commitment declaration.
        </Text>
        <Text style={styles.sectionpointtext}>
          4. Proper application and reference.
        </Text>
        <Text style={styles.sectionpointtext}>
          5. Accepted terms and conditions.
        </Text>
        <Text style={styles.sectionhead}>Study Loan</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Proper documentation of requirement (admission form, registration
          form).
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Referred by the head of the institute or registrar office.
        </Text>
        <Text style={styles.sectionpointtext}>
          4. Repayment commitment declaration.
        </Text>
        <Text style={styles.sectionpointtext}>
          5. Proper application and reference.
        </Text>
        <Text style={styles.sectionpointtext}>
          6. Accepted terms and conditions.
        </Text>
        <Text style={styles.sectionhead}>Instant Loan</Text>
        <Text style={styles.sectionpointtext}>
          1. A registered member of the platform.
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Proper Application with documents.
        </Text>
        <Text style={styles.sectionpointtext}>
          3. You need to be either employed or holding a business.
        </Text>
        <Text style={styles.sectionpointtext}>
          4. In case of employees, you need to submit approvals of your Human
          Resource Department.
        </Text>
        <Text style={styles.sectionpointtext}>
          5. Repayment commitment declaration.
        </Text>
        <Text style={styles.sectionpointtext}>
          6. Proper application and reference.
        </Text>
        <Text style={styles.sectionpointtext}>
          7. Accepted terms and conditions.
        </Text>
        <Text style={styles.sectionhead}>
          CUSTOMER SERVICE AGREEMENT (Loan)
        </Text>
        <Text style={styles.sectiontext}>
          This Customer Service Agreement (Hereinafter referred to as the
          “Agreement”) has been made, prepared and executed on this day (the
          “Date of Registration”) through Instanatsalaries.com web/mobile
          application in Bangladesh.
        </Text>
        <Text style={styles.sectionhead}>BY AND BETWEEN</Text>
        <Text style={styles.sectiontext}>
          The service provider, (Hereinafter referred to as
          “Instanatsalaries.com/Alesha Solutions Limited/Company/FIRST PARTY”)
          and Name……………., address: ……………………….. (Hereinafter referred to as the
          “Customer/Borrower/SECOND PARTY”), who applied for the registration by
          his name (the “Application of Registration”) and later will apply to
          the CO-OPERATIVES/MFI (the “Microfinance Institution”) for the loan.
        </Text>
        <Text style={styles.sectionhead}>NARRATION</Text>
        <Text style={styles.sectiontext}>
          1.‘Instanatsalaries.com’ is a digital platform that works for the
          CO-OPERATIVES/MFI to find Customers and provides services (as
          specified in this agreement) to the CO-OPERATIVES/MFI and their
          Customers through ‘Instanatsalaries.com’ web/mobile application.
        </Text>
        <Text style={styles.sectiontext}>
          2.A registered Customer who creates an account at
          ‘Instanatsalaries.com’ web/mobile application can find a suitable
          CO-OPERATIVES/MFI, through the services of ‘Instanatsalaries.com’ on
          such terms and conditions as may be provided in this Agreement.
        </Text>
        <Text style={styles.sectiontext}>
          3.That the Customer is any person who has an intention to borrow and
          is registered on the ‘Instanatsalaries.com’ as a Borrower, of the
          CO-OPERATIVES/MFI that are duly registered with the
          ‘Instanatsalaries.com’.
        </Text>
        <Text style={styles.sectiontext}>
          4.For the purpose of Borrowing, the Customer understands and accepts
          that he/she shall register him/her in order to facilitate borrowing
          from the CO-OPERATIVES/MFI through ‘Instanatsalaries.com’.
        </Text>
        <Text style={styles.sectiontext}>
          5.Customer who intends to open an account at ‘Instanatsalaries.com’
          platform:
        </Text>
        <Text style={styles.sectionpointtext}>
          5.1. Have created an account with ‘Instanatsalaries.com’ by giving
          basic information including name, contact numbers, emergency contact
          numbers, date of birth, email, place of residence, chosen password,
          gender, NID, Religion, employment and such other information sought
          for.
        </Text>
        <Text style={styles.sectionpointtext}>
          5.2. Have registered online by providing personal details as mentioned
          above in Clause 5.1 of the recitals and that the Borrower shall pay
          subscription fee and after service additional fees (if necessary) to
          the ‘Instanatsalaries.com’, exclusive of Services Tax and VAT.
        </Text>
        <Text style={styles.sectionhead}>
          NOW THEREFORE, IN CONSIDERATION OF THE MUTUAL PROMISES, COVENANTS AND
          CONDITIONS HEREINAFTER SET FORTH, THE RECEIPT AND SUFFICIENCY OF WHICH
          IS HEREBY ACKNOWLEDGED, THE PARTIES HERETO AGREE AS FOLLOWS:
        </Text>
        <Text style={styles.sectionhead}>
          1. DEFINITIONS AND INTERPRETATION
        </Text>
        <Text style={styles.sectiontext}>
          In this agreement (including any recitals, annexure, schedules or
          exhibits attached thereto) except where the context otherwise
          requires, the following words and expressions shall have the following
          meaning: -
        </Text>
        <Text style={styles.sectiontext}>
          1.1. “Borrower” means a Customer whose registration, data analysis,
          loan application, verification is completed through
          ‘Instanatsalaries.com’ and is a registered Borrower.
        </Text>
        <Text style={styles.sectiontext}>
          1.2. “CO-OPERATIVES/MFI” means the Microfinance Institution with whom
          'Alesha Solutions Limited’ do agreement as a service provider.
        </Text>
        <Text style={styles.sectiontext}>
          1.3. “Customer” is an individual who satisfies the following criteria:
        </Text>
        <Text style={styles.sectionpointtext}>1.Bangladeshi resident.</Text>
        <Text style={styles.sectionpointtext}>
          2.Above the age of 18 years.
        </Text>
        <Text style={styles.sectionpointtext}>
          3.Should be an Earning/eligible Member.
        </Text>
        <Text style={styles.sectionpointtext}>
          4.Have a NID/birth certificate issued by Bangladesh government
          authority.
        </Text>
        <Text style={styles.sectionpointtext}>
          5.Have income/reference proof.
        </Text>
        <Text style={styles.sectionpointtext}>
          6.Have residence proof (Utility bill).
        </Text>
        <Text style={styles.sectionpointtext}>
          7.Have a valid personal bKash/Nagad/MFS/bank account/valid medium.
        </Text>
        <Text style={styles.sectionpointtext}>8.Photograph</Text>
        <Text style={styles.sectiontext}>
          1.4. “Scanned Document” means, ``Customers shall scan and store
          documents in a digital format in the form of electronic documents from
          the best possible device as permitted by law; the scanned documents
          must remain accessible and legible.
        </Text>
        <Text style={styles.sectiontext}>
          1.5. “Photographs” mean Digital photographs which are stored inside
          the web/mobile application.
        </Text>
        <Text style={styles.sectiontext}>
          1.6. “SMS” means one unit of the Short Message Service defined.
        </Text>
        <Text style={styles.sectiontext}>
          1.7. “Transactional alert/message” means any alert/message, which is
          not promotional.
        </Text>
        <Text style={styles.sectiontext}>
          1.8. “Rating” means rating given by ‘Instanatsalaries.com’ based on
          its proprietary algorithm which is calculated on the basis of Credit
          rating of an individual based on the computed credit score.
        </Text>
        <Text style={styles.sectiontext}>
          1.9. “Person” means an individual or a company/firm or financial
          institution registered as per rule.
        </Text>
        <Text style={styles.sectiontext}>
          1.10. “Bank” means the bank account details provided by Customer in
          this agreement with details of bank branch; full address and account
          number, where Customer maintains its account, the said bank account
          details provided by Customer in the Instanatsalaries.com under this
          agreement.
        </Text>
        <Text style={styles.sectiontext}>
          1.11. “MFS” means the Mobile Financial Service including
          bKash/Nagad/Rocket and any other mobile banking organization etc.
        </Text>
        <Text style={styles.sectiontext}>
          1.12. “NID” means a 13-digit or 10-digit (for smart card) unique
          identification number issued by the government of Bangladesh to every
          individual resident of Bangladesh based on their biometric and
          demographic data.
        </Text>
        <Text style={styles.sectiontext}>
          1.13. “TIN” means a unique, 12-character numeric identifier, issued to
          all individuals and legal entities identifiable under the Income Tax
          Ordinance 1984. It is issued by the National Board of Revenue
          Bangladesh under the supervision of the Government of Bangladesh.
        </Text>
        <Text style={styles.sectiontext}>
          1.14. “Subscription fee” means the rate at which the CO-OPERATIVES/MFI
          shall compute and apply fee on the membership, as per the terms and
          conditions of the Loan Agreement.
        </Text>
        <Text style={styles.sectiontext}>
          1.15. “Month” means a month of a year as per the calendar year and
          each month shall begin on the first day of such month.
        </Text>
        <Text style={styles.sectiontext}>
          1.16. “Law” includes any constitution, statute, law, rule, regulation,
          ordinance, judgement, order, decree, authorization, or any published
          directive, guideline, requirement or governmental restriction having
          the force of law, or any determination by, or interpretation of any of
          the foregoing by, any judicial authority, whether in effect as of the
          date of the Application Form(s) or thereafter and each as amended from
          time to time;
        </Text>
        <Text style={styles.sectiontext}>
          1.17. “Loan Amount” means the amount of the financial assistance
          provided under the Loan Agreement to be executed between Customer and
          CO-OPERATIVES/MFI.
        </Text>
        <Text style={styles.sectiontext}>
          1.18. “Disbursement” means any payment of the Loan Amount, or any part
          thereof, made by CO-OPERATIVES/MFI in terms of Loan Agreement. All
          disbursements to be made by CO-OPERATIVES/MFI under or in terms of
          this Agreement shall be in the name of the Customer or Borrower, and
          shall be made by way of mobile banking transfer or any other mode i.e.
          RTGS, BEFTN, direct transfer, etc., at the sole discretion of
          CO-OPERATIVES/MFI;
        </Text>
        <Text style={styles.sectiontext}>
          1.19. “Disbursement of Funded amount” means the amount which
          ‘Instanatsalaries.com’ has approved after scrutinizing the credit
          score/worthiness of the Registered Customer. The same amount disbursed
          after receiving or deducting the service fees and any other charges
          towards such disbursement of approved amount.
        </Text>
        <Text style={styles.sectiontext}>
          1.20. “RTGS” means (Real Time Gross Settlement) an electronic form of
          amount/funds transfer where the transmission takes place on a real
          time basis. The beneficiary account receives the funds/amount transfer
          on Real time basis.
        </Text>
        <Text style={styles.sectiontext}>
          1.21. “BEFTN” means (Bangladesh Electronic Funds Transfer Network) is
          a Bangladesh system of Electronic Transfer of money from one bank or
          bank branch to another. Customers with Internet banking accounts can
          use the BEFTN facility to transfer funds nationwide on their own.
        </Text>
        <Text style={styles.sectiontext}>
          1.22. “Customer Dashboard” means the section of the web/mobile
          application which can be accessed by the borrower using only his/her
          own username and password through which they can update their details,
          request loan parts, and view details of their Portfolio.
        </Text>
        <Text style={styles.sectiontext}>
          1.23. “Service fee” means charges/fees based membership program
          through ‘Instanatsalaries.com’. All fees and charges mentioned in the
          Clause- 26. FEES & CHARGES will add Services Tax and Vat (if any) and
          the said fees & charges are subject to change from time to time.
          ’Instanatsalaries.com’ reserves the right to make changes in the fees
          & services.
        </Text>
        <Text style={styles.sectiontext}>
          1.24. A statutory provision shall include such provision as from time
          to time modified or re-enacted or consolidated so far as such
          modification or re-enactment or consolidation applies or is capable of
          applying to any transactions entered into hereunder.
        </Text>
        <Text style={styles.sectiontext}>
          1.25. Headings in this Agreement are for convenience of reference only
          and shall not be used to construe or interpret this agreement.
        </Text>
        <Text style={styles.sectiontext}>
          1.26. Words denoting the singular shall include the plural and vice
          versa and the word representing any gender shall be deemed to include
          all other genders.
        </Text>
        <Text style={styles.sectiontext}>
          1.27. The word 'includes' or 'including' shall be construed without
          limitation.
        </Text>
        <Text style={styles.sectiontext}>
          1.28. An 'amendment' includes a supplement, modification, notation,
          replacement or re-enactment and 'amended' is to be construed
          accordingly.
        </Text>
        <Text style={styles.sectionhead}>2. COMMENCEMENT</Text>
        <Text style={styles.sectiontext}>
          This agreement shall come into effect from the date of execution (the
          “Date of Registration”) of this agreement by acceptance of the
          Customer (the person)
        </Text>
        <Text style={styles.sectionhead}>
          3. PURPOSE/SCOPE OF THIS AGREEMENT
        </Text>
        <Text style={styles.sectiontext}>
          1.The Parties duly agree and understand that they have collaborated
          with each other for the purposes as expressed under Para(s) 1-5 to the
          Recitals.
        </Text>
        <Text style={styles.sectiontext}>
          2.The Customer duly agrees and understands that for the purposes of
          effectuating the objective as specified under RECITAL to this
          Agreement, the Customer shall register itself/himself/herself with the
          ‘Instanatsalaries.com’ in accordance with the guidelines and as duly
          specified within the provision of law;
        </Text>
        <Text style={styles.sectiontext}>
          3.The Customer is associated with the ‘Instanatsalaries.com’ in order
          to borrow money from the CO-OPERATIVES/MFI, through the
          ‘Instanatsalaries.com’.
        </Text>
        <Text style={styles.sectiontext}>
          4.The Customer duly agrees and understands that it shall be upon the
          sole discretion of the Company to register a prospective Customer as
          an actual Borrower of CO-OPERATIVES/MFI on the ‘Instanatsalaries.com’
          and the Customer shall cease and desist from raising any
          contention/dispute in respect thereof.
        </Text>
        <Text style={styles.sectionhead}>4. REGISTRATION</Text>
        <Text style={styles.sectiontext}>
          1.ICustomer agrees to register with ‘Instanatsalaries.com’ as
          Borrower. Verification process is mentioned in Clause 5 of this
          agreement. Those Customers who applied for the online registration,
          should submit the required documents for verification by
          ‘Instanatsalaries.com’ through the online process. This will be
          completed within the same working day, the date of Customer’s
          registration on the mobile application.
        </Text>
        <Text style={styles.sectiontext}>
          2.The Customer, to register itself/himself/herself with the
          ‘Instanatsalaries.com’ shall undergo the registration process of the
          ‘Instanatsalaries.com’.
        </Text>
        <Text style={styles.sectiontext}>
          3.‘Instanatsalaries.com’ Customer hereby agrees to receive SMS alert
          posted by ‘Instanatsalaries.com’ on any new registrations, loan
          closures, repayments, offers on loans, etc. registered members are
          availing these services on their own will and there will be no
          financial obligation in case of legal disputes. The registered members
          are obliged to notify ‘Instanatsalaries.com’, any change on his/her
          mobile phone number. The registered members are allowed to opt out
          from receiving SMS alerts by notifying ‘Instanatsalaries.com’ in
          writing through mobile application.
        </Text>
        <Text style={styles.sectionhead}>
          5. AUTHORIZATION FOR VERIFICATION
        </Text>
        <Text style={styles.sectiontext}>
          1.On successful completion of execution of this agreement, the
          Customers are subject to verification. Verification process includes
          online verification to check correctness of data provided for online
          registration by the Customer with the proofs. Customers should provide
          the list of documents mentioned above under clause 1.4 at the time of
          registration at ‘Instanatsalaries.com’.
        </Text>
        <Text style={styles.sectiontext}>
          2.The Customer shall submit the above mentioned documents in
          photo/scanned format in the Personal profile in
          ‘Instanatsalaries.com’, which shall be reached only after successfully
          registration completes and documents relating to work experience,
          Employment Category, Gross Income, Educational Level, Residence type,
          Residence Ownership Type, Permanent Address, and any other document
          that ‘Instanatsalaries.com’ deems fit to check;
        </Text>
        <Text style={styles.sectiontext}>
          3. The Customer must provide 02 (Two) emergency contacts or investor
          reference or HR referral.
        </Text>
        <Text style={styles.sectiontext}>
          4. Details of the place of employment should be declared by the
          Customer according to job category if any and in case of Business, the
          Customer shall provide documents related to the business (e.g. Trade
          License) of the Customer.
        </Text>
        <Text style={styles.sectiontext}>
          5.The Customer shall provide all the details pertaining to the
          Bank/MFS (e.g. bkash, Nagad, Rocket) account through which the
          Customer requests the loan amount to be transferred as Borrower.
        </Text>
        <Text style={styles.sectiontext}>
          6. Additional documents that the Customer needs to provide shall be:
          NID Card, TIN, Passport, Photograph of the Customer as per clause 1.3,
          1.4.
        </Text>
        <Text style={styles.sectiontext}>
          7. Customer or Borrower hereby authorizes the ‘Instanatsalaries.com’
          to obtain a credit report (if necessary) from any third party or such
          other organization which provides such facilities not only for the
          first time on registration but at any time during his association with
          ‘Instanatsalaries.com’. The credit report shall be used for any
          purpose that is authorized by applicable law.
        </Text>
        <Text style={styles.sectiontext}>
          8. ‘Instanatsalaries.com’ has the right to reject any Customer’s
          application, if any information provided by Customer is inaccurate or
          incorrect.
        </Text>
        <Text style={styles.sectiontext}>
          9. If any changes occur in the personal information provided after
          registration, Customer must promptly notify within 03 (three) days
          through message or email to ‘Instanatsalaries.com’ of the change. In
          the event of not notifying, ‘Instanatsalaries.com at its sole
          discretion may take necessary action including removing the
          registration.
        </Text>
        <Text style={styles.sectiontext}>
          10. Customer hereby agrees that documents sought by
          ‘Instanatsalaries.com’ at the time of registration or anytime
          thereafter become part of the official record of
          ‘Instanatsalaries.com’ and will not be returned. However, on closure
          of the user account ‘Instanatsalaries.com’ will ensure that the same
          is destroyed.
        </Text>
        <Text style={styles.sectionhead}>
          6. RE-PAYMENTS & AFTER SERVICE ADDITIONAL FEE
        </Text>
        <Text style={styles.sectiontext}>
          1. Customer agrees to pay the principal amount at the repayment date
          in the admitted manner to the CO-OPERATIVES/MFI. In the event of
          delayed payments, an after service additional fee is charged by the
          Instanatsalaries.com, charge based on the criteria as discussed
          further.
        </Text>
        <Text style={styles.sectiontext}>
          2. Customer will repay the loan to CO-OPERATIVES/MFI on the date as
          mentioned in the personal profile of Customer at
          ‘Instanatsalaries.com’. In case the repayment is not paid on the
          scheduled time, an after service additional fee will be charged for
          each day of delay beginning with the due date up to the date of actual
          repayment. So, ‘Instanatsalaries.com’ encourages all
          Customers/Borrowers to make timely repayments to avoid additional
          fees.
        </Text>
        <Text style={styles.sectiontext}>
          3. Please note that, if ‘Instanatsalaries.com’ thinks fit, at its sole
          discretion can increase/decrease this amount of after service
          additional fee;
        </Text>
        <Text style={styles.sectiontext}>
          4. If, ‘Instanatsalaries.com’ ties up with any bank in the near
          future, Customer/Borrower agrees to open a bank account with
          ‘Instanatsalaries.com’ designated bank through which the loan requests
          will be processed. In such case, Customer/Borrower agrees to make all
          loan commitments by maintaining an account for the said purpose. Till
          that period, Customer/Borrower agrees to repay the loan amount through
          its personal MFS (e.g. bKash, Nagad, Rocket) account.
        </Text>
        <Text style={styles.sectionhead}>
          7. INSTANATSALARIES.COM’ OBLIGATIONS
        </Text>
        <Text style={styles.sectiontext}>
          1. Instanatsalaries.com’ shall provide the above mentioned services at
          Clause 1.24 properly and also help the CO-OPERATIVES/MFI to enable
          swift and easy transfer of the loan disbursement amount in the account
          of the Customer/Borrower.
        </Text>
        <Text style={styles.sectiontext}>
          2. ‘Instanatsalaries.com’ agrees to engage the services of a
          collection on behalf of the CO-OPERATIVES/MFI in collecting timely
          payments from the Borrower with authorization/instructions from
          CO-OPERATIVES/MFI. ‘Instanatsalaries.com’ assumes no advisory or
          fiduciary responsibility concerning CO-OPERATIVES/MFI in connection
          with lending to Borrower, provided all the parties shall strictly
          abide by the procedure prescribed under the law.
        </Text>
        <Text style={styles.sectionhead}>8. NON-AGENT</Text>
        <Text style={styles.sectiontext}>
          1.‘Instanatsalaries.com’, the digital platform, shall in no measures
          be construed as an agent of either party (CO-OPERATIVES/MFI or
          Borrower). ‘Instanatsalaries.com’ is facilitating a digital platform
          only which shall not be held liable for any acts of either Party
          (CO-OPERATIVES/MFI or Borrower).
        </Text>
        <Text style={styles.sectiontext}>
          2. Any actions by either Party (CO-OPERATIVES/MFI and Borrower) shall
          not be interpreted as an act of ‘Instanatsalaries.com’ or ‘Alesha
          Solutions Limited’.
        </Text>
        <Text style={styles.sectionhead}>9. CUSTOMER/BORROWER OBLIGATIONS</Text>
        <Text style={styles.sectiontext}>
          1. To provide accurate and true information that shall be sought by
          the ‘Instanatsalaries.com’ for validating the information and
          documents.
        </Text>
        <Text style={styles.sectiontext}>
          2. To repay the loan to the CO-OPERATIVES/MFI without any failure
          within the scheduled loan period.
        </Text>
        <Text style={styles.sectiontext}>
          3. To pay the service fee (as invoiced at ‘Instanatsalaries.com’
          web/mobile application for Customer after his/her service finished) to
          the ‘Instanatsalaries.com’ or authorize the CO-OPERATIVES/MFI to
          deduct the service fee from the loan amount of the Borrower before
          disbursement, and which shall be authorized by the Borrower through
          his/her ‘Instanatsalaries.com’ mobile application.
        </Text>
        <Text style={styles.sectiontext}>
          4. To comply with all the terms of use for the website & mobile
          application, privacy policy and as well any other rules or policies
          set forth on Instanatsalaries.com’s website & mobile application, any
          of which may be amended from time to time by ‘Instanatsalaries.com’ at
          its sole discretion.
        </Text>
        <Text style={styles.sectiontext}>
          5. To co-operate with ‘Instanatsalaries.com’ for the
          information/documents sought from time to time and shall inform the
          ‘Instanatsalaries.com’ if there is any change in the address or change
          in the contact number.
        </Text>
        <Text style={styles.sectiontext}>
          6.Customer agrees that, in connection with any loan offers, loans or
          other transactions involving or potentially involving
          ‘Instanatsalaries.com’, not to
        </Text>
        <Text style={styles.sectionpointtext}>
          1.Make any false, misleading or deceptive statements or omissions of
          material fact.
        </Text>
        <Text style={styles.sectionpointtext}>
          2.Misrepresent his identity, or describe, present or portray himself
          as a person other than him.
        </Text>
        <Text style={styles.sectionpointtext}>
          3.Represent himself to any person as a director, officer or employee
          of ‘Instanatsalaries.com’ or Alesha Solutions Limited, unless being
          so.
        </Text>
        <Text style={styles.sectionpointtext}>
          4.Post anything abusive, harmful content or pictures; which is
          defamatory to ‘Instanatsalaries.com’ or Alesha Solutions Limited’.
        </Text>
        <Text style={styles.sectionhead}>
          10. REPRESENTATIONS AND WARRANTIES
        </Text>
        <Text style={styles.sectiontext}>
          Each party to the agreement makes the following representations and
          warranties with respect to itself, and confirms that they are true,
          correct and valid:
        </Text>
        <Text style={styles.sectiontext}>
          1.It has full power and authority to enter into, deliver and perform
          the terms and provisions of this agreement and, in particular, to
          exercise its rights, perform the obligations expressed to be assumed
          by and make the representations and warranties made by it hereunder.
        </Text>
        <Text style={styles.sectiontext}>
          2.Its obligations under this agreement are legal and valid obligations
          binding on it and enforceable against it in accordance with the terms
          hereof.
        </Text>
        <Text style={styles.sectiontext}>
          3.The parties to the agreement warrant and represent to have the legal
          competence and capacity to execute and perform this agreement.
        </Text>

        <Text style={styles.sectionhead}>11. NO GUARANTEE</Text>
        <Text style={styles.sectiontext}>
          Instanatsalaries.com’ does not guarantee:
        </Text>
        <Text style={styles.sectiontext}>
          1.That Customers loan request will be selected by an CO-OPERATIVES/MFI
          or;
        </Text>
        <Text style={styles.sectiontext}>
          2.That Customer will receive an CO-OPERATIVES/MFI’s acceptance for the
          loan request as a result of application, or;
        </Text>
        <Text style={styles.sectiontext}>
          3.That Customer will receive the loan at any rate of return, or any
          minimum amount of principal at all; The Parties duly agree and
          understand that the ‘Instanatsalaries.com’ makes no guarantee as to
          whether the Customer/Borrower shall receive a loan/credit upon his/her
          application.
        </Text>

        <Text style={styles.sectionhead}>12. RESTRICTIONS ON USE</Text>
        <Text style={styles.sectiontext}>
          Customer represents itself/himself/herself and no third party.
          Customer is not authorized or permitted to use ‘Instanatsalaries.com’
          to make requests for someone other than itself/himself/herself. All
          the transactions carried will be solely by Customer/Borrower from
          his/her personal bank/MFS (e.g. bKash, Nagad, Rocket) account.
        </Text>

        <Text style={styles.sectionhead}>13. TERMINATION</Text>
        <Text style={styles.sectiontext}>
          1.‘Instanatsalaries.com’ may at its sole discretion, with or without
          cause, terminate this agreement and registration with
          ‘Instanatsalaries.com’ at any time by giving a notice to the
          Customer/Borrower.
        </Text>
        <Text style={styles.sectiontext}>
          2. In case of non- compliance of Customer/Borrower’s obligations
          mentioned under Clause 09 above, involvements of Customer/Borrower in
          any prohibited activity or illegal purpose, Customer/Borrower’s
          failure to abide by the terms of this agreement or the
          ‘Instanatsalaries.com’ terms of service and any law of land. Upon such
          determination in connection with registration/loan offer,
          ‘Instanatsalaries.com’ may, at its sole discretion, immediately and
          without notice, take one or more of the following act:
        </Text>
        <Text style={styles.sectionpointtext}>
          1.Terminate or suspend Customer/Borrower’s application for loan
          through the ’Instanatsalaries.com’; or
        </Text>
        <Text style={styles.sectionpointtext}>
          2.Terminate this Agreement and Customer’s registration with
          ‘Instanatsalaries.
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Upon termination of this agreement and registration with
          ‘Instanatsalaries.com’, all the loans originated through
          ‘Instanatsalaries.com’ still hold good as the agreements are legal and
          signed by the Borrower & CO-OPERATIVES/MFI;
        </Text>
        <Text style={styles.sectiontext}>
          3. The Parties duly agree and understand that the
          ‘Instanatsalaries.com’ may at its sole discretion, with or without
          cause, terminate this Agreement and the Registration of the Customer
          with the Company at any time by giving a notice or notification at
          web/mobile application of such intent to the Customer;
        </Text>
        <Text style={styles.sectiontext}>
          4.The parties duly agree that the Company has sole authority to
          determine the non- compliance of the Customers obligations as
          expressed under Clause- 9 herein, involvement of the Customer in any
          prohibited activity or illegal purpose, Customer’s failure to abide by
          the terms of this Agreement or the Company’s terms of service and any
          law of land. Upon such determination, the Company may, at its sole
          discretion, immediately and without notice, cancel the provision of
          loan/credit facility and/or terminate the Customer’s right to apply or
          otherwise participate in the ‘Instanatsalaries.com’ and/or terminate
          this Agreement;
        </Text>
        <Text style={styles.sectiontext}>
          5.The Parties duly agree and understand that upon termination of this
          Agreement and registration with the Company, all loan/credit
          originated by CO-OPERATIVES/MFI through the ‘Instanatsalaries.com’
          still hold good as the Agreement executed herein is legal and signed
          by the Borrower and CO-OPERATIVES/MFI without any coercion and it
          shall be legally enforceable.
        </Text>
        <Text style={styles.sectiontext}>
          6. The Customer or Borrower duly agree that, after the termination of
          this agreement, the borrower shall be bound to pay the service
          fees/after service additional fee to the Instanatsalaries.com if
          required before the termination.
        </Text>

        <Text style={styles.sectionhead}>14. THIRD PARTY DISCLOSURES</Text>
        <Text style={styles.sectiontext}>
          The Customer hereby agrees and explicitly consents that
          ‘Instanatsalaries.com’ may disclose confidential information of the
          Customer to any third party who under the terms of this agreement have
          rights or any other Governmental Regulatory Body and law enforcing
          agency who needs to access such information in pursuance of any
          functions, duties and laws with regards to this Agreement.
        </Text>

        <Text style={styles.sectionhead}>
          15. INSTANATSALARIES.COM'S RIGHT TO MODIFY TERMS
        </Text>
        <Text style={styles.sectiontext}>
          1. ‘Instanatsalaries.com’ reserves the right to modify the clauses of
          the agreement or its terms of service from time to time without
          consent of Customer due to any of the following reasons:
        </Text>
        <Text style={styles.sectionpointtext}>
          1.To make these Customer Terms and Conditions clearer or easier to
          understand;
        </Text>
        <Text style={styles.sectionpointtext}>
          2. To make changes which benefit Customer or do not disadvantage
          Customer;
        </Text>
        <Text style={styles.sectionpointtext}>
          3. To make changes to the products or services we offer or provide to
          the Customer, to introduce new products or services or to withdraw
          products or services ‘Instanatsalaries.com’/Company no longer offers;
        </Text>
        <Text style={styles.sectionpointtext}>
          4. To change our existing services because of changes to technology,
          systems, processes or our business policies or procedures;
        </Text>
        <Text style={styles.sectionpointtext}>
          5. To reflect changes in law, regulation, industry guidance or a
          decision of a competent court or any regulation framed by the
          regulatory from time to time;
        </Text>
        <Text style={styles.sectiontext}>
          2. Instanatsalaries.com’ will always try to give at least one month's
          notice of modification/amendment to this Customer Agreement by
          notifying the Customer of the proposed change through the Website &
          web/mobile application or sending an email to Borrower’s registered
          email address (if available). However, this may not always be possible
          and ‘Instanatsalaries.com’ may make changes on shorter notice periods.
          Changes will not apply retrospectively. Changes to the terms of
          service will become effective no sooner than two days after they are
          modified. ‘Instanatsalaries.com’ will give Customer notice of material
          changes to this agreement, within 30 working days.
        </Text>

        <Text style={styles.sectionhead}>
          16. CEASING TO BE A CUSTOMER WITH ‘INSTANATSALARIES.COM’ AND
          TERMINATION
        </Text>
        <Text style={styles.sectiontext}>
          1. In case, a Customer no longer wishes to be a ‘Instanatsalaries.com’
          Customer then, provided that, the Customer has:
        </Text>
        <Text style={styles.sectionpointtext}>
          1.No Loan Agreements in force;
        </Text>
        <Text style={styles.sectionpointtext}>
          2.No Loan Agreements with CO-OPERATIVES/MFI in force;
        </Text>
        <Text style={styles.sectionpointtext}>
          3.No outstanding dues in his/ her account;
        </Text>
        <Text style={styles.sectionpointtext}>
          4.No dues regarding service and after service additional fees;
        </Text>
        <Text style={styles.sectiontext}>
          2.Customers can notify us at any time ‘Instanatsalaries.com’ will
          close your Customer Account.
        </Text>
        <Text style={styles.sectiontext}>
          3.If a Registered Customer wishes to close his/her Customer Account
          but the following circumstances apply:
        </Text>
        <Text style={styles.sectionpointtext}>
          1.Customer/Borrower is a party to a Loan Agreement which is in force
          or;
        </Text>
        <Text style={styles.sectionpointtext}>
          2.Customer/Borrower has active loans in his account; or
        </Text>
        <Text style={styles.sectionpointtext}>
          3.Customer/Borrower has an active loan application;
        </Text>
        <Text style={styles.sectiontext}>
          4. Customer/Borrower must either (depending on the circumstances below
          mentioned):
        </Text>
        <Text style={styles.sectionpointtext}>
          1. Repay all loan repayment and due (if any); or
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Cancel his/her application from the ‘Instanatsalaries.com’;
        </Text>
        <Text style={styles.sectiontext}>
          5. ‘Instanatsalaries.com’ may give Customer notice that
          ‘Instanatsalaries.com’ intend to close your Customer Account at any
          time if:
        </Text>
        <Text style={styles.sectionpointtext}>
          1. Customer breaches any term of this Customer Service Agreement,
          Terms and Conditions and the Privacy Policy;
        </Text>
        <Text style={styles.sectionpointtext}>
          2. Customer/Borrower breaches any term of a Loan Agreement and fail to
          remedy the breach within any reasonable period notified to Borrower;
        </Text>
        <Text style={styles.sectionpointtext}>
          3. Customer ceases to be an eligible Customer because Customer does
          not satisfy the criteria as per Terms and Conditions of this Customer
          Service Agreement.
        </Text>

        <Text style={styles.sectionhead}>17. INDEMNIFICATION</Text>
        <Text style={styles.sectiontext}>
          Customer agrees to indemnify and hold ‘Instanatsalaries.com’ harmless
          from and against any and all claims, action, liability, cost, loss,
          damage, endured by ‘Instanatsalaries.com’ by Customer's access in
          violation to the listed terms of service and also to the applicable
          laws, rules and regulations or agreements prevailing from time to
          time. Cost or expense also includes and is not limited to reminder
          fees, attorney fees, notary fees, appraisal fees, legal fees,
          transportation fees, investigation cost etc.
        </Text>

        <Text style={styles.sectionhead}>18. SEVERABILITY</Text>
        <Text style={styles.sectiontext}>
          If any provision of this agreement is found to be invalid or
          unenforceable, then the invalid or unenforceable provision will be
          deemed superseded by a valid enforceable provision that most closely
          matches the intent of the original provision and the remainder of the
          agreement shall continue in effect.
        </Text>

        <Text style={styles.sectionhead}>19. INTELLECTUAL PROPERTY RIGHTS</Text>
        <Text style={styles.sectiontext}>
          ‘Instanatsalaries.com’ and all related logos (collectively the
          "‘Instanatsalaries.com’ trademarks") are trademarks or service marks
          of ‘Instanatsalaries.com’. Nothing on this site should be construed as
          granting, by implication, estoppel, or otherwise, any license or right
          to use any of ‘Instanatsalaries.com’ trademarks displayed on this
          site, without our prior written permission in each instance. Customers
          should not use, copy, display, distribute, modify or reproduce any of
          the trademarks found on the site unless in accordance with written
          authorization by us. ‘Instanatsalaries.com’ prohibits use of any of
          Instanatsalaries.com’s trademarks as part of a link to or from any
          site unless establishment of such a link is approved in writing by
          ‘Instanatsalaries.com’ in advance.
        </Text>

        <Text style={styles.sectionhead}>20. NOTICES</Text>
        <Text style={styles.sectiontext}>
          All notices and other communications hereunder shall be given by
          notification at ‘Instanatsalaries.com’ application or email to
          Customer’s registered email address (if any) and wherever possible,
          posted on ‘Instanatsalaries.com’s website & mobile application.
          Customers may request ‘Instanatsalaries.com’ to send the
          correspondence through post, for which Customer must bear the
          expenditure. For effective communication, Customer agrees to notify
          any changes in the communication details i.e. mailing address, contact
          details, email-id by sending an email at care@instanatsalaries.com
        </Text>

        <Text style={styles.sectionhead}>21. ERRORS</Text>
        <Text style={styles.sectiontext}>
          Customer authorizes ‘Instanatsalaries.com’ to correct obvious clerical
          errors appearing in information he/she provides to
          ‘Instanatsalaries.com’, without notice to him/her; although,
          ‘Instanatsalaries.com’ expressly undertakes no obligation to identify
          or correct such errors.
        </Text>

        <Text style={styles.sectionhead}>
          22. GOVERNING LAW, DISPUTE RESOLUTION AND JURISDICTION
        </Text>
        <Text style={styles.sectiontext}>
          1.Any and all disputes or differences between Customer and
          ‘Instanatsalaries.com’ the Company, arising out of or in connection
          with this agreement or its performance shall, so far as it is
          possible, be settled by negotiations between the parties amicably
          through consultation.
        </Text>
        <Text style={styles.sectiontext}>
          2.Any dispute, which could not be settled by the parties through
          amicable settlement, shall be finally settled by the arbitration or
          court of law having jurisdiction to grant the same. The Jurisdiction
          shall be that of Dhaka, Bangladesh.
        </Text>
        <Text style={styles.sectiontext}>
          3.This agreement and the arrangements contemplated hereby shall in all
          respects be governed by and construed in accordance with the laws of
          Bangladesh without giving effect to the principles of conflict of laws
          there under.
        </Text>

        <Text style={styles.sectionhead}>23. FORCE MAJEURE</Text>
        <Text style={styles.sectiontext}>
          1.No Party shall be liable to the other if, and to the extent, that
          the performance or delay in performance of any of its obligations
          under this agreement is prevented, restricted, delayed or interfered
          with, due to circumstances beyond the reasonable control of such
          party, including but not limited to, Government legislations, fires,
          floods, explosions, epidemics, accidents, acts of God, wars, riots,
          strikes, lockouts, or other concerted acts of workmen, acts of
          Government and/or shortages of materials;
        </Text>
        <Text style={styles.sectiontext}>
          2.The party claiming an event of force majeure shall promptly notify
          the other parties in writing and provide full particulars of the cause
          or event and the date of first occurrence thereof, as soon as possible
          after the event and also keep the other parties informed of any
          further developments. The party so affected shall use its best efforts
          to remove the cause of non-performance, and the parties shall resume
          performance hereunder with the utmost dispatch when such cause is
          removed.
        </Text>

        <Text style={styles.sectionhead}>24. BINDING EFFECT</Text>
        <Text style={styles.sectiontext}>
          All warranties, undertakings and agreements given herein by the
          parties shall be binding upon the parties and upon its legal
          representatives. This agreement (together with any amendments or
          modifications thereof) supersedes all prior discussions and agreements
          (whether oral or written) with respect to the transaction or
          electronic, between Customer and ‘Instanatsalaries.com’ with respect
          to Customer involvement as a Customer on the ‘Instanatsalaries.com.
        </Text>

        <Text style={styles.sectionhead}>25. ENTIRE AGREEMENT</Text>
        <Text style={styles.sectiontext}>
          This agreement, along with the ‘Instanatsalaries.com’ terms and
          conditions, represents the entire agreement between Customer and
          ‘‘Instanatsalaries.com’ regarding the participation as a Customer on
          the ‘Instanatsalaries.com’, and supersedes all prior or
          contemporaneous communications, promises and proposals, whether oral
          or written.
        </Text>

        <Text style={styles.sectionhead}>26. FEES & CHARGES</Text>
        <Text style={styles.sectiontext}>
          Subscription fee only as per agreed.
        </Text>
        <Text style={styles.sectiontext}>
          This Customer Service Agreement is made and agreed upon by both
          parties in the form of an electronic Agreement and placed in the
          Instanatsalaries.com mobile application. This Agreement is valid and
          accepted by the Customer's confirmation and digital signature
          according to his/her understanding, own will in the
          Instanatsalaries.com mobile application. A version of this Agreement
          remains stored in the instantsalaries.com is considered the final
          proof of dispute settlement and both parties also agree that if any
          dispute arises between the parties, this electronic agreement will be
          printed and used as evidence to mitigate the dispute.
        </Text>
        <Text style={styles.sectiontext}>
          The Customer/Borrower for the related platform, click on the “I Agree”
          button, which means that the Customer has carefully read the entire
          terms and conditions of this Agreement and has no objections.
        </Text>
        <Text style={styles.sectiontext}>
          The Customer/Borrower has signified his/her consent to the terms and
          conditions of this Agreement by clicking in the “Agree” button.
        </Text>
      </View>
    </ScrollView>
  );
};

export default TermsAndConditions;

const styles = StyleSheet.create({
  header: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 50,
    marginBottom: 90,
    borderBottomWidth: 4,
    borderBottomColor: 'white',
  },
  bgimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  bgyellow: {
    backgroundColor: '#f47f20cb',
  },
  content: {
    backgroundColor: 'white',
    marginTop: -20,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
  },
  maincontent: {
    textAlign: 'justify',
    fontSize: 14,
  },
  maincontentextra: {
    fontSize: 13,
    alignSelf: 'flex-end',
    marginBottom: 10,
  },
  sectionhead: {
    marginTop: 15,
    marginBottom: 5,
    fontWeight: 'bold',
    fontSize: 17,
  },
  sectiontext: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
  },
  sectionpointtext: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
    paddingLeft: 10,
  },
  hr: {
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
    marginBottom: 0,
    marginTop: 20,
  },
});
