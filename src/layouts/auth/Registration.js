import React, {createRef, useContext, useEffect} from 'react';
import CheckBox from '@react-native-community/checkbox';

import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Keyboard,
  BackHandler,
} from 'react-native';
import {APPS_COLOR, ROW_WIDTH, TEXT_BLACK, TEXT_GRAY} from '../../lib/const';
import {AppContext} from '../../context/AppContext.js';
import {
  CHANGE_EMAIL,
  CHANGE_MOBILE,
  CHANGE_NAME,
  CHANGE_PASSWORD,
  CHANGE_TERMS,
  PASSWORD_CONFIRMATION,
} from '../../context/ReducerConst';
import Input from '../Form/Input';
import LogoBanner from '../LogoBanner';

const Registration = ({navigation}) => {
  const {
    user,
    changeUserData,
    submitRegistration,
    resetUserEmailNPass,
    resetUserData,
  } = useContext(AppContext);
  useEffect(() => {
    const backAction = () => {
      resetUserData();
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const nameInputRef = createRef(null);
  const emailInputRef = createRef(null);
  const mobileInputRef = createRef(null);
  const confirmationRef = createRef(null);
  const passwordInputRef = createRef(null);

  const changeName = (value) => {
    changeUserData(CHANGE_NAME, value);
  };
  const changeEmail = (value) => {
    changeUserData(CHANGE_EMAIL, value);
  };
  const changeContactNo = (value) => {
    changeUserData(CHANGE_MOBILE, value);
  };
  const changePassword = (value) => {
    changeUserData(CHANGE_PASSWORD, value);
  };
  const changeConfirmPassword = (value) => {
    changeUserData(PASSWORD_CONFIRMATION, value);
  };
  const setTermsCondition = (value) => {
    changeUserData(CHANGE_TERMS, value);
  };
  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <LogoBanner />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <Text
          style={{
            color: TEXT_BLACK,
            fontSize: 24,
            marginBottom: 4,
          }}>
          Sign up your account
        </Text>
        <View style={{flexDirection: 'row', marginBottom: 15}}>
          <Text
            style={{
              color: TEXT_GRAY,
              fontSize: 15,
              margin: 0,
              padding: 0,
              marginBottom: 15,
            }}>
            Already have an account ?
          </Text>
          <TouchableOpacity
            onPress={() => {
              resetUserEmailNPass();
              navigation.navigate('Login');
            }}>
            <Text
              style={{
                color: APPS_COLOR,
                marginLeft: 5,
              }}>
              Sign in here
            </Text>
          </TouchableOpacity>
        </View>
        <Input
          inputRef={nameInputRef}
          onSubmitEditing={emailInputRef}
          onChangeText={changeName}
          value={user.name}
          placeholder="Name"
        />
        <Input
          onChangeText={changeEmail}
          inputRef={emailInputRef}
          onSubmitEditing={mobileInputRef}
          value={user.email}
          keyboardType="email-address"
          placeholder="Email Address"
        />
        <Input
          onChangeText={changeContactNo}
          inputRef={mobileInputRef}
          onSubmitEditing={passwordInputRef}
          value={user.contact_no}
          keyboardType="phone-pad"
          placeholder="Mobile number"
        />
        <Input
          onChangeText={changePassword}
          value={user.password}
          inputRef={passwordInputRef}
          secureTextEntry={true}
          placeholder="Password"
        />
        <Input
          onChangeText={changeConfirmPassword}
          inputRef={confirmationRef}
          value={user.password_confirmation}
          secureTextEntry={true}
          onSubmitEditing={Keyboard.dismiss}
          placeholder="Confirm Password"
        />
        <View
          style={{
            marginBottom: 25,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <CheckBox
            value={user.terms_and_condition}
            onValueChange={setTermsCondition}
          />
          <Text
            style={{
              color: TEXT_GRAY,
              marginLeft: 10,
            }}>
            I agree to instant salaries Privacy Policy and T&C
          </Text>
        </View>
        <TouchableOpacity
          onPress={submitRegistration}
          style={{
            backgroundColor: '#2c398f',
            width: 200,
            alignSelf: 'center',
            paddingTop: 15,
            paddingBottom: 15,
            paddingLeft: 50,
            paddingRight: 50,
            borderRadius: 50,
          }}>
          <Text style={{color: '#ffffff', alignSelf: 'center'}}>
            Sign Up
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{height: 50}} />
    </ScrollView>
  );
};

export default Registration;
