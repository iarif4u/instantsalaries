import React, {useContext} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {APPS_COLOR, ROW_WIDTH, TEXT_BLACK, TEXT_GRAY} from '../../lib/const';
import {AppContext} from '../../context/AppContext.js';
import {CHANGE_EMAIL} from '../../context/ReducerConst';
import Input from '../Form/Input';
import LogoBanner from '../LogoBanner';

const ForgetPassword = ({navigation}) => {
  const {user, changeUserData, sendResetMail, resetUserEmailNPass} = useContext(
    AppContext,
  );
  const changeEmail = (value) => {
    changeUserData(CHANGE_EMAIL, value);
  };
  return (
    <ScrollView
      style={{backgroundColor: '#ffffff'}}
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <LogoBanner />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <Text
          style={{
            color: TEXT_BLACK,
            fontSize: 24,
            marginBottom: 4,
          }}>
          Enter your email address
        </Text>
        <View style={{ marginBottom: 15}}>
          <Text
            style={{
              color: TEXT_GRAY,
              fontSize: 15,
              margin: 0,
              padding: 0,
              marginBottom: 15,
            }}>
            We'll send you a verification code to reset your password.
          </Text>
        </View>
        <Input
          onChangeText={changeEmail}
          value={user.email}
          placeholder="Email"
        />
        <TouchableOpacity
          onPress={() =>
            sendResetMail((is_done) => {
              if (is_done) {
                navigation.navigate('ResetPassword');
              }
            })
          }
          style={{
            backgroundColor: '#2c398f',
            width: 200,
            alignSelf: 'center',
            paddingTop: 15,
            paddingBottom: 15,
            paddingLeft: 50,
            paddingRight: 50,
            borderRadius: 50,
          }}>
          <Text style={{color: '#ffffff', alignSelf: 'center'}}>Submit</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default ForgetPassword;
