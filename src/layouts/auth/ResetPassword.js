import React, {useContext} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {APPS_COLOR, ROW_WIDTH, TEXT_BLACK, TEXT_GRAY} from '../../lib/const';
import {AppContext} from '../../context/AppContext.js';
import {
  CHANGE_PASSWORD,
  PASSWORD_CONFIRMATION,
  SET_TOKEN,
} from '../../context/ReducerConst';
import Input from '../Form/Input';
import LogoBanner from '../LogoBanner';

const ResetPassword = ({navigation}) => {
  const {user, changeUserData, resetPassword, resetUserEmailNPass} = useContext(
    AppContext,
  );
  const changePassword = (value) => {
    changeUserData(CHANGE_PASSWORD, value);
  };
  const changeConfirmPassword = (value) => {
    changeUserData(PASSWORD_CONFIRMATION, value);
  };
  const changeToken = (value) => {
    changeUserData(SET_TOKEN, value);
  };
  return (
    <ScrollView
      style={{backgroundColor: '#ffffff'}}
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <LogoBanner />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <Text
          style={{
            color: TEXT_BLACK,
            fontSize: 24,
            marginBottom: 4,
          }}>
          Enter your email address
        </Text>
        <View style={{flexDirection: 'row', marginBottom: 15}}>
          <Text
            style={{
              color: TEXT_GRAY,
              fontSize: 15,
              margin: 0,
              padding: 0,
              marginBottom: 15,
            }}>
            Don't Forget Your Password ?
          </Text>
          <TouchableOpacity
            onPress={() => {
              resetUserEmailNPass();
              navigation.navigate('Login');
            }}>
            <Text
              style={{
                color: APPS_COLOR,
                marginLeft: 5,
              }}>
              Sign in here
            </Text>
          </TouchableOpacity>
        </View>
        <Input
          onChangeText={changeToken}
          keyboardType="numeric"
          value={user.token}
          placeholder="Verification Code"
        />
        <Input
          onChangeText={changePassword}
          value={user.password}
          secureTextEntry={true}
          placeholder="Password"
        />
        <Input
          onChangeText={changeConfirmPassword}
          value={user.password_confirmation}
          secureTextEntry={true}
          placeholder="Confirm Password"
        />
        <TouchableOpacity
          onPress={() => {
            resetPassword((is_done) => {
              if (is_done) {
                resetUserEmailNPass();
                changeToken('');
                navigation.navigate('Login');
              }
            });
          }}
          style={{
            backgroundColor: '#2c398f',
            width: 200,
            alignSelf: 'center',
            paddingTop: 15,
            paddingBottom: 15,
            paddingLeft: 50,
            paddingRight: 50,
            borderRadius: 50,
          }}>
          <Text style={{color: '#ffffff', alignSelf: 'center'}}>Submit</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};
export default ResetPassword;
