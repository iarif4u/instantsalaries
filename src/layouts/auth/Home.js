import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ToastAndroid,
  BackHandler,
} from 'react-native';
import {APPS_COLOR, ROW_WIDTH, HOME_IMAGE} from '../../lib/const';
import {AppContext} from '../../context/AppContext';
import moment from 'moment';

const Home = ({navigation}) => {
  const {resetUserData} = useContext(AppContext);
  const [exitCount, setExit] = useState(moment());

  useEffect(() => {
    const backAction = () => {
      if (!navigation.canGoBack()) {
        if (
          Math.round(moment.duration(moment().diff(exitCount)).asSeconds()) > 2
        ) {
          setExit(moment());
          ToastAndroid.show(
            'Tap back again to exit the App',
            ToastAndroid.SHORT,
          );
        } else {
          BackHandler.exitApp();
        }
        return true;
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [exitCount, navigation]);
  return (
    <ScrollView
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <Image
        style={{width: '100%', height: 300}}
        resizeMode={'center'}
        source={HOME_IMAGE}
      />

      <View style={{flex: 3, width: ROW_WIDTH, marginTop: 20}}>
        <Text style={{color: APPS_COLOR, fontSize: 27, textAlign: 'center'}}>
          Get Your Salary In Advance
        </Text>
        <Text
          style={{
            fontSize: 20,
            textAlign: 'center',
            marginTop: 10,
            padding: 10,
            color: '#aaaaaa',
          }}>
          Instanatsalaries is the only 0% interest platform in Bangladesh for
          solving financial crises in case of emergency.
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          resetUserData();
          navigation.navigate('Login');
        }}
        style={{
          backgroundColor: '#2c398f',
          paddingTop: 15,
          paddingBottom: 15,
          paddingLeft: 50,
          paddingRight: 50,
          borderRadius: 50,
        }}>
        <Text style={{color: '#ffffff', alignSelf: 'center', fontSize: 22}}>
          Sign in
        </Text>
      </TouchableOpacity>
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: '#aaaaaa',
            fontSize: 18,
          }}>
          Don't have an account?
        </Text>
        <TouchableOpacity
          onPress={() => {
            resetUserData();
            navigation.navigate('Registration');
          }}>
          <Text style={{color: APPS_COLOR, fontSize: 18}}> Sign up</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Home;
