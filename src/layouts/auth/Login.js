import React, {useContext, createRef} from 'react';
import {Text, View, TouchableOpacity, ScrollView, Keyboard} from 'react-native';
import {APPS_COLOR, ROW_WIDTH, TEXT_GRAY} from '../../lib/const';
import {AppContext} from '../../context/AppContext.js';
import {CHANGE_EMAIL, CHANGE_PASSWORD} from '../../context/ReducerConst';
import Input from '../Form/Input';
import LogoBanner from '../LogoBanner';

const LoginForm = ({navigation}) => {
  const {user, changeUserData, submitLogin, resetUserEmailNPass} = useContext(
    AppContext,
  );
  const passwordInputRef = createRef(null);
  const emailInputRef = createRef(null);
  const changeEmail = (value) => {
    changeUserData(CHANGE_EMAIL, value);
  };
  const changePassword = (value) => {
    changeUserData(CHANGE_PASSWORD, value);
  };

  return (
    <ScrollView
      style={{backgroundColor: '#ffffff'}}
      keyboardShouldPersistTaps={'handled'}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <LogoBanner />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <Text
          style={{
            color: APPS_COLOR,
            fontSize: 28,
            marginBottom: 4,
          }}>
          Sign in to your account
        </Text>
        <View style={{flexDirection: 'row', marginBottom: 15}}>
          <Text
            style={{
              color: TEXT_GRAY,
              fontSize: 15,
              margin: 0,
              padding: 0,
              marginBottom: 15,
            }}>
            Don't have an account ?
          </Text>
          <TouchableOpacity
            onPress={() => {
              resetUserEmailNPass();
              navigation.navigate('Registration');
            }}>
            <Text
              style={{
                color: APPS_COLOR,
                marginLeft: 5,
              }}>
              Sign up here
            </Text>
          </TouchableOpacity>
        </View>
        <Input
          label="Email Address"
          onChangeText={changeEmail}
          inputRef={emailInputRef}
          onSubmitEditing={passwordInputRef}
          value={user.email}
          style={{borderRadius: 50, overflow: 'hidden'}}
          keyboardType="email-address"
        />
        <Input
          onChangeText={changePassword}
          value={user.password}
          inputRef={passwordInputRef}
          secureTextEntry={true}
          placeholder="Password"
          onSubmitEditing={Keyboard.dismiss}
        />
        <View
          style={{
            marginBottom: 25,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <View style={{flex: 4}}>
            <TouchableOpacity
              onPress={() => {
                resetUserEmailNPass();
                navigation.navigate('ForgetPassword');
              }}>
              <Text
                style={{
                  color: APPS_COLOR,
                  textAlign: 'left',
                }}>
                Forgot Password
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => submitLogin()}
          style={{
            backgroundColor: '#2c398f',
            width: 200,
            alignSelf: 'center',
            paddingTop: 15,
            paddingBottom: 15,
            paddingLeft: 50,
            paddingRight: 50,
            borderRadius: 50,
          }}>
          <Text style={{color: '#ffffff', alignSelf: 'center'}}>Sign in</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default LoginForm;
