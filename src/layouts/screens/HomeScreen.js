import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  StatusBar,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Linking,
  Image,
  ImageBackground,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import moment from 'moment';
import {Avatar} from 'react-native-paper';
import {FloatingAction} from 'react-native-floating-action';

import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {APPS_COLOR, AVATAR, HOME_IMAGE, POCKET} from '../../lib/const';
import {AppContext} from '../../context/AppContext';
import {createStackNavigator} from '@react-navigation/stack';
import ProfileUpdate from '../Form/ProfileUpdate';
import ServiceUpdate from '../Form/ServiceUpdate';
import SalaryApplication from '../Form/SalaryApplication';
import Menu from '../components/Menu';
import Applications from './Applications';
import ApplicationFee from './ApplicationFee';
import CameraAndGallery from '../components/CameraAndGallery';
import TransactionMethod from './TransactionMethod';
import PayBack from './PayBack';
import Transactions from './Transactions';
import LeftMenu from '../header/LeftMenu';
import LinearGradient from 'react-native-linear-gradient';

const HomeNavigator = createStackNavigator();

const HomeDashboard = ({navigation}) => {
  const {getUserProfile, profile, TakeImage, user} = useContext(AppContext);
  const [exitCount, setExit] = useState(moment());
  const refRBSheet = useRef();
  useEffect(() => {
    getUserProfile();
  }, []);

  useEffect(() => {
    const backAction = () => {
      if (!navigation.canGoBack()) {
        if (
          Math.round(moment.duration(moment().diff(exitCount)).asSeconds()) > 2
        ) {
          setExit(moment());
          ToastAndroid.show(
            'Tap back again to exit the App',
            ToastAndroid.SHORT,
          );
        } else {
          BackHandler.exitApp();
        }
        return true;
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [exitCount, navigation]);
  return (
    <>
      <StatusBar backgroundColor={'#aa80f9'} barStyle="light-content" />
      <ScrollView contentContainerStyle={{backgroundColor: '#fff'}}>
        <LinearGradient colors={['#aa80f9', '#6165d7']}>
          <ImageBackground
            style={{width: '100%', height: 210}}
            resizeMode={'center'}
            source={HOME_IMAGE}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
              }}>
              <Image
                resizeMode="center"
                style={{height: 50, width: 50}}
                source={POCKET}
              />
              <LeftMenu navigation={navigation} color={'#ffffff'} />
            </View>
          </ImageBackground>
        </LinearGradient>
        <View
          style={{position: 'absolute', marginTop: 140, alignSelf: 'center'}}>
          <TouchableOpacity onPress={() => refRBSheet.current.open()}>
            {profile.photo ? (
              <Avatar.Image size={120} source={{uri: profile.photo}} />
            ) : (
              <Avatar.Image size={120} source={AVATAR} />
            )}
            <Icon
              name="camera"
              color="gray"
              size={24}
              style={{
                position: 'absolute',
                bottom: 0,
                alignSelf: 'center',
                right: 0,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 25,
              marginTop: 60,
              color: APPS_COLOR,
              alignSelf: 'center',
            }}>
            {profile.full_name
              ? profile.full_name
              : user.name
              ? user?.name
              : profile.contact_email}
          </Text>
          {profile.contact_no != null && (
            <Text style={{fontSize: 20, color: 'gray', alignSelf: 'center'}}>
              Mobile: {profile.contact_no}
            </Text>
          )}
        </View>
        <Menu navigation={navigation} />
      </ScrollView>
      <CameraAndGallery refRBSheet={refRBSheet} TakeImage={TakeImage} />
      <FloatingAction
        color={'#297eff'}
        floatingIcon={
          <MaterialIcons color="#fff" name="facebook-messenger" size={35} />
        }
        showBackground={false}
        onPressMain={() => {
          Linking.canOpenURL('fb-messenger://')
            .then((supported) => {
              if (!supported) {
                Linking.openURL('https://m.me/Instantsalaries');
              } else {
                Linking.openURL('fb-messenger://user-thread/103020051700333');
              }
            })
            .catch((err) => console.log(err));
        }}
      />
    </>
  );
};
const HomeScreen = () => {
  return (
    <HomeNavigator.Navigator>
      <HomeNavigator.Screen
        options={{headerShown: false}}
        name="HomeDashboard"
        component={HomeDashboard}
      />
      <HomeNavigator.Screen
        options={{
          headerShown: true,
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="ProfileUpdate"
        component={ProfileUpdate}
      />
      <HomeNavigator.Screen
        options={{
          title: 'Employee Information',
          headerShown: true,
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="ServiceUpdate"
        component={ServiceUpdate}
      />
      <HomeNavigator.Screen
        options={{
          title: 'Application for Instant Salary',
          headerShown: true,
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="SalaryApplication"
        component={SalaryApplication}
      />
      <HomeNavigator.Screen
        options={{
          headerShown: true,
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="Applications"
        component={Applications}
      />
      <HomeNavigator.Screen
        options={{
          headerShown: true,
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="Transactions"
        component={Transactions}
      />
      <HomeNavigator.Screen
        options={{
          headerShown: true,
          title: 'Application Fees',
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="ApplicationFee"
        component={ApplicationFee}
      />
      <HomeNavigator.Screen
        options={{
          headerShown: true,
          title: 'Transaction Method',
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="TransactionMethod"
        component={TransactionMethod}
      />
      <HomeNavigator.Screen
        options={{
          headerShown: true,
          title: 'Pay Back',
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: APPS_COLOR,
          },
        }}
        name="PayBack"
        component={PayBack}
      />
    </HomeNavigator.Navigator>
  );
};

export default HomeScreen;
