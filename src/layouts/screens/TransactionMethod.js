import React, {useState, createRef, useContext} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import BackHeader from '../header/BackHeader';
import {
  TRANSACTION_METHOD_URL,
  APPS_COLOR,
  BANKS,
  ROW_WIDTH,
} from '../../lib/const';
import RadioBtn from '../Form/RadioBtn';
import Input from '../Form/Input';
import {checkTransactionMethod} from '../../lib/funcs';
import http from '../../lib/http';
import {showMessage} from 'react-native-flash-message';
import {AppContext} from '../../context/AppContext';
import DropDownSelect from '../Form/DropDownSelect';

const TransactionMethod = ({navigation, route}) => {
  const {makeLoading, user, getUserApplications} = useContext(AppContext);
  const [method, setMethod] = useState('bank');
  const [bank_name, setBankName] = useState();
  const [branch_name, setBranch] = useState('');
  const [account_no, setAccNo] = useState('');
  const [agent_name, setAgent] = useState(null);
  const [mobile_no, setMobile] = useState(null);
  const mobileInputRef = createRef(null);
  const branchInputRef = createRef(null);
  const accNoInputRef = createRef(null);
  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        paddingTop: 20,
      }}>
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <RadioBtn
          label="Select Method"
          list={[
            {text: 'Bank Transaction', value: 'bank'},
            {text: 'Mobile Banking', value: 'mobile'},
          ]}
          value={method}
          onChange={(value) => setMethod(value)}
        />
        {method === 'bank' ? (
          <>
            <DropDownSelect
              changeText={setBankName}
              label="Bank Name"
              list={BANKS}
            />
            <Input
              label="Branch"
              inputRef={branchInputRef}
              onSubmitEditing={accNoInputRef}
              onChangeText={(value) => setBranch(value)}
              value={branch_name}
              placeholder="Branch name"
            />
            <Input
              inputRef={accNoInputRef}
              onChangeText={(value) => setAccNo(value)}
              value={account_no}
              placeholder="Account Number"
            />
          </>
        ) : (
          <View style={{marginTop: 10}}>
            <RadioBtn
              label="Agent"
              list={[
                {text: 'BKash', value: 'Bkash'},
                {text: 'Rocket', value: 'Rocket'},
                {text: 'Nagad', value: 'Nagad'},
              ]}
              value={agent_name}
              onChange={(value) => setAgent(value)}
            />
            <Input
              label="Mobile Account No."
              inputRef={mobileInputRef}
              onChangeText={(value) => setMobile(value)}
              value={mobile_no}
              keyboardType="phone-pad"
              placeholder="Mobile Account No."
            />
          </View>
        )}
        <TouchableOpacity
          onPress={() => {
            if (
              checkTransactionMethod({
                method,
                bank_name,
                branch_name,
                account_no,
                agent_name,
                mobile_no,
              })
            ) {
              makeLoading(true);
              var data = new FormData();
              data.append('serviceId', route?.params?.serviceId);
              data.append('bank_name', bank_name);
              data.append('branch_name', branch_name);
              data.append('account_no', account_no);
              data.append('agent_name', agent_name);
              data.append('mobile_no', mobile_no);
              http
                .postDataToken(
                  `${TRANSACTION_METHOD_URL}/${method}`,
                  data,
                  user.token,
                )
                .then((response) => {
                  showMessage({
                    message: response?.data.join(''),
                    type: 'success',
                  });
                  navigation.goBack();
                })
                .catch((e) => {
                  if (e?.response?.status === 422) {
                    showMessage({
                      message: e.response.data?.errors[
                        Object.keys(e.response.data?.errors)[0]
                      ].join(''),
                      type: 'danger',
                    });
                  }
                })
                .then(() => {
                  getUserApplications();
                  makeLoading(false);
                });
            }
          }}
          style={{
            alignItems: 'center',
            backgroundColor: APPS_COLOR,
            padding: 10,
          }}>
          <Text style={{color: '#ffffff', fontSize: 16}}>Submit</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default TransactionMethod;
