import React, {useContext, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {APPS_COLOR, ROW_WIDTH} from '../../lib/const';
import {AppContext} from '../../context/AppContext';
import UserApplication from '../components/UserApplication';
const Applications = ({navigation}) => {
  const {
    getUserApplications,
    getUserTransactions,
    applications,
    loading,
    transactions,
  } = useContext(AppContext);

  useEffect(() => {
    getUserApplications();
    getUserTransactions();
  }, []);

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <StatusBar backgroundColor={APPS_COLOR} barStyle="light-content" />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
          marginTop: 10,
        }}>
        {transactions.filter(
          (transaction) => transaction.payback_status === 'UNPAID',
        ).length > 0 && (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('PayBack', {
                transaction: transactions.find(
                  (transaction) => transaction.payback_status === 'UNPAID',
                ),
              });
            }}
            style={{
              alignSelf: 'center',
              padding: 10,
              backgroundColor: APPS_COLOR,
              width: ROW_WIDTH,
              alignItems: 'center',
            }}>
            <Text style={{color: '#fff'}}>Pay Back</Text>
          </TouchableOpacity>
        )}
        {applications.length > 0 ? (
          <>
            {applications.map((application, index) => (
              <UserApplication
                navigation={navigation}
                key={index}
                application={application}
              />
            ))}
          </>
        ) : (
          <>
            {!loading.is_loading && (
              <View
                style={{
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Text style={{color: '#db4949', fontSize: 18}}>
                  You have not applied yet
                </Text>
              </View>
            )}
          </>
        )}
      </View>
    </ScrollView>
  );
};

export default Applications;
