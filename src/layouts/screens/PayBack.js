import React, {useState, createRef, useContext, useEffect} from 'react';
import {
  Image,
  ScrollView,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import BackHeader from '../header/BackHeader';
import {
  PAYBACK_URL,
  APPS_COLOR,
  PLACEHOLDER_IMG,
  ROW_WIDTH,
} from '../../lib/const';
import RadioBtn from '../Form/RadioBtn';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import Input from '../Form/Input';
import {checkPaybackMethod} from '../../lib/funcs';
import http from '../../lib/http';
import {showMessage} from 'react-native-flash-message';
import {AppContext} from '../../context/AppContext';
import DropDownSelect from '../Form/DropDownSelect';

const PayBack = ({navigation, route}) => {
  const {
    makeLoading,
    user,
    getUserApplications,
    transactions,
    loading,
  } = useContext(AppContext);
  const [referralCode, setReferralCode] = useState('');
  const [amount, setAmount] = useState('0');
  const [method, setMethod] = useState('bank');
  const [marchent, setMarchent] = useState(null);
  const [transactionCode, setTransaction] = useState(null);
  const [mobileBankingNo, setMobile] = useState(null);
  const [paymentSlip, setImage] = useState(null);
  const mobileInputRef = createRef(null);
  const transInputRef = createRef(null);
  useEffect(() => {
    if (
      typeof transactions.find(
        (transaction) => transaction.payback_status === 'UNPAID',
      ) !== undefined
    ) {
      setAmount(
        transactions
          .find((transaction) => transaction.payback_status === 'UNPAID')
          .amount.toString(),
      );
      setReferralCode(
        transactions
          .find((transaction) => transaction.payback_status === 'UNPAID')
          .application_code.toString(),
      );
    }
  }, []);

  const changeReferralCode = (code) => {
    if (code.length > 0) {
      setReferralCode(code);
      setAmount(
        transactions
          .find((transaction) => transaction.application_code === code)
          .amount.toString(),
      );
    } else {
      setReferralCode('');
      setAmount('0');
    }
  };
  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        paddingTop: 20,
      }}>
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <DropDownSelect
          changeText={changeReferralCode}
          label="Referral Code"
          list={transactions
            .map((transaction, index) => {
              if (transaction.payback_status === 'UNPAID') {
                if (index === 0) {
                  return {
                    id: transaction.application_code,
                    name: transaction.application_code,
                    checked: true,
                  };
                }
                return {
                  id: transaction.application_code,
                  name: transaction.application_code,
                };
              }
            })
            .filter((transaction) => transaction)}
        />
        <Input value={amount} label="Amount" editable={false} />
        <RadioBtn
          label="Select Method"
          list={[
            {text: 'Bank Transaction', value: 'bank'},
            {text: 'Mobile Banking', value: 'mobile'},
          ]}
          value={method}
          onChange={(value) => setMethod(value)}
        />
        {method === 'bank' ? (
          <View
            style={{
              borderWidth: 1,
              borderColor: 'gray',
              padding: 10,
              marginTop: 10,
              marginBottom: 10,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 13,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              Deposit/Transfer Slip Image
            </Text>
            <Image
              style={{
                width: 300,
                height: 150,
                alignSelf: 'center',
                margin: 10,
                borderColor: 'gray',
                borderWidth: 0.5,
              }}
              resizeMethod="resize"
              source={
                paymentSlip == null ? PLACEHOLDER_IMG : {uri: paymentSlip.path}
              }
            />
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <View style={{borderColor: 'gray', borderWidth: 1, padding: 15}}>
                <TouchableHighlight
                  onPress={() =>
                    ImagePicker.openCamera({
                      mediaType: 'photo',
                      width: 600,
                      height: 600,
                      cropping: true,
                    })
                      .then((image) => {
                        setImage(image);
                      })
                      .catch((e) => {
                        console.log(e);
                      })
                  }>
                  <>
                    <Icon
                      style={{alignSelf: 'center'}}
                      color={'#000'}
                      name="camera"
                      size={34}
                    />
                    <Text style={{color: '#000'}}>Camera</Text>
                  </>
                </TouchableHighlight>
              </View>
              <View style={{borderColor: 'gray', borderWidth: 1, padding: 15}}>
                <TouchableOpacity
                  onPress={() =>
                    ImagePicker.openPicker({
                      mediaType: 'photo',
                      width: 600,
                      height: 600,
                      cropping: true,
                    })
                      .then((image) => {
                        setImage(image);
                      })
                      .catch((e) => {
                        console.log(e);
                      })
                  }>
                  <>
                    <Icon
                      style={{alignSelf: 'center'}}
                      name="image"
                      size={34}
                    />
                    <Text>Open Gallery</Text>
                  </>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        ) : (
          <View style={{marginTop: 10}}>
            <RadioBtn
              label="Agent"
              list={[
                {text: 'BKash', value: 'Bkash'},
                {text: 'Rocket', value: 'Rocket'},
                {text: 'Nagad', value: 'Nagad'},
              ]}
              value={marchent}
              onChange={(value) => setMarchent(value)}
            />
            <Input
              label="Transaction Code"
              inputRef={transInputRef}
              onSubmitEditing={mobileInputRef}
              onChangeText={(value) => setTransaction(value)}
              value={transactionCode}
              placeholder="Transaction Code"
            />
            <Input
              label="Mobile Account No."
              inputRef={mobileInputRef}
              onChangeText={(value) => setMobile(value)}
              value={mobileBankingNo}
              keyboardType="phone-pad"
              placeholder="Mobile Account No."
            />
          </View>
        )}
        <TouchableOpacity
          onPress={() => {
            if (loading.is_offline) {
              showMessage({
                message: 'Check your internet connection',
                type: 'danger',
              });
              return;
            }
            if (
              checkPaybackMethod({
                referralCode,
                amount,
                method,
                paymentSlip,
                marchent,
                transactionCode,
                mobileBankingNo,
              })
            ) {
              makeLoading(true);
              var data = new FormData();
              if (
                paymentSlip !== null &&
                typeof paymentSlip?.path !== undefined
              ) {
                data.append('paymentSlip', {
                  uri: paymentSlip.path,
                  type: 'image/jpeg',
                  name: 'paymentSlip',
                });
              }
              data.append('referralCode', referralCode);
              data.append('amount', amount);
              data.append('marchent', marchent);
              data.append('transactionCode', transactionCode);
              data.append('mobileBankingNo', mobileBankingNo);
              http
                .postDataToken(`${PAYBACK_URL}/${method}`, data, user.token)
                .then((response) => {
                  showMessage({
                    message: response.data.join(''),
                    type: 'success',
                  });
                  navigation.goBack();
                })
                .catch((e) => {
                  if (e.response.status === 422) {
                    showMessage({
                      message: e.response.data?.errors[
                        Object.keys(e.response.data?.errors)[0]
                      ].join(''),
                      type: 'danger',
                    });
                  }
                })
                .then(() => {
                  getUserApplications();
                  makeLoading(false);
                });
            }
          }}
          style={{
            alignItems: 'center',
            backgroundColor: APPS_COLOR,
            padding: 10,
          }}>
          <Text style={{color: '#ffffff', fontSize: 16}}>Pay Back</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default PayBack;
