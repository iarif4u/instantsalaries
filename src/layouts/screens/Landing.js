import {StatusBar, Text, View, ScrollView} from 'react-native';
import * as React from 'react';
import {APPS_COLOR, BASE_BG, BASE_F5, TEXT_DASH} from '../../lib/const';

const Landing = ({navigation}) => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: BASE_BG,
          padding: 10,
        }}>
        <View>
          <Text style={{fontSize: 19, color: TEXT_DASH}}>
            Instantsalaries is the only
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 19,
                backgroundColor: APPS_COLOR,
                padding: 3,
                paddingLeft: 8,
                paddingRight: 8,
                borderRadius: 5,
                color: '#ffffff',
                fontWeight: 'bold',
              }}>
              0% interest
            </Text>
            <Text style={{fontSize: 19, color: TEXT_DASH}}>
              {' '}
              platform in Bangladesh for
            </Text>
          </View>
          <Text style={{fontSize: 19, color: TEXT_DASH}}>
            solving financial crisis in case of emergency
          </Text>
        </View>
      </ScrollView>
    </>
  );
};
export default Landing;
