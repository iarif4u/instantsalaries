import React, {useContext, useEffect} from 'react';
import { ScrollView, StatusBar, Text, View } from "react-native";
import { APPS_COLOR, ROW_WIDTH } from "../../lib/const";
import BackHeader from '../header/BackHeader';
import {AppContext} from '../../context/AppContext';
import UserTransaction from '../components/UserTransaction';

const Transactions = ({navigation}) => {
  const {transactions, loading, getUserTransactions} = useContext(AppContext);
  useEffect(() => {
    getUserTransactions();
  }, []);
  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <StatusBar backgroundColor={APPS_COLOR} barStyle="light-content" />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        {transactions.length > 0 ? (
          <>
            {transactions.map((transaction, index) => {
              return <UserTransaction transaction={transaction} key={index} />;
            })}
          </>
        ) : (
          <>
            {!loading.is_loading && (
              <View
                style={{
                  alignItems: 'center',
                  padding: 10,
                  marginTop: 20,
                }}>
                <Text style={{color: '#db4949', fontSize: 18}}>
                  You have not any transaction yet
                </Text>
              </View>
            )}
          </>
        )}
      </View>
    </ScrollView>
  );
};
export default Transactions;
