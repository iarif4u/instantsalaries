import React from 'react';
import {StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import {DISPLAY_WIDTH, PRIVACY_IMG} from '../lib/const';

const PrivacyPolicy = () => {
  return (
    <ScrollView>
      <Image
        style={{width: DISPLAY_WIDTH, height: 210, top: 0, marginTop: 0}}
        resizeMode="cover"
        source={PRIVACY_IMG}
      />
      <View style={styles.content}>
        <Text style={styles.maincontent}>
          THIS PRIVACY POLICY (PRIVACY POLICY) SETS FORTH THE MODES OF
          COLLECTION, USE AND DISCLOSURE OF YOUR INFORMATION GATHERED ON THE
          PLATFORM. THIS PRIVACY POLICY APPLIES ONLY TO PERSONAL INFORMATION
          COLLECTED ON THE PLATFORM. THIS PRIVACY POLICY DOES NOT APPLY TO
          INFORMATION COLLECTED BY THE COMPANY IN OTHER WAYS,INCLUDING
          INFORMATION COLLECTED OFFLINE. PLEASE READ THIS PRIVACY POLICY
          CAREFULLY. BY CONTINUING TO USE THE SERVICES, OR ACCESS THE PLATFORM
          YOU AGREE TO THIS PRIVACY POLICY. IF YOU DO NOT AGREE TO THIS PRIVACY
          POLICY, YOU MAY NOT AVAIL THE SERVICES.
        </Text>
        <Text style={[styles.maincontent, styles.maincontentextra]}>
          Access to the Platform is subject to the Terms and Conditions (Terms)
          accessible on the Platform, capitalized terms used and not defined in
          this Privacy Policy, shall have the same meaning ascribed to them in
          the Terms.
        </Text>
        <Text style={styles.sectionhead}>INTRODUCTION</Text>
        <Text style={styles.sectiontext}>
          We, Alesha Solutions Limited (Company; We; us; our) have developed an
          application called instantsalaries (Application/ App) through which
          you may apply for a short term loan (Loan) to be granted by various
          co-operatives/non-banking financial company OR Banks (Lender), based
          on the loan agreement executed between you and LENDER. LENDER has
          appointed and authorized us to collect, store, authenticate, verify
          and distribute the Personal Information (defined below) as may be
          required by LENDER to sanction the Loan. The Personal Information
          shall be collected through the application form (Application Form)
          available on the Application.
        </Text>
        <Text style={styles.sectiontext}>
          We are committed to protect your privacy and have made this Privacy
          Policy to describe the procedures we adhere to for collecting, using,
          and disclosing the Personal Information. We recommend you to read this
          Privacy Policy carefully so that you understand our approach towards
          the use of your Personal Information.
        </Text>
        <Text style={styles.sectionhead}>PERSONAL INFORMATION COLLECTED</Text>
        <Text style={styles.sectiontext}>
          In order to avail the Services, you are required to register with the
          Company by logging in through online Platforms (defined below) (User
          Account). During the application process, you shall be required to
          share/upload certain personal information, your name, e-mail address,
          gender, date of birth, mobile number, passwords, photograph, mobile
          phone information including contact numbers, SMS and browsing history,
          data and login-in credentials of Third Party Platforms (defined
          below), financial information such as bank documents, salary slips,
          bank statements, NID card, bank account no., data from Credit
          Information Companies, data required for Know Your Customer
          compliances, requirement and other relevant details (Personal
          Information). You undertake that you shall be solely responsible for
          the accuracy and truthfulness of the Personal Information you share
          with us.
        </Text>
        <Text style={styles.sectiontext}>
          As part of the Services, you authorize us to import your details and
          Personal Information dispersed over Platforms. Third Party Platforms
          are social networking platforms, such as Facebook, LinkedIn and other
          similar platforms.
        </Text>
        <Text style={styles.sectiontext}>
          You understand and acknowledge that the Company reserves the right to
          track your location (Track) during the provision of the Services, and
          also in the event that you stop, cease, discontinue to use or avail
          the Services, through the deletion or uninstallation of the App or
          otherwise, till the event that your obligations to pay the Outstanding
          Amount(s) to LENDER, exist. Deletion, uninstallation, and/or
          discontinuation of our Services, shall not release you from the
          responsibility, obligation and liability to repay the Outstanding
          Amount(s).
        </Text>
        <Text style={styles.sectionhead}>GOVERNING STATUTE</Text>
        <Text style={styles.sectiontext}>
          This Privacy Policy is governed by and is compliant with the
          Information Technology (Reasonable Security Practices and Procedures
          and Sensitive Personal Data or Information) Rules, which is designed
          to protect Personal Information of the End-User(s) of the Services;
          and other applicable rules and regulations related to privacy.
        </Text>
        <Text style={styles.sectionhead}>COOKIES</Text>
        <Text style={styles.sectiontext}>
          We may set cookies to track your use of the Platform. Cookies are
          small encrypted files that a site or its service provider transfers to
          your device’s hard drive that enables the sites or service provider’s
          systems to recognize your device and capture and remember certain
          information. By using the Application, you signify your consent to our
          use of cookies.
        </Text>
        <Text style={styles.sectionhead}>DISCLOSURES</Text>
        <Text style={styles.sectiontext}>
          We do not sell, rent, lease your Personal Information to anybody and
          will never do so. Keeping this in mind, we may disclose your Personal
          Information in the following cases:
        </Text>
        <Text style={styles.sectionpointtext}>
          1.Administrators: We shall provide access to your Personal information
          to our authorized administrative(s) for internal business purposes,
          who shall be under confidentiality obligations towards the same.
        </Text>
        <Text style={styles.sectionpointtext}>
          2.Affiliates: We may provide Personal Information we collect to our
          affiliates. For example, we may disclose Personal Information to our
          affiliates in order to respond to your requests for information or the
          Services, or to help limit your receipt of marketing materials you
          have requested not to receive.
        </Text>
        <Text style={styles.sectionpointtext}>
          3.Business Partners: We may use certain trusted third party companies
          and individuals to help us provide, analyze, and improve the Services
          including but not limited to data storage, maintenance services,
          database management, credit bureaus, rating agencies, web analytics,
          payment processing, and improvement of the Platform’s features. These
          third parties may have access to your information only for purposes of
          performing these tasks on our behalf and under obligations similar to
          those in this Privacy Policy. We may disclose your Personal
          Information to partners who perform business functions or hosting
          services on our behalf and who may be located outside Bangladesh.
        </Text>
        <Text style={styles.sectionpointtext}>
          4.Service Providers: We may share your Personal Information with the
          service providers, including LENDER, who are working with us in
          connection with the operation of the Services or the Platform, so long
          as such service providers are subject to confidentiality restrictions
          consistent with this Privacy Policy.
        </Text>
        <Text style={styles.sectionpointtext}>
          5.Joint Marketing Arrangements: Where permitted by law, we may share
          your Personal Information with joint marketers with whom we have a
          marketing arrangement, we would require all such joint marketers to
          have written contracts with us that specify the appropriate use of
          your Personal Information, require them to safeguard your Personal
          Information, and prohibit them from making unauthorized or unlawful
          use of your Personal Information
        </Text>
        <Text style={styles.sectionpointtext}>
          6.Persons Who Acquire Our Assets or Business: If we sell or transfer
          any of our business or assets, certain Personal Information may be a
          part of that sale or transfer. In the event of such a sale or
          transfer, we will notify you.
        </Text>
        <Text style={styles.sectionpointtext}>
          7.Legal and Regulatory Authorities: We may be required to disclose
          your Personal Information due to legal or regulatory requirements. In
          such instances, we reserve the right to disclose your Personal
          Information as required in order to comply with our legal obligations,
          including but not limited to complying with court orders, warrants, or
          discovery requests. We may also disclose your Personal Information(a)
          to law enforcement officers or others; (b) to Credit Information
          Companies; (c) to comply with a judicial proceeding, court order, or
          legal process served on us or the Platform; (d) to enforce or apply
          this Privacy Policy or the Terms of Service or our other policies or
          Agreements; (e) for an insolvency proceeding involving all or part of
          the business or asset to which the information pertains; (f) respond
          to claims that any Personal Information violates the rights of
          third-parties; (g) or protect the rights, property, or personal safety
          of the Company, or the general public. You agree and acknowledge that
          we may not inform you prior to or after disclosures made according to
          this section.
        </Text>
        <Text style={styles.sectiontext}>
          Notwithstanding anything mentioned hereinabove, the Company shall not
          be responsible for the actions or omissions of the service providers
          or parties with whom the Personal Information is shared, nor shall the
          Company be responsible and/or liable for any additional information
          you may choose to provide directly to any service provider or any
          third party.
        </Text>
        <Text style={styles.sectionhead}>DATA RETENTION</Text>
        <Text style={styles.sectiontext}>
          We will retain your Personal Information for as long as your
          registration with us is valid and the Outstanding Amount(s) is due and
          payable to LENDER. We may also retain and use your Personal
          Information as necessary to comply with our legal obligations, resolve
          disputes, and enforce our agreements. Subject to this section, we will
          try to delete your Personal Information upon reasonable written
          request for the same. Please note, however, that there might be
          latency in deleting Personal Information from our servers and
          backed-up versions might exist even after deletion.
        </Text>
        <Text style={styles.sectionhead}>SECURITY</Text>
        <Text style={styles.sectiontext}>
          We value your Personal Information, and protect it on the Platform
          against loss, misuse or alteration by taking extensive security
          measures. In order to protect your Personal Information, we have
          implemented adequate technology and will update these measures as new
          technology becomes available, as appropriate. All Personal Information
          is securely stored on a secure cloud setup and all communication
          happens via bank-grade secure SSL communication channels. The Personal
          Information is stored on Google cloud. Although we provide appropriate
          firewalls and protections, we cannot warrant the security of any
          Personal Information transmitted as our systems are not hack proof.
          Data pilferage due to unauthorized hacking, virus attacks, technical
          issues is possible and we take no liabilities or responsibilities for
          it.
        </Text>
        <Text style={styles.sectiontext}>
          You are responsible for all actions that take place under your User
          Account. If you choose to share your User Account details and password
          or any Personal Information with third parties, you are solely
          responsible for the same. If you lose control of your User Account,
          you may lose substantial control over your Personal Information and
          may be subject to legally binding actions.
        </Text>
        <Text style={styles.sectionhead}>
          ACCESSING AND MODIFYING PERSONAL INFORMATION
        </Text>
        <Text style={styles.sectiontext}>
          In case you need to access, review, and/or make changes to the
          Personal Information, you shall have to login to your User Account and
          change the requisite details. You shall keep your Personal Information
          updated to help us better serve you.
        </Text>
        <Text style={{...styles.sectionhead, color: 'red'}}>
          ANCILLARY SERVICES
        </Text>
        <Text style={styles.sectiontext}>
          We may provide you with certain ancillary services such as chat rooms,
          blogs and reviews for the Services. Subject to any applicable laws,
          any communication shared by you via the Platform or through the blogs,
          reviews or otherwise to us (including without limitation contents,
          images, audio, financial information, feedback etc. collectively
          Feedback) is on a non-confidential basis, and we are under no
          obligation to refrain from reproducing, publishing or otherwise using
          it in any way or for any purpose. You shall be responsible for the
          content and information contained in any Feedback shared by you
          through the Platform or otherwise to us, including without limitation
          for its truthfulness and accuracy. Sharing your Feedback with us,
          constitutes an assignment to us of all worldwide rights, titles and
          interests in all copyrights and other intellectual property rights in
          the Feedback and you authorize us to use the Feedback for any purpose,
          which we may deem fit.
        </Text>
        <Text style={styles.sectionhead}>COMMUNICATIONS FROM THE PLATFORM</Text>
        <Text style={styles.sectiontext}>
          1.Special Offers and Updates: We may send you information on products,
          services, special deals, and a newsletter of the Company. Out of
          respect for your privacy, we present you with the option of not
          receiving these types of communications. You may unsubscribe via the
          unsubscribe mechanism provided in each such communication or by
          emailing us at care@instantsalaries.com
        </Text>
        <Text style={styles.sectiontext}>
          2.Service Announcements: On certain occasions or under law, we are
          required to send out Service or Platform related announcements. We
          respect your privacy, however you may not opt-out of these
          communications. These communications would not be promotional in
          nature.
        </Text>
        <Text style={styles.sectiontext}>
          3.Customer Service: We communicate with Customer(s) on a regular basis
          to provide requested services and in regards to issues relating to
          their User Account, we reply via email or phone, based on Customer(s)
          requirements and convenience
        </Text>
        <Text style={styles.sectionhead}>INDEMNIFICATION</Text>
        <Text style={styles.sectiontext}>
          You agree to indemnify us, our subsidiaries, affiliates, officers,
          agents, co-branders or other partners, and employees and hold us
          harmless from and against any claims and demand, including reasonable
          attorneys fees, made by any third party arising out of or relating to:
          (i) Personal Information and contents that you submit or share through
          the Platform; (ii) your violation of this Privacy Policy, (iii) or
          your violation of rights of another Customer(s).
        </Text>
        <Text style={styles.sectionhead}>LIMITATIONS OF LIABILITY</Text>
        <Text style={styles.sectiontext}>
          You expressly understand and agree that the Company shall not be
          liable for any direct, indirect, incidental, special, consequential or
          exemplary damages, including but not limited to, damages for loss of
          profits, goodwill, use, data, information, details or other intangible
          losses (even if the Company has been advised of the possibility of
          such damages), resulting from: (i) the use or the inability to use the
          Services; (ii) unauthorized access to or alteration of your Personal
          Information.
        </Text>
        <Text style={styles.sectionhead}>GOVERNING LAWS AND DUTIES</Text>
        <Text style={styles.sectiontext}>
          You expressly understand and agree that the Company, including its
          directors, officers, employees, representatives or the service
          provider, shall not be liable for any direct, indirect, incidental,
          special, consequential or exemplary damages, including but not limited
          to, damages for loss of profits, goodwill, use, data or other
          intangible losses (even if the Company has been advised of the
          possibility of such damages), resulting from; (a) use or the inability
          to avail the Services (b) inability to use the Platform (c) failure or
          delay in providing the Services or access to the Platform (d) any
          performance or non-performance by the Company (e) any damages to or
          viruses that may infect your electronic devices or other property as
          the result of your access to the Platform or your downloading of any
          content from the Platform and (f) server failure or otherwise or in
          any way relating to the Services.
        </Text>
        <Text style={styles.sectionhead}>FORCE MAJEURE</Text>
        <Text style={styles.sectiontext}>
          This Agreement shall be construed and governed by the laws of
          Bangladesh over such disputes without regard to principles of conflict
          of laws.
        </Text>
        <Text style={styles.sectionhead}>CHANGES TO THIS POLICY</Text>
        <Text style={styles.sectiontext}>
          We may update this Privacy Policy without notice to you. You are
          encouraged to check this Privacy Policy on a regular basis to be aware
          of the changes made to it. Continued use of the Services and access to
          the Platform shall be deemed to be your acceptance of this Privacy
          Policy.
        </Text>
        <Text style={styles.sectionhead}>CONTACT US</Text>
        <Text style={styles.sectiontext}>
          If you have questions, concerns or grievances regarding this Privacy
          Policy, you can email us at our grievance email-address:
          care@instantsalaries.com
        </Text>
      </View>
    </ScrollView>
  );
};

export default PrivacyPolicy;

const styles = StyleSheet.create({
  header: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 50,
    marginBottom: 90,
    borderBottomWidth: 4,
    borderBottomColor: 'white',
  },
  bgimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  bgyellow: {
    backgroundColor: '#f47f20cb',
  },
  content: {
    backgroundColor: 'white',
    marginTop: -10,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
  },
  maincontent: {
    color: '#f48120',
    fontWeight: 'bold',
    textAlign: 'justify',
    fontSize: 14,
  },
  maincontentextra: {
    fontSize: 15,
  },
  sectionhead: {
    marginTop: 15,
    marginBottom: 5,
    fontWeight: 'bold',
    fontSize: 17,
  },
  sectiontext: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
  },
  sectionpointtext: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
    paddingLeft: 10,
  },
});
