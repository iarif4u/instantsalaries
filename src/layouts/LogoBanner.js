import {LOGO, ROW_WIDTH} from '../lib/const';
import {Image, View} from 'react-native';
import React from 'react';

const LogoBanner = () => {
  return (
    <View
      style={{
        flex: 3,
        alignItems: 'center',
        width: ROW_WIDTH * 0.8,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
      }}>
      <Image
        style={{width: '60%', height: 80, marginBottom: 10}}
        resizeMode={'stretch'}
        source={LOGO}
      />
    </View>
  );
};
export default LogoBanner;
