import React, {useContext, useEffect, createRef, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  StatusBar,
} from 'react-native';

import {APPS_COLOR, AVATAR, PLACEHOLDER_IMG, ROW_WIDTH} from '../../lib/const';
import {AppContext} from '../../context/AppContext.js';
import Input from '../Form/Input';
import RadioBtn from './RadioBtn';
import SelectDropdown from './SelectDropdown';
import DatePicker from './DatePicker';
import CameraAndGallery from '../components/CameraAndGallery';

const ProfileUpdate = ({navigation}) => {
  const {
    profileUpdate,
    updateProfile,
    profile,
    getUserProfile,
    TakeImage,
    TakeSignature,
    TakeNIDFront,
    TakeNIDBack,
  } = useContext(AppContext);

  const nameInputRef = createRef(null);
  const fatherInputRef = createRef(null);
  const motherInputRef = createRef(null);
  const presentAddrRef = createRef(null);
  const permanentAddrRef = createRef(null);
  const emailInputRef = createRef(null);
  const mobileInputRef = createRef(null);
  const tinRef = createRef(null);
  const nidInputRef = createRef(null);
  const refRBSheet = useRef();
  const signatureRBSheet = useRef();
  const nidFrontRBSheet = useRef();
  const nidBackRBSheet = useRef();
  useEffect(() => {
    getUserProfile();
  }, []);

  const changeFullName = (value) => {
    updateProfile({...profile, full_name: value});
  };
  const changeFatherName = (value) => {
    updateProfile({...profile, father_name: value});
  };
  const changeMotherName = (value) => {
    updateProfile({...profile, mother_name: value});
  };
  const changeGender = (value) => {
    updateProfile({...profile, gender: value});
  };
  const changeDateOfBirth = (value) => {
    updateProfile({...profile, dob: value});
  };
  const changePresentAddress = (value) => {
    updateProfile({...profile, present_address: value});
  };
  const changePermanentAddress = (value) => {
    updateProfile({...profile, permanent_address: value});
  };
  const changeReligion = (value) => {
    updateProfile({...profile, religion: value});
  };
  const changeEmail = (value) => {
    updateProfile({...profile, contact_email: value});
  };
  const changeContactNo = (value) => {
    updateProfile({...profile, contact_no: value});
  };
  const changeTin = (value) => {
    updateProfile({...profile, tin: value});
  };
  const changeNid = (value) => {
    updateProfile({...profile, nid: value});
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        paddingTop: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <StatusBar backgroundColor={APPS_COLOR} barStyle="light-content" />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginRight: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 13,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              Photo
            </Text>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                marginTop: 5,
              }}
              onPress={() => refRBSheet.current.open()}>
              {profile.photo == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={AVATAR}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: profile.photo,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginLeft: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 8,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              Signature
            </Text>
            <TouchableOpacity
              style={{alignItems: 'center', marginTop: 5}}
              onPress={() => signatureRBSheet.current.open()}>
              {profile.signature == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: profile.signature,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View style={{marginBottom: 15}}>
          <Text
            style={{
              borderBottomColor: '#297eff',
              borderBottomWidth: 1,
              width: ROW_WIDTH / 5.2,
              paddingBottom: 5,
            }}>
            Upload NID
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginRight: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 13,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              NID Front
            </Text>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                marginTop: 5,
              }}
              onPress={() => nidFrontRBSheet.current.open()}>
              {profile.nid_front == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: profile.nid_front,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginLeft: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 8,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              NID Back
            </Text>
            <TouchableOpacity
              style={{alignItems: 'center', marginTop: 5}}
              onPress={() => nidBackRBSheet.current.open()}>
              {profile.nid_back == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: profile.nid_back,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <>
          <Input
            inputRef={nameInputRef}
            onSubmitEditing={fatherInputRef}
            onChangeText={changeFullName}
            value={profile.full_name}
            placeholder="Full Name"
          />
          <Input
            inputRef={fatherInputRef}
            onSubmitEditing={motherInputRef}
            onChangeText={changeFatherName}
            value={profile.father_name}
            placeholder="Father Name"
          />
          <Input
            inputRef={motherInputRef}
            onChangeText={changeMotherName}
            value={profile.mother_name}
            placeholder="Mother Name"
          />
          <RadioBtn
            label="Gender"
            list={[
              {text: 'Male', value: 'MALE'},
              {text: 'Female', value: 'FEMALE'},
            ]}
            value={profile.gender}
            onChange={changeGender}
          />
          <DatePicker
            label="Date of Birth"
            value={profile.dob}
            minYearAgo={18}
            onDateChange={changeDateOfBirth}
          />
          <Input
            onChangeText={changePresentAddress}
            inputRef={presentAddrRef}
            onSubmitEditing={permanentAddrRef}
            multiline={true}
            numberOfLines={3}
            value={profile.present_address}
            placeholder="Present Address"
          />
          <Input
            onChangeText={changePermanentAddress}
            inputRef={permanentAddrRef}
            multiline={true}
            numberOfLines={3}
            value={profile.permanent_address}
            placeholder="Permanent Address"
          />
          <SelectDropdown
            label="Religion"
            list={[
              {text: 'ISLAM', value: 'ISLAM'},
              {text: 'HINDUISM', value: 'HINDUISM'},
              {text: 'CHRISTIANISM', value: 'CHRISTIANISM'},
              {text: 'BUDDHISM', value: 'BUDDHISM'},
              {text: 'OTHER', value: 'OTHER'},
            ]}
            value={profile.religion}
            changeText={changeReligion}
          />
          <Input
            inputRef={emailInputRef}
            onSubmitEditing={mobileInputRef}
            onChangeText={changeEmail}
            value={profile.contact_email}
            keyboardType="email-address"
            placeholder="Email Address"
          />
          <Input
            inputRef={mobileInputRef}
            onSubmitEditing={tinRef}
            onChangeText={changeContactNo}
            value={profile.contact_no}
            keyboardType="phone-pad"
            placeholder="Mobile no"
          />
          <Input
            keyboardType="numeric"
            inputRef={nidInputRef}
            onSubmitEditing={tinRef}
            onChangeText={changeNid}
            value={profile.nid === 'null' ? '' : profile.nid}
            placeholder="NID Number"
          />
          <Input
            inputRef={tinRef}
            onChangeText={changeTin}
            value={profile.tin}
            keyboardType="numeric"
            placeholder="TIN Number"
          />

          <TouchableOpacity
            onPress={() => {
              if (profile.religion === null) {
                changeReligion('ISLAM');
              }
              profileUpdate((is_done) => {
                if (is_done) {
                  navigation.navigate('HomeDashboard');
                }
              });
            }}
            style={{
              backgroundColor: APPS_COLOR,
              padding: 15,
              ...styles.inputContainer,
            }}>
            <Text style={{color: '#ffffff', alignSelf: 'center'}}>Update</Text>
          </TouchableOpacity>
        </>
      </View>
      <CameraAndGallery refRBSheet={refRBSheet} TakeImage={TakeImage} />
      <CameraAndGallery
        refRBSheet={signatureRBSheet}
        TakeImage={TakeSignature}
      />
      <CameraAndGallery refRBSheet={nidFrontRBSheet} TakeImage={TakeNIDFront} />
      <CameraAndGallery refRBSheet={nidBackRBSheet} TakeImage={TakeNIDBack} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  inputContainer: {
    paddingLeft: 10,
    borderRadius: 10,
    borderWidth: 0.01,
    borderColor: '#c9c9c9',
    marginBottom: 15,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
  },
});
export default ProfileUpdate;
