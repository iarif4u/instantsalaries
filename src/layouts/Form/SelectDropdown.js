import {Picker} from '@react-native-picker/picker';
import React from 'react';
import {Text, View} from 'react-native';
const SelectDropdown = ({label, list, value, changeText}) => {
  return (
    <View
      style={{
        paddingBottom: 5,
        borderColor: '#0000000',
        borderWidth: 1,
        marginBottom: 10,
      }}>
      <Text
        style={{
          position: 'absolute',
          marginTop: -15,
          marginLeft: 13,
          backgroundColor: '#fff',
          padding: 5,
          color: 'gray',
        }}>
        {label}
      </Text>
      <Picker
        selectedValue={value}
        onValueChange={(itemValue, itemIndex) => changeText(itemValue)}>
        {list.map((item, index) => (
          <Picker.Item
            key={index}
            label={item.text.toString()}
            value={item.value.toString()}
          />
        ))}
      </Picker>
    </View>
  );
};

export default SelectDropdown;
