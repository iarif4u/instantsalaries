import React, {useContext, createRef, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image, StatusBar,
} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import {
  APPS_COLOR,
  PLACEHOLDER_IMG,
  ROW_WIDTH,
  TEXT_BLACK,
} from '../../lib/const';
import {AppContext} from '../../context/AppContext';
import Input from './Input';
import DatePicker from './DatePicker';
import CameraAndGallery from '../components/CameraAndGallery';

const ServiceUpdate = ({navigation}) => {
  const {
    service,
    TakeOfficeFrontId,
    TakeOfficeBackId,
    changeCompanyName,
    changeCompanyAddress,
    changeDesignation,
    changeJoiningDate,
    changeSalary,
    changeExperience,
    submitServices,
  } = useContext(AppContext);
  const nameInputRef = createRef(null);
  const addrInputRef = createRef(null);
  const designationInputRef = createRef(null);
  const salaryInputRef = createRef(null);
  const experienceInputRef = createRef(null);
  const refRBSheet = useRef();
  const backRBSheet = useRef();

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        paddingTop: 20,
      }}>
      <StatusBar backgroundColor={APPS_COLOR} barStyle="light-content" />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginRight: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 13,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              Office ID Front
            </Text>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                marginTop: 5,
              }}
              onPress={() => refRBSheet.current.open()}>
              {service.oid_front == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: service.oid_front,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginLeft: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 8,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              Office ID Back
            </Text>
            <TouchableOpacity
              style={{alignItems: 'center', marginTop: 5}}
              onPress={() => backRBSheet.current.open()}>
              {service.oid_back == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: service.oid_back,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <Input
          inputRef={nameInputRef}
          onSubmitEditing={addrInputRef}
          onChangeText={changeCompanyName}
          value={service.company}
          placeholder="Company Name"
        />
        <Input
          inputRef={addrInputRef}
          onSubmitEditing={designationInputRef}
          onChangeText={changeCompanyAddress}
          value={service.address}
          placeholder="Company Address"
        />
        <Input
          inputRef={designationInputRef}
          onChangeText={changeDesignation}
          value={service.designation}
          placeholder="Designation"
        />
        <DatePicker
          label="Joining Date"
          value={service.joining_date}
          onDateChange={changeJoiningDate}
        />
        <Input
          onChangeText={changeSalary}
          inputRef={salaryInputRef}
          onSubmitEditing={experienceInputRef}
          value={service.salary?.toString()}
          placeholder="Salary"
          keyboardType="numeric"
        />
        <Input
          inputRef={experienceInputRef}
          onChangeText={changeExperience}
          value={service.experience}
          placeholder="Experience"
          keyboardType="numeric"
        />

        <TouchableOpacity
          onPress={() =>
            submitServices((is_done) => {
              if (is_done) {
                navigation.navigate('HomeDashboard');
              }
            })
          }
          style={{
            backgroundColor: APPS_COLOR,
            padding: 15,
            ...styles.inputContainer,
          }}>
          <Text style={{color: '#ffffff', alignSelf: 'center'}}>Update</Text>
        </TouchableOpacity>
      </View>
      <CameraAndGallery refRBSheet={refRBSheet} TakeImage={TakeOfficeFrontId} />
      <CameraAndGallery refRBSheet={backRBSheet} TakeImage={TakeOfficeBackId} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  inputContainer: {
    paddingLeft: 10,
    borderRadius: 10,
    borderWidth: 0.01,
    borderColor: '#c9c9c9',
    marginBottom: 15,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
  },
});
export default ServiceUpdate;
