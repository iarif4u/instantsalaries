import React, {useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const DatePicker = ({label, value, onDateChange, minYearAgo = 0}) => {
  const [show, setShow] = useState(false);
  const showDatepicker = () => {
    setShow(true);
  };
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (typeof selectedDate !== 'undefined') {
      onDateChange(selectedDate.toISOString().split('T')[0]);
    }
  };
  return (
    <>
      <View
        style={{
          borderWidth: 1,
          borderColor: 'gray',
          padding: 2,
          marginTop: 10,
          marginBottom: 10,
        }}>
        <Text
          style={{
            position: 'absolute',
            marginTop: -15,
            marginLeft: 13,
            backgroundColor: '#fff',
            padding: 5,
            color: 'gray',
          }}>
          {label}
        </Text>
        <TouchableOpacity style={{padding: 15}} onPress={showDatepicker}>
          <Text>{value ? value : 'YYYY-MM-DD'}</Text>
        </TouchableOpacity>
      </View>
      {show && (
        <DateTimePicker
          testID="joinDatePicker"
          value={value == null ? new Date() : new Date(value)}
          mode={'date'}
          display="default"
          maximumDate={new Date().setFullYear(
            new Date().getFullYear() - minYearAgo,
          )}
          onChange={onChange}
        />
      )}
    </>
  );
};
export default DatePicker;
