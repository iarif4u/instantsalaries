import React, {createRef, useContext, useRef, useState} from 'react';
import {
  Image,
  ScrollView, StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  APPS_COLOR,
  PLACEHOLDER_IMG,
  ROW_WIDTH,
  TEXT_BLACK,
} from '../../lib/const';
import Icon from 'react-native-vector-icons/Ionicons';
import Input from './Input';
import {AppContext} from '../../context/AppContext';
import SelectDropdown from './SelectDropdown';
import CameraAndGallery from '../components/CameraAndGallery';
import ReferrerInfo from '../components/ReferrerInfo';

const SalaryApplication = ({navigation}) => {
  const {
    application,
    profile,
    updateApplication,
    applySalary,
    TakeTINImage,
    TakeSalaryStatementImage,
  } = useContext(AppContext);
  const [referrers, setReferrers] = useState(['', '']);
  const appDetailsInputRef = createRef(null);
  const tinRBSheet = useRef();
  const ssRBSheet = useRef();
  const changeSalaryDate = (value) => {
    updateApplication({...application, salary_expected_date: value});
  };
  const changeDetails = (value) => {
    updateApplication({...application, details: value});
  };

  const salaries_dates = () => {
    const dates = [];
    for (var i = 1; i <= 25; i++) {
      dates.push({text: i, value: i});
    }
    return dates;
  };
  return (
    <ScrollView
      style={{backgroundColor: '#ffffff'}}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}
      keyboardShouldPersistTaps={'handled'}>
      <StatusBar backgroundColor={APPS_COLOR} barStyle="light-content" />
      <View
        style={{
          flex: 3,
          width: ROW_WIDTH,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
            paddingTop: 20,
          }}>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginRight: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 8,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              TIN
            </Text>
            <TouchableOpacity
              style={{alignItems: 'center', marginTop: 5}}
              onPress={() => tinRBSheet.current.open()}>
              {profile?.tin_doc == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: profile?.tin_doc,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              padding: 10,
              borderColor: 'gray',
              marginLeft: 5,
              borderWidth: 1,
            }}>
            <Text
              style={{
                position: 'absolute',
                marginTop: -15,
                marginLeft: 13,
                backgroundColor: '#fff',
                padding: 5,
                color: 'gray',
              }}>
              Salary Statement
            </Text>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                marginTop: 5,
              }}
              onPress={() => ssRBSheet.current.open()}>
              {application?.salary_statement?.path == null ? (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={PLACEHOLDER_IMG}
                />
              ) : (
                <Image
                  style={{width: 140, height: 100}}
                  resizeMethod="resize"
                  source={{
                    uri: application?.salary_statement?.path,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <Input
          onChangeText={changeDetails}
          value={application.details}
          inputRef={appDetailsInputRef}
          placeholder="Details"
        />
        <SelectDropdown
          label="Salary Expected Date"
          list={salaries_dates()}
          value={application.salary_expected_date}
          changeText={changeSalaryDate}
        />
        {referrers.map((el, i) => {
          return (
            <ReferrerInfo
              removeRef={(index) => {
                setReferrers(
                  referrers.filter((ref, refIndex) => refIndex !== index),
                );
              }}
              index={i}
              key={i}
            />
          );
        })}
        <TouchableOpacity
          onPress={() => setReferrers([...referrers, ''])}
          style={{
            flexDirection: 'row',
            padding: 10,
            justifyContent: 'space-around',
          }}>
          <Icon name="add" color="#000" size={18} style={{flex: 1}} />
          <Text style={{flex: 8}}>Add Referrer Info</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            applySalary((is_done) => {
              if (is_done) {
                navigation.navigate('HomeDashboard');
              }
            })
          }
          style={{
            backgroundColor: APPS_COLOR,
            padding: 15,
            ...styles.inputContainer,
          }}>
          <Text style={{color: '#ffffff', alignSelf: 'center'}}>Apply</Text>
        </TouchableOpacity>
      </View>
      <CameraAndGallery refRBSheet={tinRBSheet} TakeImage={TakeTINImage} />
      <CameraAndGallery
        refRBSheet={ssRBSheet}
        TakeImage={TakeSalaryStatementImage}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  inputContainer: {
    paddingLeft: 10,
    borderRadius: 10,
    borderWidth: 0.01,
    borderColor: '#c9c9c9',
    marginBottom: 15,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
  },
});

export default SalaryApplication;
