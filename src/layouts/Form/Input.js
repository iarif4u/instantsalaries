import React, {useState} from 'react';
import {TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import {Keyboard} from 'react-native';

const Input = ({
  value,
  onChangeText,
  label,
  placeholder,
  keyboardType = 'default',
  secureTextEntry,
  multiline = false,
  numberOfLines = 1,
  inputRef = null,
  onSubmitEditing = false,
  returnKeyType = 'next',
  editable = true,
}) => {
  const [show, setShow] = useState(secureTextEntry || false);
  return (
    <>
      {secureTextEntry ? (
        <TextInput
          mode="outlined"
          label={label || placeholder}
          style={{backgroundColor: '#ffffff', marginBottom: 15}}
          value={value == null ? '' : value}
          onChangeText={onChangeText}
          secureTextEntry={show}
          placeholder={placeholder || ''}
          keyboardType={keyboardType}
          multiline={multiline}
          numberOfLines={numberOfLines}
          blurOnSubmit={false}
          ref={inputRef}
          returnKeyType={returnKeyType}
          onSubmitEditing={onSubmitEditing ? onSubmitEditing : Keyboard.dismiss}
          right={
            <TextInput.Icon
              name={() => {
                return <Icon name={show ? 'eye-off' : 'eye'} size={20} />;
              }}
              onPress={() => {
                setShow(!show);
              }}
            />
          }
        />
      ) : (
        <>
          {onSubmitEditing ? (
            <TextInput
              theme={{roundness: 0}}
              mode="outlined"
              label={label || placeholder}
              style={{backgroundColor: '#ffffff', marginBottom: 15}}
              value={value == null || value === 'null' ? '' : value}
              onChangeText={onChangeText}
              ref={inputRef}
              placeholder={placeholder || ''}
              keyboardType={keyboardType}
              multiline={multiline}
              blurOnSubmit={false}
              returnKeyType={returnKeyType}
              onSubmitEditing={() =>
                onSubmitEditing.current && onSubmitEditing.current.focus()
              }
              numberOfLines={numberOfLines}
              editable={editable}
            />
          ) : (
            <TextInput
              theme={{roundness: 0}}
              mode="outlined"
              style={{backgroundColor: '#ffffff', marginBottom: 15}}
              label={label || placeholder}
              value={value == null || value === 'null' ? '' : value}
              onChangeText={onChangeText}
              ref={inputRef}
              placeholder={placeholder || ''}
              keyboardType={keyboardType}
              multiline={multiline}
              blurOnSubmit={false}
              returnKeyType={returnKeyType}
              onSubmitEditing={Keyboard.dismiss}
              numberOfLines={numberOfLines}
              editable={editable}
            />
          )}
        </>
      )}
    </>
  );
};
export default Input;
