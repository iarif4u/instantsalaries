import React from 'react';
import {Text, View} from 'react-native';
import Select2 from 'react-native-select-two';
import {APPS_COLOR} from '../../lib/const';

const DropDownSelect = ({label, list, changeText}) => {
  return (
    <View
      style={{
        borderWidth: 1,
        borderColor: 'gray',
        padding: 15,
        marginBottom: 15,
        marginTop: 15,
      }}>
      <Text
        style={{
          position: 'absolute',
          marginTop: -17,
          marginLeft: 13,
          backgroundColor: '#fff',
          padding: 5,
          color: 'gray',
        }}>
        {label}
      </Text>
      <Select2
        isSelectSingle
        style={{
          borderRadius: 5,
          padding: 0,
          color: 'red',
        }}
        colorTheme={APPS_COLOR}
        searchPlaceHolderText={label}
        cancelButtonText="Cancel"
        selectButtonText="Select"
        popupTitle={`Select ${label}`}
        title={label}
        data={list}
        onSelect={(data) => {
          if (typeof data[0] !== 'undefined') {
            changeText(data[0]);
          } else {
            changeText(data);
          }
        }}
      />
    </View>
  );
};

export default DropDownSelect;
