import React, {Fragment} from 'react';
import {Text, View} from 'react-native';
import {RadioButton} from 'react-native-paper';

const RadioBtn = ({label, list, value, onChange}) => {
  return (
    <View
      style={{
        paddingBottom: 5,
        borderColor: '#0000000',
        borderWidth: 1,
        marginBottom: 10,
        marginTop: 5,
      }}>
      <Text
        style={{
          position: 'absolute',
          marginTop: -15,
          marginLeft: 13,
          backgroundColor: '#fff',
          padding: 5,
          color: 'gray',
        }}>
        {label}
      </Text>
      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
        {list.map((item, index) => {
          return (
            <Fragment key={index}>
              <RadioButton
                value={item.value}
                status={value == item.value ? 'checked' : 'unchecked'}
                onPress={() => onChange(item.value)}
              />
              <Text>{item.text}</Text>
            </Fragment>
          );
        })}
      </View>
    </View>
  );
};
export default RadioBtn;
