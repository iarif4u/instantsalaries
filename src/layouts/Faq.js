import {
  Collapse,
  CollapseBody,
  CollapseHeader,
} from 'accordion-collapse-react-native';
import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {DISPLAY_WIDTH, FAQ_IMAGE} from '../lib/const';
const QA = (props) => {
  return (
    <Collapse>
      <CollapseHeader>
        <Text style={styles.collapseHeader}>{props.question}</Text>
      </CollapseHeader>
      <CollapseBody>
        <Text style={styles.sectiontextcontent}>{props.answer}</Text>
      </CollapseBody>
    </Collapse>
  );
};
const Faq = () => {
  return (
    <ScrollView>
      <Image
        source={FAQ_IMAGE}
        resizeMethod="resize"
        resizeMode={'cover'}
        style={{width: DISPLAY_WIDTH, height: 200}}
      />
      <View style={styles.maincontent}>
        <Text style={styles.sectionhead}>About instantsalaries</Text>
        <Text style={styles.sectiontext}>We are here to help!</Text>
        <Text style={styles.sectionhead}>Application Process</Text>
        <QA
          question="What is the application process for instant salary?"
          answer="Just register and give us the necessary information, we will knock and
          approve your proposal within 30minutes (Subject to information
          authentication)."
        />
        <QA
          question="Do I need to submit an authorization letter from HRD?"
          answer="Yes, if your company has no corporate deal with us. And you fail
          to use deposit reference code or your references deny to give your
          referral."
        />
        <QA
          question="How can I get an emergency loan?"
          answer="Fill up the information properly. We will call the concerns
          (hospitals, pharmacy, grocery chain, headmaster or any other
          applicable), verify and disburse."
        />
        <Text style={styles.sectionhead}>Security</Text>
        <QA
          question="If I provide deposit code to anyone, will I be liable?"
          answer="Yes, you will not be able to withdraw your fund unless the credit
          taker returns his loan amount or transfer deposit or reference code.
          You will be liable if he/she does not return the loan within 15days of
          expiry."
        />
        <QA
          question="Will you share our information to any third party?"
          answer="Never, if not requested or ordered from court or legal authorities."
        />

        <Text style={styles.sectionhead}>Status of Applications</Text>
        <QA
          question="How long does it take for the final approval?"
          answer="Once you complete all required steps on the app, it will take around 1
          - 2 working days to process your application."
        />
        <QA
          question="Why was my application rejected?"
          answer="Once you complete all required steps on the app, it will take around 1
          - 2 working days to process your application."
        />

        <QA
          question="Can I reapply my application?"
          answer='What do I have to do? You can reapply after at least 3 months from the
          date of disapproval. Please contact us for further assistance. Select
          "No" to contact our Support.'
        />
        <QA
          question="Who can I choose as my reference details?"
          answer="Reference is considered valid if it belongs to the following
          categories: Father/ Mother, Son/ Daughter, Brother/ Sister, Wife/
          Husband, Friends, Colleagues, Relatives."
        />
        <Text style={styles.sectionhead}>Fees</Text>
        <QA
          question="Do I need to pay any interest?"
          answer="No, this service is interest free and no hidden costs over any term.
          You just need to pay a subscription fee (only if any credit service is
          taken). No subscription fee for the depositors."
        />
        <QA
          question="Are there any charges of disbursement or deposit return?"
          answer="No, if you request to make a disbursement through the bank. There are
          no charges."
        />

        <Text style={styles.sectionhead}>Get Money and Repay</Text>
        <QA
          question="Do I need to pay interest or charges if I take the loan repeatedly?"
          answer="No, if you are subscribed, you can avail your limit loan 24*7 as many
          times as you want without any extra charges."
        />
        <QA
          question="Will my references be liable for my loan?"
          answer="Yes, if you cannot return your loan amount within 15days of your
          credit expiry."
        />
        <QA
          question="What will be the impact of late return?"
          answer="If you fail to make the return on time, we will give you 3 days. If
          not returned within 3days. You will lose the confidence and your
          references will be requested to call you and assure the return. After
          10 days you will be black listed and we will apply the recovery
          process."
        />
      </View>
    </ScrollView>
  );
};

export default Faq;

const styles = StyleSheet.create({
  header: {
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 50,
    marginBottom: 90,
    borderBottomWidth: 4,
    borderBottomColor: 'white',
  },
  bgimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  bgyellow: {
    backgroundColor: '#f47f20cb',
  },
  maincontent: {
    color: '#f48120',
    fontWeight: 'bold',
    textAlign: 'justify',
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
  },
  sectionhead: {
    marginTop: 15,
    marginBottom: 15,
    fontWeight: 'bold',
    fontSize: 22,
  },
  sectiontext: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
    paddingLeft: 5,
    paddingRight: 5,
  },
  collapseHeader: {
    marginBottom: 3,
    fontSize: 15,
    textAlign: 'justify',
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 7,
    paddingBottom: 7,
    borderLeftColor: '#f47f20cb',
    borderLeftWidth: 3,
  },
  sectiontextcontent: {
    fontSize: 15,
    fontFamily: 'Cochin',
    textAlign: 'justify',
    backgroundColor: 'white',
    padding: 10,
    marginTop: 10,
    marginBottom: 15,
  },
});
