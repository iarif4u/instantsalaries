import React, {useContext} from 'react';
import {
  Modal,
  StyleSheet,
  View,
  ActivityIndicator,
  Image,
  ImageBackground,
} from 'react-native';
import {AppContext} from '../context/AppContext';
import {APPS_COLOR, POCKET} from '../lib/const';

const Activity = () => {
  const {loading} = useContext(AppContext);
  return (
    <Modal
      statusBarTranslucent
      animated={false}
      transparent={true}
      visible={loading.is_loading}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ImageBackground
            resizeMode="center"
            style={{
              width: '100%',
              height: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            source={POCKET}>
            <ActivityIndicator
              style={{alignSelf: 'center'}}
              size="large"
              color="#fff"
            />
          </ImageBackground>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Activity;
