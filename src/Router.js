import React, {useContext, useEffect, useState} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {createStackNavigator} from '@react-navigation/stack';

import LoginForm from './layouts/auth/Login';
import Home from './layouts/auth/Home';
import Registration from './layouts/auth/Registration';
import {AppContext} from './context/AppContext';
import Loading from './resource/Loading';
import {BASE_F5, LOGO, TEXT_BLACK} from './lib/const';
import {Image, View} from 'react-native';
import LeftMenu from './layouts/header/LeftMenu';
import DashboardScreen from './layouts/DashboardScreen';
import ForgetPassword from './layouts/auth/ForgetPassword';
import ResetPassword from './layouts/auth/ResetPassword';
import NetInfo from '@react-native-community/netinfo';

const AuthStack = createStackNavigator();

const Router = () => {
  const {user, loading, makeLoading, makeOffline} = useContext(AppContext);

  useEffect(() => {
    let timer1 = '';
    if (loading.apps_loaded) {
      SplashScreen.hide();
    }
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      if (offline) {
        makeLoading(false);
      }
      makeOffline(offline);
    });

    return () => {
      removeNetInfoSubscription();
      clearTimeout(timer1);
    };
  }, [loading.apps_loaded]);
  return (
    <>
      {loading.apps_loaded ? (
        <AuthStack.Navigator>
          {user.is_signed_in ? (
            <>
              <AuthStack.Screen
                options={{headerShown: false}}
                name="DashboardScreen"
                component={DashboardScreen}
              />
            </>
          ) : (
            <>
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Home"
                component={Home}
              />
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Login"
                component={LoginForm}
              />
              <AuthStack.Screen
                options={{headerShown: false}}
                name="ForgetPassword"
                component={ForgetPassword}
              />
              <AuthStack.Screen
                options={{headerShown: false}}
                name="ResetPassword"
                component={ResetPassword}
              />
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Registration"
                component={Registration}
              />
            </>
          )}
        </AuthStack.Navigator>
      ) : (
        <Loading />
      )}
    </>
  );
};
export default Router;
