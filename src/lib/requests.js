import http from './http';
import {LOGIN_URL} from './const';

const LoginRequest = ({username, password}) => {
  http.postData(LOGIN_URL, {username, password});
};
export {LoginRequest};
