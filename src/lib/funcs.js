import {showMessage} from 'react-native-flash-message';

const checkLogin = ({email, password}) => {
  if (email.length === 0) {
    showMessage({
      message: 'Email address required',
      type: 'danger',
    });
    return false;
  }
  if (!validateEmail(email)) {
    showMessage({
      message: 'Invalid email address',
      type: 'danger',
    });
    return false;
  }
  if (password.length === 0) {
    showMessage({
      message: 'Password is required',
      type: 'danger',
    });
    return false;
  }
  if (password.length < 8) {
    showMessage({
      message: 'Password must be at least 8 characters',
      type: 'danger',
    });
    return false;
  }

  return true;
};
const checkResetPassword = ({token, password, confirm_password}) => {
  if (token.length === 0) {
    showMessage({
      message: 'Verification code is required',
      type: 'danger',
    });
    return false;
  }
  if (token.length > 6 || token.length < 4) {
    showMessage({
      message: 'Verification code is invalid',
      type: 'danger',
    });
    return false;
  }
  if (password.length === 0) {
    showMessage({
      message: 'Password is required',
      type: 'danger',
    });
    return false;
  }
  if (password.length < 8) {
    showMessage({
      message: 'Password must be at least 8 characters',
      type: 'danger',
    });
    return false;
  }
  if (password !== confirm_password) {
    showMessage({
      message: "Confirm password doesn't match",
      type: 'danger',
    });
    return false;
  }
  return true;
};

const checkRegistration = ({
  name,
  email,
  password,
  mobile,
  confirm_password,
  terms_and_condition,
}) => {
  if (name.length === 0) {
    showMessage({
      message: 'Name is required',
      type: 'danger',
    });
    return false;
  }
  if (name.length < 3 || name.length > 40) {
    showMessage({
      message: 'Name is invalid',
      type: 'danger',
    });
    return false;
  }
  if (email.length === 0) {
    showMessage({
      message: 'Email address required',
      type: 'danger',
    });
    return false;
  }
  if (!validateEmail(email)) {
    showMessage({
      message: 'Email address is invalid',
      type: 'danger',
    });
    return false;
  }
  if (mobile.length === 0) {
    showMessage({
      message: 'Mobile number is required',
      type: 'danger',
    });
    return false;
  }
  if (mobile.length !== 11) {
    showMessage({
      message: 'Mobile number is invalid',
      type: 'danger',
    });
    return false;
  }
  if (password.length === 0) {
    showMessage({
      message: 'Password is required',
      type: 'danger',
    });
    return false;
  }
  if (password.length < 8) {
    showMessage({
      message: 'Password must be at least 8 characters',
      type: 'danger',
    });
    return false;
  }
  if (password != confirm_password) {
    showMessage({
      message: "Confirm password doesn't match",
      type: 'danger',
    });
    return false;
  }
  if (terms_and_condition === false) {
    showMessage({
      message: 'You have to agree with our terms and conditions',
      type: 'danger',
    });
    return false;
  }
  return true;
};

const checkApplication = ({
  nid,
  tin,
  salary,
  company,
  address,
  designation,
  joining_date,
  salary_expected_date,
  details,
  referrerName,
  referrerContactNo,
  referrerNID,
  referrerProfessionalDetails,
  salary_statement,
  tin_doc,
}) => {
  let errors = [];
  if (nid == null || nid.length === 0) {
    errors.push({
      message: 'NID number is invalid',
      type: 'danger',
    });
  }
  if (tin == null || tin.length === 0) {
    errors.push({
      message: 'TIN number is invalid',
      type: 'danger',
    });
  }
  if (salary == null || salary.length === 0) {
    errors.push({
      message: 'Salary is invalid',
      type: 'danger',
    });
  }
  if (salary_statement?.path == null || salary_statement.path === 0) {
    errors.push({
      message: 'Salary statement is required',
      type: 'danger',
    });
  }
  if (details == null || details.length === 0) {
    errors.push({
      message: 'Application details is required',
      type: 'danger',
    });
  }
  if (tin_doc == null || tin_doc.length === 0) {
    errors.push({
      message: 'TIN image is required',
      type: 'danger',
    });
  }
  referrerName.forEach((refName, index) => {
    if (refName == null || refName.length === 0) {
      errors.push({
        message: `Referrer ${index + 1} name is required`,
        type: 'danger',
      });
    }
  });
  referrerNID.forEach((refNid, index) => {
    if (refNid == null || refNid.length === 0) {
      errors.push({
        message: `Referrer ${index + 1} NID is required`,
        type: 'danger',
      });
    }
  });
  referrerProfessionalDetails.map((refProfessional, index) => {
    if (refProfessional == null || refProfessional.length === 0) {
      errors.push({
        message: `Referrer ${index + 1} professional details is required`,
        type: 'danger',
      });
    }
  });
  if (company.length === 0) {
    errors.push({
      message: 'Company name is invalid',
      type: 'danger',
    });
  }
  if (salary_expected_date.length === 0) {
    errors.push({
      message: 'Salary expected date is invalid',
      type: 'danger',
    });
  }
  if (address.length === 0) {
    errors.push({
      message: 'Company address is invalid',
      type: 'danger',
    });
  }
  if (designation.length === 0) {
    errors.push({
      message: 'Designation is invalid',
      type: 'danger',
    });
  }
  referrerContactNo.forEach((refContactNo, index) => {
    if (refContactNo.length === 0) {
      errors.push({
        message: `Referrer ${index + 1} Contact No is required`,
        type: 'danger',
      });
      return false;
    }
    if (refContactNo.length !== 11) {
      errors.push({
        message: `Referrer ${index + 1} Contact No is invalid`,
        type: 'danger',
      });
    }
  });

  if (joining_date.length === 0) {
    errors.push({
      message: 'Joining date is invalid',
      type: 'danger',
    });
    return false;
  }
  if (salary.length === 0) {
    errors.push({
      message: 'Salary is invalid',
      type: 'danger',
    });
    return false;
  }
  if (errors.length > 0) {
    showMessage(errors[0]);
    return false;
  }
  return true
};

const isApplicable = ({
  photo,
  signature,
  nid_front,
  nid_back,
  nid,
  tin,
  salary,
  company,
  address,
  designation,
  joining_date,
  experience,
}) => {
  if (photo == null || photo?.length === 0) {
    showMessage({
      message: 'Upload photo from profile update',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (signature == null || signature?.length === 0) {
    showMessage({
      message: 'Upload signature from profile update',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (nid_front == null || nid_front === 0) {
    showMessage({
      message: 'Upload NID front image',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (nid_back == null || nid_back === 0) {
    showMessage({
      message: 'Upload NID back image',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (nid == null || nid.length === 0) {
    showMessage({
      message: 'Update NID number from profile update',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (tin == null || tin.length === 0) {
    showMessage({
      message: 'Update TIN number from profile update',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (salary == null || salary.length === 0) {
    showMessage({
      message: 'Update salary from service information',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (company.length === 0) {
    showMessage({
      message: 'Update company name from service information',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (address.length === 0) {
    showMessage({
      message: 'Update company address from service information',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (designation.length === 0) {
    showMessage({
      message: 'Update designation from service information',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (joining_date.length === 0) {
    showMessage({
      message: 'Update joining date from service information',
      type: 'danger',
      position: 'center',
    });
    return false;
  }

  return true;
};
const checkServiceInfo = ({
  oid_front,
  oid_back,
  company,
  address,
  designation,
  joining_date,
  salary,
}) => {
  if (oid_front == null || oid_front?.length === 0) {
    showMessage({
      message: 'Please upload office id front image',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (oid_back == null || oid_back?.length === 0) {
    showMessage({
      message: 'Please upload office id back image',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (company == null || company?.length === 0) {
    showMessage({
      message: 'Company name is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (address == null || address?.length === 0) {
    showMessage({
      message: 'Company address is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (designation == null || designation?.length === 0) {
    showMessage({
      message: 'Designation is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (joining_date == null || joining_date?.length === 0) {
    showMessage({
      message: 'Joining date is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (salary == null || salary?.length === 0) {
    showMessage({
      message: 'Salary field should required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  return true;
};
const checkProfile = ({
  photo,
  signature,
  nid_front,
  nid_back,
  full_name,
  father_name,
  mother_name,
  gender,
  dob,
  religion,
  present_address,
  permanent_address,
  email,
  mobile,
  nid,
  tin,
}) => {
  if (photo == null || photo?.length === 0) {
    showMessage({
      message: 'Profile photo is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (signature == null || signature?.length === 0) {
    showMessage({
      message: 'Signature is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (nid_front == null || nid_front?.length === 0) {
    showMessage({
      message: 'NID front image is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (nid_back == null || nid_back?.length === 0) {
    showMessage({
      message: 'NID back image is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (full_name == null || full_name?.length === 0) {
    showMessage({
      message: 'Full name is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (father_name == null || father_name?.length === 0) {
    showMessage({
      message: 'Father name is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (mother_name == null || mother_name?.length === 0) {
    showMessage({
      message: 'Mother name is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (gender == null || gender?.length === 0) {
    showMessage({
      message: 'Gender is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (dob == null || dob?.length === 0) {
    showMessage({
      message: 'Date of birth is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (religion == null || religion?.length === 0) {
    showMessage({
      message: 'Religion is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (present_address == null || present_address?.length === 0) {
    showMessage({
      message: 'Present address is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (permanent_address == null || permanent_address?.length === 0) {
    showMessage({
      message: 'Permanent address is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (email == null || email?.length === 0) {
    showMessage({
      message: 'Email address is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (!validateEmail(email)) {
    showMessage({
      message: 'Invalid email address',
      type: 'danger',
    });
    return false;
  }
  if (mobile == null || mobile?.length === 0) {
    showMessage({
      message: 'Phone number is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (mobile?.length !== 11) {
    showMessage({
      message: 'Phone number is invalid',
      type: 'danger',
      position: 'center',
    });
    return false;
  }

  if (nid == null || nid.length === 0) {
    showMessage({
      message: 'NID is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (tin == null || tin.length === 0) {
    showMessage({
      message: 'TIN number is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  return true;
};

const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
const checkContactUs = ({subject, message}) => {
  if (subject == null || subject.length === 0) {
    showMessage({
      message: 'Subject is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  if (message == null || message.length === 0) {
    showMessage({
      message: 'Message is required',
      type: 'danger',
      position: 'center',
    });
    return false;
  }
  return true;
};
const checkPayback = ({
  method,
  paymentSlip,
  marchent,
  transactionCode,
  mobileBankingNo,
}) => {
  if (method === 'bank') {
    if (paymentSlip == null || paymentSlip.length === 0) {
      showMessage({
        message: 'Payment slip is required',
        type: 'danger',
      });
      return false;
    }
  } else {
    if (marchent == null || marchent.length === 0) {
      showMessage({
        message: 'Agent is required',
        type: 'danger',
      });
      return false;
    }
    if (transactionCode == null || transactionCode.length === 0) {
      showMessage({
        message: 'Transaction code is required',
        type: 'danger',
      });
      return false;
    }
    if (mobileBankingNo === null || mobileBankingNo.length === 0) {
      showMessage({
        message: 'Mobile account number is required',
        type: 'danger',
      });
      return false;
    }
    if (mobileBankingNo.length < 11 || mobileBankingNo.length > 13) {
      showMessage({
        message: 'Mobile account number is invalid',
        type: 'danger',
      });
      return false;
    }
  }
  return true;
};

const checkPaybackMethod = ({
  referralCode,
  amount,
  method,
  paymentSlip,
  marchent,
  transactionCode,
  mobileBankingNo,
}) => {
  if (referralCode == null || referralCode.length === 0) {
    showMessage({
      message: 'Referral code is required',
      type: 'danger',
    });
    return false;
  }
  if (amount == null || amount.length === 0) {
    showMessage({
      message: 'Amount is required',
      type: 'danger',
    });
    return false;
  }
  if (method === 'bank') {
    if (paymentSlip == null || paymentSlip.length === 0) {
      showMessage({
        message: 'Payment slip is required',
        type: 'danger',
      });
      return false;
    }
  } else {
    if (marchent == null || marchent.length === 0) {
      showMessage({
        message: 'Agent is required',
        type: 'danger',
      });
      return false;
    }
    if (transactionCode == null || transactionCode.length === 0) {
      showMessage({
        message: 'Transaction code is required',
        type: 'danger',
      });
      return false;
    }
    if (mobileBankingNo === null || mobileBankingNo.length === 0) {
      showMessage({
        message: 'Mobile account number is required',
        type: 'danger',
      });
      return false;
    }
    if (mobileBankingNo.length < 11 || mobileBankingNo.length > 13) {
      showMessage({
        message: 'Mobile account number is invalid',
        type: 'danger',
      });
      return false;
    }
  }
  return true;
};

const checkTransactionMethod = ({
  method,
  bank_name,
  branch_name,
  account_no,
  agent_name,
  mobile_no,
}) => {
  if (method === 'bank') {
    if (bank_name == null || bank_name.length === 0) {
      showMessage({
        message: 'Bank name is required',
        type: 'danger',
      });
      return false;
    }
    if (branch_name == null || branch_name.length === 0) {
      showMessage({
        message: 'Branch name is required',
        type: 'danger',
      });
      return false;
    }
    if (account_no == null || account_no.length === 0) {
      showMessage({
        message: 'Account number is required',
        type: 'danger',
      });
      return false;
    }
  } else {
    if (agent_name == null || agent_name.length === 0) {
      showMessage({
        message: 'Agent is required',
        type: 'danger',
      });
      return false;
    }
    if (mobile_no === null || mobile_no.length === 0) {
      showMessage({
        message: 'Mobile account number is required',
        type: 'danger',
      });
      return false;
    }
    if (mobile_no.length !== 11) {
      showMessage({
        message: 'Mobile account number is invalid',
        type: 'danger',
      });
      return false;
    }
  }
  return true;
};
export {
  checkPaybackMethod,
  checkTransactionMethod,
  checkPayback,
  checkContactUs,
  checkLogin,
  checkRegistration,
  validateEmail,
  checkResetPassword,
  checkApplication,
  isApplicable,
  checkProfile,
  checkServiceInfo,
};
