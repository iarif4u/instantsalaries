import {Dimensions} from 'react-native';
const APP_URL = 'https://instantsalaries.com/api';
const TAWK_TO_LINK = 'https://tawk.to/chat/603ca8401c1c2a130d637dc7/1evmf2fq4';
const LOGIN_URL = `${APP_URL}/login`;
const REGISTER_URL = `${APP_URL}/register`;
const PROFILE_URL = `${APP_URL}/me`;
const FORGET_PASSWORD_URL = `${APP_URL}/forget-password`;
const RESET_PASSWORD_URL = `${APP_URL}/reset-password`;
const PROFILE_UPDATE_URL = `${APP_URL}/profile/update`;
const PROFILE_PHOTO_URL = `${APP_URL}/profile/photo/update`;
const SIGNATURE_URL = `${APP_URL}/profile/signature/update`;
const SERVICE_INFO_URL = `${APP_URL}/service-info`;
const SERVICE_UPDATE_URL = `${APP_URL}/service-info/update`;
const OFFICE_ID_URL = `${APP_URL}/service-info/office/identification`;
const PROFILE_DOC_URL = `${APP_URL}/profile/information/update`;
const APPLICATION_URL = `${APP_URL}/instant-salary/app-submit`;
const APPLICATIONS_URL = `${APP_URL}/profile/view-applications`;
const CONTACT_US_URL = `${APP_URL}/contact-us`;
const APPLICATION_FEE_URL = `${APP_URL}/user/fee`;
const PAYBACK_URL = `${APP_URL}/user/payback`;
const CANCEL_URL = `${APP_URL}/cancel/instant_salaries`;
const TRANSACTION_METHOD_URL = `${APP_URL}/user/transaction-method`;
const TRANSACTIONS_URL = `${APP_URL}/user/transactions`;
const APPS_COLOR = '#F48120';
const TEXT_BLACK = '#000000';
const TEXT_GRAY = '#7f7f7f';
const TEXT_DASH = '#4e4e4e';
const BASE_F5 = '#e2e2e2';
const BASE_BG = '#f8f8f8';
const APPLY_BG = require('./../assets/1.png');
const APPLY_ICON = require('./../assets/1.1.png');
const LOGO = require('./../assets/logo.png');
const PROFILE = require('./../assets/profile.png');
const POCKET = require('./../assets/pocket.png');
const PLACEHOLDER_IMG = require('./../assets/placeholder.jpg');
const MESSENGER_IMG = require('./../assets/messenger.png');
const HOME_IMAGE = require('./../assets/image.png');
const FAQ_IMAGE = require('./../assets/FAQ.png');
const CONTACT_US_IMG = require('./../assets/contact_us.png');
const PRIVACY_IMG = require('./../assets/privacy_policy.png');
const TERMS_CONDITION_IMG = require('./../assets/terms_condition.png');
const DISPLAY_WIDTH = Dimensions.get('window').width;
const DISPLAY_HEIGHT = Dimensions.get('window').height;
const AVATAR = require('./../assets/avatar.png');
const HAND_TREE = require('./../assets/handtree.jpg');
const ROW_WIDTH = Math.floor(DISPLAY_WIDTH - DISPLAY_WIDTH / 15);
const SUPPORT_NUMBER = '+8801921222111';
const SUPPORT_EMAIL = 'care@instantsalaries.com';
const DRAWER_INACTIVE_COLOR = 'rgba(28, 28, 30, 0.68)';
const BANK_LIST = [
  'Sonali Bank Limited',
  'Janata Bank Limited',
  'Agrani Bank Limited',
  'Rupali Bank Limited',
  'BASIC Bank Limited',
  'Bangladesh Development Bank Limited',
  'Al-Arafah Islami Bank Limited',
  'EXIM Bank Limited',
  'First Security Islami Bank Limited',
  'ICB Islamic Bank Limited',
  'Islami Bank Bangladesh Limited',
  'Shahjalal Islami Bank Limited',
  'Social Islami Bank Limited',
  'Union Bank Ltd',
  'Bangladesh Krishi Bank',
  'Rajshahi Krishi Unnayan Bank',
  'Probashi Kallyan Bank',
  'AB Bank Limited',
  'Bangladesh Commerce Bank Limited',
  'Bank Asia Limited',
  'BRAC Bank Limited',
  'City Bank Limited',
  'Community Bank Bangladesh Limited',
  'Dhaka Bank Limited',
  'Dutch-Bangla Bank Limited',
  'Eastern Bank Limited',
  'HSBC Bank',
  'IFIC Bank Limited',
  'Jamuna Bank Limited',
  'Meghna Bank Limited',
  'Mercantile Bank Limited',
  'Midland Bank Limited',
  'Modhumoti Bank Limited',
  'Mutual Trust Bank Limited',
  'National Bank Limited',
  'National Credit & Commerce Bank Limited',
  'NRB Bank Limited',
  'NRB Commercial Bank Ltd',
  'NRB Global Bank Ltd',
  'One Bank Limited',
  'Padma Bank Limited',
  'Premier Bank Limited',
  'Prime Bank Limited',
  'Pubali Bank Limited',
  'Standard Bank Limited',
  'Standard Chartered Bank',
  'Shimanto Bank Ltd',
  'Southeast Bank Limited',
  'South Bangla Agriculture and Commerce Bank Limited',
  'Trust Bank Limited',
  'United Commercial Bank Ltd',
  'Uttara Bank Limited',
  'Bengal Commercial Bank Ltd',
];
const BANKS = BANK_LIST.map((bank, index) => {
  return {id: bank.split(' ').join('_'), name: bank};
});

export {
  PRIVACY_IMG,
  TERMS_CONDITION_IMG,
  CONTACT_US_IMG,
  FAQ_IMAGE,
  HOME_IMAGE,
  DISPLAY_WIDTH,
  CANCEL_URL,
  MESSENGER_IMG,
  PAYBACK_URL,
  TRANSACTIONS_URL,
  BANKS,
  BANK_LIST,
  APPLICATION_FEE_URL,
  CONTACT_US_URL,
  DRAWER_INACTIVE_COLOR,
  SUPPORT_EMAIL,
  SUPPORT_NUMBER,
  TAWK_TO_LINK,
  HAND_TREE,
  POCKET,
  APPLICATION_URL,
  PROFILE_DOC_URL,
  PLACEHOLDER_IMG,
  APPLY_BG,
  APPLY_ICON,
  APPS_COLOR,
  DISPLAY_HEIGHT,
  LOGO,
  PROFILE,
  ROW_WIDTH,
  LOGIN_URL,
  TEXT_BLACK,
  TEXT_GRAY,
  TEXT_DASH,
  BASE_BG,
  BASE_F5,
  REGISTER_URL,
  SIGNATURE_URL,
  AVATAR,
  PROFILE_URL,
  PROFILE_PHOTO_URL,
  PROFILE_UPDATE_URL,
  SERVICE_INFO_URL,
  OFFICE_ID_URL,
  SERVICE_UPDATE_URL,
  FORGET_PASSWORD_URL,
  RESET_PASSWORD_URL,
  APPLICATIONS_URL,
  TRANSACTION_METHOD_URL,
};
