import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaView, StatusBar} from 'react-native';
import Application from './Application';
import FlashMessage from 'react-native-flash-message';

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        <Application />
      </SafeAreaView>
      <FlashMessage autoHide={true} hideStatusBar={false} position="top" />
    </NavigationContainer>
  );
};

export default App;
